/*
 * Anjuta evo-mail plugin
 *
 * Start an evolution composer window with the current document attached.
 */

#include <config.h>
#include <bonobo/bonobo-stream-memory.h>
#include <libanjuta/libanjuta.h>
#include <gdl/gdl.h>

#include <liboaf/oaf-async.h>
#include "Evolution-Composer.h"
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#define MAIL_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:mail"
#define PLUGIN_NAME			"anjuta-mail-plugin"
#define PLUGIN_XML			"anjuta-mail-plugin.xml"

#define BUF_SIZE 500

static GNOME_Evolution_Composer_AttachmentData *
attachment_data_from_stream (BonoboStreamMem *stream)
{
	GNOME_Evolution_Composer_AttachmentData *ret;
	ret = GNOME_Evolution_Composer_AttachmentData__alloc ();
	ret->_length = ret->_maximum = bonobo_stream_mem_get_size (stream);
	ret->_buffer = CORBA_sequence_CORBA_char_allocbuf (ret->_length);
	strcpy (ret->_buffer, bonobo_Stream_get_buffer (stream));

	return ret;
}

static void
send_mail (CORBA_Object evo_shell, 
	   const char *error_reason, 
	   gpointer data)
{
	CORBA_Environment ev;
	CORBA_Object composer;
	GNOME_Development_Environment_Document doc;
	AnjutaTool *tool = ANJUTA_TOOL (data);
	char *filename;
	char *mime_type;
	char *basename;
	Bonobo_PersistStream pstream;
	BonoboStream *memstream;
	Bonobo_Stream_iobuf iobuf;

	CORBA_exception_init (&ev);
	oaf_activate_from_id ("OAFIID:GNOME_Evolution_Shell",
			      0, NULL, &ev);
	oaf_activate_from_id ("OAFIID:GNOME_Evolution_Mail_ShellComponent",
			      0, NULL, &ev);

	doc = GNOME_Development_Environment_Shell_getObject (tool->shell,
							     "CurrentDocument",
							     &ev);
	
	if (CORBA_Object_is_nil (doc, &ev)) {
		anjuta_error_dialog (_("No document to send."));
		CORBA_exception_free (&ev);
		return;
	}
	
	pstream = GNOME_Development_Environment_Document_getEditorInterface 
		(doc, "IDL:Bonobo/PersistStream:1.0", &ev);
	
	if (CORBA_Object_is_nil (pstream, &ev)) {
		/* FIXME: Should support files too */
		anjuta_error_dialog (_("Document doesn't support the Bonobo::PersistStream interface."));
		bonobo_object_release_unref (doc, &ev);
		CORBA_exception_free (&ev);
		return;
	}	

	composer = oaf_activate_from_id ((const OAF_ActivationID)"OAFIID:GNOME_Evolution_Mail_Composer",
					 0, NULL, &ev);
	
	if (CORBA_Object_is_nil (composer, &ev)) {
		bonobo_object_release_unref (pstream, &ev);
		bonobo_object_release_unref (doc, &ev);
		CORBA_exception_free (&ev);
		return;
	}
	
	memstream = bonobo_stream_mem_create (NULL, 0, FALSE, TRUE);
	
	mime_type = GNOME_Development_Environment_Document_getMimeType (doc, 
									&ev);
	
	Bonobo_PersistStream_save (pstream, BONOBO_OBJREF (memstream),
				   mime_type, &ev);

	/* null terminate the stream */
	iobuf._buffer = CORBA_sequence_CORBA_octet_allocbuf (1);
	iobuf._buffer[0] = '\0';
	iobuf._length = 1;
	Bonobo_Stream_write (BONOBO_OBJREF (memstream), &iobuf, &ev);

	filename = GNOME_Development_Environment_Document_getFilename (doc,
								       &ev);
	if (filename[0] == '\0') {
		CORBA_free (filename);
		filename = CORBA_string_dup ("Untitled");
	}
	
	basename = g_basename (filename);
	attach_data = attachment_data_from_stream (memstream);

	GNOME_Evolution_Composer_attachData (composer, mime_type, basename, 
					     basename, CORBA_TRUE, 
					     attach_data,
					     &ev);
	CORBA_free (attach_data);
	
	GNOME_Evolution_Composer_show (composer, &ev);

	CORBA_free (mime_type);
	CORBA_free (filename);
	bonobo_object_unref (BONOBO_OBJECT (memstream));
	bonobo_object_release_unref (pstream, &ev);
	bonobo_object_release_unref (doc, &ev);
	
	CORBA_exception_free (&ev);	
}

static void
mail (GtkWidget *widget, gpointer data) 
{
	CORBA_Environment ev;
	Bonobo_Unknown obj;
	gboolean ret;

	CORBA_exception_init (&ev);
	
	obj = oaf_activate_from_id ("OAFIID:GNOME_Evolution_Shell", 
				    OAF_FLAG_EXISTING_ONLY, NULL, &ev);
	

	if (CORBA_Object_is_nil (obj, &ev)) {
		anjuta_error_dialog (_("Evolution is not running.  Evolution "
				   "must be \nrunning to send mail with "
				   "this plugin."));
	} else {
		send_mail (obj, NULL, data);
	}
	
	CORBA_exception_free (&ev);
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("Mail", mail),
	BONOBO_UI_VERB_END
};

/*
 * Boilerplate initialization function
 */
static gboolean
impl_init(
	AnjutaTool*			tool,
	gpointer			closure
)
{
	BonoboUIComponent*		uic;
	CORBA_Environment		ev;

	g_return_val_if_fail(tool != NULL, FALSE);
	g_return_val_if_fail(ANJUTA_IS_TOOL(tool), FALSE);

	CORBA_exception_init(&ev);

	uic = bonobo_ui_component_new(PLUGIN_NAME);
	bonobo_ui_component_set_container(uic, tool->ui_container);
	bonobo_ui_util_set_ui(uic, ANJUTA_DATADIR, PLUGIN_XML,
		PLUGIN_NAME);

	bonobo_ui_component_add_verb_list_with_data(uic, verbs, tool);

	gtk_object_set_data(GTK_OBJECT(tool), "ui_component", uic);

	CORBA_exception_free(&ev);

	return TRUE;
}

/*
 * Boilerplace cleanup function
 */
static void
impl_cleanup(
	AnjutaTool*			tool,
	gpointer			closure
)
{
	bonobo_ui_component_unset_container(gtk_object_get_data(GTK_OBJECT(
		tool), "ui_component"));
	gtk_object_remove_data(GTK_OBJECT(tool), "ui_component");
}

ANJUTA_SHLIB_COMPONENT(MAIL_COMPONENT_IID, "Anjuta Mail Plugin",
		       impl_init, impl_cleanup, NULL);
