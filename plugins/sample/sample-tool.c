/*
 * Anjuta sample plugin
 *
 * Demonstrates the basics of how to write a plugin for Anjuta.
 */

#include <config.h>

#include <bonobo/bonobo-ui-component.h>

#include <libanjuta/anjuta-tool.h>
#include <libanjuta/glue-plugin.h>

gpointer parent_class;

typedef struct {
	AnjutaTool parent;
} SampleTool;

typedef struct {
	AnjutaToolClass parent;
} SampleToolClass;

/*
 * Insert the text "Hello, World!" in the current document
 */
static void
hello_world (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);

	anjuta_insert_text_at_cursor (tool, "Hello, world!\n");
}

static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("HelloWorld", hello_world),
	BONOBO_UI_VERB_END
};

static void
shell_set (AnjutaTool *tool)
{
	anjuta_tool_merge_ui (tool,
			      "anjuta-sample-tool",
			      ANJUTA_DATADIR,
			      "anjuta-sample-plugin.xml",
			      verbs,
			      tool);
}

static void
dispose (GObject *obj)
{
	anjuta_tool_unmerge_ui (ANJUTA_TOOL (obj));
}

static void
sample_tool_instance_init (GObject *object)
{
}

static void
sample_tool_class_init (GObjectClass *klass) 
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);
	
	parent_class = g_type_class_peek_parent (klass);

	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (SampleTool, sample_tool);

ANJUTA_SIMPLE_PLUGIN (SampleTool, sample_tool);
