/* Anjuta
 * Copyright 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <pwd.h>
#include <bonobo.h>
#include <gconf/gconf-client.h>
#include <libanjuta/anjuta-tool.h>
#include <libanjuta/glue-plugin.h>
#include <vte/vte.h>

/* Number of terminals created. */
#define TERMINAL_COUNT		"/apps/anjuta2/plugins/terminal/terminal_count"

/* Some desktop/gnome-terminal gconf keys. */
#define MONOSPACE_FONT		"/desktop/gnome/interface/monospace_font_name"
#define BACKGROUND_COLOR	"/apps/gnome-terminal/profiles/Default/background_color"
#define BACKSPACE_BINDING	"/apps/gnome-terminal/profiles/Default/backspace_binding"
#define CURSOR_BLINK		"/apps/gnome-terminal/profiles/Default/cursor_blink"
#define DELETE_BINDING		"/apps/gnome-terminal/profiles/Default/delete_binding"
#define EXIT_ACTION		"/apps/gnome-terminal/profiles/Default/exit_action"
#define TERMINAL_FONT		"/apps/gnome-terminal/profiles/Default/font"
#define FOREGROUND_COLOR	"/apps/gnome-terminal/profiles/Default/foreground_color"
#define SCROLLBACK_LINES	"/apps/gnome-terminal/profiles/Default/scrollback_lines"
#define SCROLL_ON_KEYSTROKE	"/apps/gnome-terminal/profiles/Default/scroll_on_keystroke"
#define SCROLL_ON_OUTPUT	"/apps/gnome-terminal/profiles/Default/scroll_on_output"
#define SILENT_BELL		"/apps/gnome-terminal/profiles/Default/silent_bell"
#define USE_SYSTEM_FONT		"/apps/gnome-terminal/profiles/Default/use_system_font"
#define WORD_CHARS		"/apps/gnome-terminal/profiles/Default/word_chars"

gpointer parent_class;
extern char **environ;

typedef struct {
	AnjutaTool parent;
} TerminalTool;

typedef struct {
	AnjutaToolClass parent;
} TerminalToolClass;

static BonoboControl *term_new (void);

static void
new_terminal (GtkWidget *widget,
	      gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	GConfClient *client;
	BonoboControl *control;
	char *name;
	int term_count;
	
	/* Increase terminal_count value. */
	client = gconf_client_get_default ();
	term_count = gconf_client_get_int (client, TERMINAL_COUNT, NULL);
	term_count++;
	gconf_client_set_int (client, TERMINAL_COUNT, term_count, NULL);
	g_object_unref (client);

	/* Create new terminal. */
	control = term_new ();
	name = g_strdup_printf ("Terminal%d", term_count);
	anjuta_shell_add_control (tool->shell,
				  BONOBO_OBJREF (control),
				  name,
				  _("Terminal"),
				  NULL);
	g_free (name);
}

static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("NewTerminal", new_terminal),
	BONOBO_UI_VERB_END
};

static char **
get_child_environment (GtkWidget *term)
{
	/* code from gnome-terminal, sort of. */
	char **p;
	int i;
	char **retval;
#define EXTRA_ENV_VARS 6

	/* count env vars that are set */
	for (p = environ; *p; p++);

	i = p - environ;
	retval = g_new (char *, i + 1 + EXTRA_ENV_VARS);

	for (i = 0, p = environ; *p; p++) {
		/* Strip all these out, we'll replace some of them */
		if ((strncmp (*p, "COLUMNS=", 8) == 0) ||
		    (strncmp (*p, "LINES=", 6) == 0)   ||
		    (strncmp (*p, "TERM=", 5) == 0)    ||
		    (strncmp (*p, "GNOME_DESKTOP_ICON=", 19) == 0)) {
			/* nothing: do not copy */
		} else {
			retval[i] = g_strdup (*p);
			++i;
		}
	}

	retval[i] = g_strdup ("TERM=xterm"); /* FIXME configurable later? */
	++i;

	retval[i] = NULL;

	return retval;
}

static void
term_init_cb (GtkWidget *widget,
	      gpointer   data)
{
	VteTerminal *term = VTE_TERMINAL (widget);
	struct passwd *pw;
	const char *shell;
	const char *dir;
	char **env;
	GConfClient *client;
	GdkColor color;
	char *colorname;

	vte_terminal_reset (term, TRUE, TRUE);

	pw = getpwuid (getuid ());
	if (pw) {
		shell = pw->pw_shell;
		dir = pw->pw_dir;
	} else {
		shell = "/bin/sh";
		dir = "/";
	}
	env = get_child_environment (widget);
	vte_terminal_fork_command (term, shell, NULL, env, dir, FALSE, FALSE, FALSE);
	g_strfreev (env);

	client = gconf_client_get_default ();

	/* Set fore- and background colors. */
	colorname = gconf_client_get_string (client, BACKGROUND_COLOR, NULL);
	gdk_color_parse (colorname, &color);
	vte_terminal_set_color_background (term, &color);
	colorname = gconf_client_get_string (client, FOREGROUND_COLOR, NULL);
	gdk_color_parse (colorname, &color);
	vte_terminal_set_color_foreground (term, &color);

	g_object_unref (client);
}

static gboolean
term_focus_cb (GtkWidget *widget,
	       GdkEvent  *event,
	       gpointer   data) 
{
	gtk_widget_grab_focus (widget);
	
	return TRUE;
}

static void
term_exited_cb (GtkWidget *widget,
		gpointer   data)
{
	GConfClient *client;
	char *exit_action;

	/* Get exit_action value. */
	client = gconf_client_get_default ();
	exit_action = gconf_client_get_string (client, EXIT_ACTION, NULL);
	g_object_unref (client);

	if (!strcmp (exit_action, "exit")) {
		g_object_unref (widget);
	} else if (!strcmp (exit_action, "restart")) {
		term_init_cb (widget, data);
	}
}

static void
term_destroy_cb (GtkWidget *widget,
		 gpointer   data)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (widget),
					      G_CALLBACK (term_init_cb), NULL);
}

static BonoboControl *
term_new (void)
{
	GConfClient *client;
	char *text;
	int value;
	gboolean setting;
	GtkWidget *vte, *sb, *frame, *hbox;
	BonoboControl *control;

	client = gconf_client_get_default ();

	/* Create new terminal. */
	vte = vte_terminal_new ();
	vte_terminal_set_size (VTE_TERMINAL (vte), 50, 1);
	vte_terminal_set_mouse_autohide (VTE_TERMINAL (vte), TRUE);

	/* Set terminal font either using the desktop wide font or g-t one. */
	setting = gconf_client_get_bool (client, USE_SYSTEM_FONT, NULL);
	if (setting) {
		text = gconf_client_get_string (client, MONOSPACE_FONT, NULL);
		if (!text)
			text = gconf_client_get_string (client, TERMINAL_FONT, NULL);
	} else {
		text = gconf_client_get_string (client, TERMINAL_FONT, NULL);
	}
	vte_terminal_set_font_from_string (VTE_TERMINAL (vte), text);

	setting = gconf_client_get_bool (client, CURSOR_BLINK, NULL);
	vte_terminal_set_cursor_blinks (VTE_TERMINAL (vte), setting);
	setting = gconf_client_get_bool (client, SILENT_BELL, NULL);
	vte_terminal_set_audible_bell (VTE_TERMINAL (vte), !setting);
	value = gconf_client_get_int (client, SCROLLBACK_LINES, NULL);
	vte_terminal_set_scrollback_lines (VTE_TERMINAL (vte), value);
	setting = gconf_client_get_bool (client, SCROLL_ON_KEYSTROKE, NULL);
	vte_terminal_set_scroll_on_keystroke (VTE_TERMINAL (vte), setting);
	setting = gconf_client_get_bool (client, SCROLL_ON_OUTPUT, NULL);
	vte_terminal_set_scroll_on_output (VTE_TERMINAL (vte), TRUE);
	text = gconf_client_get_string (client, WORD_CHARS, NULL);
	vte_terminal_set_word_chars (VTE_TERMINAL (vte), text);

	text = gconf_client_get_string (client, BACKSPACE_BINDING, NULL);
	if (!strcmp (text, "ascii-del"))
		vte_terminal_set_backspace_binding (VTE_TERMINAL (vte),
						    VTE_ERASE_ASCII_DELETE);
	else if (!strcmp (text, "escape-sequence"))
		vte_terminal_set_backspace_binding (VTE_TERMINAL (vte),
						    VTE_ERASE_DELETE_SEQUENCE);
	else if (!strcmp (text, "control-h"))
		vte_terminal_set_backspace_binding (VTE_TERMINAL (vte),
						    VTE_ERASE_ASCII_BACKSPACE);
	else
		vte_terminal_set_backspace_binding (VTE_TERMINAL (vte),
						    VTE_ERASE_AUTO);

	text = gconf_client_get_string (client, DELETE_BINDING, NULL);
	if (!strcmp (text, "ascii-del"))
		vte_terminal_set_delete_binding (VTE_TERMINAL (vte),
						 VTE_ERASE_ASCII_DELETE);
	else if (!strcmp (text, "escape-sequence"))
		vte_terminal_set_delete_binding (VTE_TERMINAL (vte),
						 VTE_ERASE_DELETE_SEQUENCE);
	else if (!strcmp (text, "control-h"))
		vte_terminal_set_delete_binding (VTE_TERMINAL (vte),
						 VTE_ERASE_ASCII_BACKSPACE);
	else
		vte_terminal_set_delete_binding (VTE_TERMINAL (vte),
						 VTE_ERASE_AUTO);

	g_object_unref (client);

	g_signal_connect(G_OBJECT (vte), "button_press_event",
			 G_CALLBACK (term_focus_cb), NULL);
	g_signal_connect (G_OBJECT (vte), "child-exited",
			  G_CALLBACK (term_exited_cb), NULL);
	g_signal_connect (G_OBJECT (vte), "destroy",
			  G_CALLBACK (term_destroy_cb), NULL);

	sb = gtk_vscrollbar_new (GTK_ADJUSTMENT (VTE_TERMINAL (vte)->adjustment));
	GTK_WIDGET_UNSET_FLAGS (sb, GTK_CAN_FOCUS);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (frame), hbox);
	gtk_box_pack_start (GTK_BOX (hbox), vte, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), sb, FALSE, TRUE, 0);
	gtk_widget_show_all (frame);

	control = bonobo_control_new (frame);

	term_init_cb (vte, NULL);

	return control;
}

static void
shell_set (AnjutaTool *tool)
{
	GConfClient *client;
	int term_count, i;
	BonoboControl *control;

	anjuta_tool_merge_ui (tool,
			      "anjuta-terminal-tool",
			      ANJUTA_DATADIR,
			      "anjuta-terminal-plugin.xml",
			      verbs,
			      tool);

	/* Get the number of terminals in a previous anjuta2 instance. */
	client = gconf_client_get_default ();
	term_count = gconf_client_get_int (client, TERMINAL_COUNT, NULL);
	g_object_unref (client);

	/* Recreate terminals. */
	for (i = 0; i < term_count; i++) {
		char *name = g_strdup_printf ("Terminal%d", i);

		control = term_new ();

		anjuta_shell_add_control (tool->shell,
					  BONOBO_OBJREF (control),
					  name,
					  _("Terminal"),
					  NULL);
		g_free (name);
	}
}

static void
dispose (GObject *obj)
{
	anjuta_tool_unmerge_ui (ANJUTA_TOOL (obj));
}

static void
terminal_tool_instance_init (GObject *object)
{
}

static void
terminal_tool_class_init (GObjectClass *klass) 
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (TerminalTool, terminal_tool);

ANJUTA_SIMPLE_PLUGIN (TerminalTool, terminal_tool);
