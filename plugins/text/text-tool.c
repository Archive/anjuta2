/*
 * Anjuta text plugin
 *
 * Contains all text based plugins:
 *  - insert date and time
 *  - insert GPL header (C and C++)
 *  - insert file
 *  - delete BOF, EOF, BOL, EOL
 *  - replace tabs with spaces
 */

#include <config.h>

#include <bonobo-activation/bonobo-activation.h>
#include <gtk/gtk.h>
#include <libbonobo.h>
#include <libbonoboui.h>
#include <libanjuta/libanjuta.h>

#include "text-insert.h"
#include "text-delete.h"
#include "text-replace.h"

#define PLUGIN_NAME			"anjuta-text-plugin"
#define PLUGIN_XML			"anjuta-text-plugin.xml"


typedef struct {
	AnjutaTool parent;
} TextTool;

typedef struct {
	AnjutaToolClass parent;
} TextToolClass;

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("TextInsertDateTime", text_insert_date),
	BONOBO_UI_UNSAFE_VERB("TextInsertGplC", text_insert_gpl_c),
	BONOBO_UI_UNSAFE_VERB("TextInsertGplCpp", text_insert_gpl_cpp),
	BONOBO_UI_UNSAFE_VERB("TextInsertFile", text_insert_file),
	BONOBO_UI_UNSAFE_VERB("TextDeleteBOF", text_delete_to_bof),
	BONOBO_UI_UNSAFE_VERB("TextDeleteEOF", text_delete_to_eof),
	BONOBO_UI_UNSAFE_VERB("TextDeleteBOL", text_delete_to_bol),
	BONOBO_UI_UNSAFE_VERB("TextDeleteEOL", text_delete_to_eol),
	BONOBO_UI_UNSAFE_VERB("TextReplaceTabs", text_replace_tab_spaces),
	BONOBO_UI_VERB_END
};

static void
shell_set (AnjutaTool *tool)
{
	anjuta_tool_merge_ui (tool,
			      "anjuta-text-tool",
			      ANJUTA_DATADIR,
			      "anjuta-text-plugin.xml",
			      verbs, tool);
}

static void
dispose (GObject *obj)
{
	anjuta_tool_unmerge_ui (ANJUTA_TOOL (obj));
}

static void
text_tool_instance_init (GObject *object)
{
}

static void
text_tool_class_init (GObjectClass *klass)
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);
	
	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (TextTool, text_tool);

ANJUTA_SIMPLE_PLUGIN (TextTool, text_tool);
