/*
 * Text manipulation plugin
 *
 * Author:
 *   JP Rosevear (jpr@arcavia.com)
 */

#include <libanjuta/libanjuta.h>
#include <stdio.h>
#include <errno.h>
#include <bonobo/bonobo-file-selector-util.h>
#include "text-insert.h"
#include <bonobo/bonobo-i18n.h>
#include <time.h>

void
text_insert_date(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;
	time_t				t;
	struct tm*			st;
	gchar*				timestr;

	time(&t);

	st = localtime(&t);

	timestr = asctime(st);

	anjuta_insert_text_at_cursor(tool, timestr);
}

void
text_insert_gpl_c(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;
	gchar*				GPL_Notice =
	    "/* Program Name\n"
	    " * Copyright (C) 1999 Author Name\n"
	    " *\n"
	    " * This program is free software; you can redistribute it and/or modify\n"
	    " * it under the terms of the GNU General Public License as published by\n"
	    " * the Free Software Foundation; either version 2 of the License, or\n"
	    " * (at your option) any later version.\n"
	    " *\n"
	    " * This program is distributed in the hope that it will be useful,\n"
	    " * but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	    " * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	    " * GNU General Public License for more details.\n"
	    " *\n"
	    " * You should have received a copy of the GNU General Public License\n"
	    " * along with this program; if not, write to the Free Software\n"
	    " * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.\n"
	    " */\n"
	    "\n";

	anjuta_insert_text_at_cursor(tool, GPL_Notice);
}

void
text_insert_gpl_cpp(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;
	gchar*				GPL_Notice =
	    "// Program Name\n"
	    "// Copyright (C) 1999 Author Name\n"
	    "//\n"
	    "// This program is free software; you can redistribute it and/or modify\n"
	    "// it under the terms of the GNU General Public License as published by\n"
	    "// the Free Software Foundation; either version 2 of the License, or\n"
	    "// (at your option) any later version.\n"
	    "//\n"
	    "// This program is distributed in the hope that it will be useful,\n"
	    "// but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	    "// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	    "// GNU General Public License for more details.\n"
	    "//\n"
	    "// You should have received a copy of the GNU General Public License\n"
	    "// along with this program; if not, write to the Free Software\n"
	    "// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.\n"
	    "\n";

	anjuta_insert_text_at_cursor(tool, GPL_Notice);
}

#define STRLEN 255

void
text_insert_file(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;
	FILE*				f;
	gchar*				filename;
	gchar				buf[STRLEN];
	
	filename = bonobo_file_selector_open (NULL, FALSE, _("Insert File..."), 
					      NULL, NULL);
	if(filename) {
		f = fopen(filename, "r");
		if(!f)
		{
			g_snprintf(buf, sizeof(buf), _("File Error: %s"),
				   strerror(errno));
		        anjuta_dialog_error(buf);
			g_free (filename);
			return;
		}

		while(fgets(buf, sizeof(buf), f))
		{
			anjuta_insert_text_at_cursor(tool, buf);
		}

		fclose(f);
		g_free (filename);
	}
}
