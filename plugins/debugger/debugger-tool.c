/* Anjuta Debugger component, based on gnome-debug */

#include <config.h>

#include <gnome.h>
#include <bonobo.h>
#include <gdf/gdf.h>
#include <gdl/gdl.h>
#include <liboaf/liboaf.h>
#include <libanjuta/libanjuta.h>
#include <gbf/gbf.h>

typedef struct 
{
	BonoboUIComponent *uic;

	GNOME_Development_Target *current_target;
	CORBA_any *current_target_any;
	
	Bonobo_EventSource shell_event_source;
	Bonobo_EventSource_ListenerId listener_id;

#if 0
	GNOME_Development_BreakpointSet bs;
	GNOME_Development_Debugger *dbg;
#endif
	gboolean running;
	
	char *last_file;
	int last_line;
} DebuggerToolPriv;


#define PRIV(m) ((DebuggerToolPriv*)m->data)

#define DEBUGGER_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:debugger" 
      
typedef struct {
	char *iid;
	char *name;
	char *title;
	
	GNOME_Development_Environment_Shell_WindowLocation loc;
} GdfControl;

#if 0
static void unload_debugger (AnjutaTool *tool);
static void debug_clicked_cb (GtkWidget *widget, gpointer data);
static void stop_cb (GtkWidget *widget, gpointer data);
static void step_into_cb (GtkWidget *widget, gpointer data);
static void step_over_cb (GtkWidget *widget, gpointer data);
static void step_out_cb (GtkWidget *widget, gpointer data);
static void set_breakpoint_cb (GtkWidget *widget, gpointer data);
static void load_debugger (AnjutaTool *tool);
static void set_sensitivities (AnjutaTool *tool);
static void state_change_cb (AnjutaTool *tool, GdfDebuggerClient *client);

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("Debug", debug_clicked_cb),
	BONOBO_UI_UNSAFE_VERB ("Stop", stop_cb),
	BONOBO_UI_UNSAFE_VERB ("StepInto", step_into_cb),
	BONOBO_UI_UNSAFE_VERB ("StepOver", step_over_cb),
	BONOBO_UI_UNSAFE_VERB ("StepOut", step_out_cb),
	BONOBO_UI_UNSAFE_VERB ("SetBreakpoint", set_breakpoint_cb),
	BONOBO_UI_VERB_END
};
#endif

GdfControl controls [] = {
	{
		"OAFIID:GNOME_Development_LocalsViewer",
		"GdfLocalsViewer",
		"Locals Viewer",
		ANJUTA_WINDOW_LOC_BOTTOM
	},
	{
		"OAFIID:GNOME_Development_VariableViewer",
		"GdfVariableViewer",
		"Variable Viewer",
		ANJUTA_WINDOW_LOC_BOTTOM
	},
	{
		"OAFIID:GNOME_Development_StackBrowser",
		"GdfStackBrowser",
		"Stack Browser",
		ANJUTA_WINDOW_LOC_BOTTOM
	},
	{
		"OAFIID:GNOME_Development_BreakpointManager",
		"GdfBreakpointManager",
		"Breakpoint Manager",
		ANJUTA_WINDOW_LOC_BOTTOM
	},
	{
		"OAFIID:GNOME_Development_OutputTerminal",
		"GdfOutputTerminal",
		"Output Terminal",
		ANJUTA_WINDOW_LOC_BOTTOM
	},
	{ NULL }
};

#if 0
static void 
debug_clicked_cb (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);

	g_return_if_fail (PRIV(tool)->current_target);

	if (PRIV(tool)->dbg) {
		gdf_debugger_client_cont (PRIV(tool)->dbg);
	} else {
		load_debugger (tool);
	}
}

static void
stop_cb (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);

	g_return_if_fail (PRIV(tool)->dbg);
	gdf_debugger_client_stop (PRIV(tool)->dbg);
}

static void
step_into_cb (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);

	g_return_if_fail (PRIV(tool)->dbg);
	gdf_debugger_client_step_into (PRIV(tool)->dbg);
}

static void
step_over_cb (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);

	g_return_if_fail (PRIV(tool)->dbg);
	gdf_debugger_client_step_over (PRIV(tool)->dbg);
}

static void
step_out_cb (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);

	g_return_if_fail (PRIV(tool)->dbg);
	gdf_debugger_client_step_out (PRIV(tool)->dbg);
}

static void
set_breakpoint_cb (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	g_return_if_fail (PRIV(tool)->bs);
	
	gdf_add_breakpoint_dialog (NULL, PRIV(tool)->bs);
}

static char *
get_target_filename (AnjutaTool *tool, GNOME_Development_Target *target)
{
	char *ret = NULL;
	CORBA_Environment ev;	
	GNOME_Development_Project corba_prj;
	GbfProjectClient *prj;

	CORBA_exception_init (&ev);
	corba_prj =
		GNOME_Development_Environment_Shell_getObject (tool->shell, 
							       "CurrentProject",
							       &ev);
	if (!CORBA_Object_is_nil (corba_prj, &ev)) {
		GbfProjectClientResult res;
		char *root;

		prj = gbf_project_client_new_from_corba (corba_prj);
		res = gbf_project_client_get_project_root (prj, &root);
		if (res == GBF_PROJECT_CLIENT_OK) {
			ret = g_strdup_printf ("%s/%s/.libs/%s",
					       root, target->path, 
					       target->name);
			
			CORBA_free (root);
		} else {
			GDL_TRACE ();
		}
		gtk_object_unref (GTK_OBJECT (prj));
	}	
	bonobo_object_release_unref (corba_prj, &ev);

	CORBA_exception_free (&ev);

	return ret;
}

static void
set_control_debugger (Bonobo_Control ctrl, char *ior)
{
	CORBA_Environment ev;
	Bonobo_PropertyBag pb;
	
	CORBA_exception_init (&ev);

	pb = Bonobo_Control_getProperties (ctrl, &ev);
	bonobo_property_bag_client_set_value_string (pb, "debugger-ior",
						     ior, &ev);
	CORBA_exception_free (&ev);
}

static void
set_debuggers (AnjutaTool *tool)
{
	CORBA_Environment ev;
	GdfControl *ctrl = controls;
	char *ior;
	
	CORBA_exception_init (&ev);
	if (PRIV(tool)->dbg) {
		ior = CORBA_ORB_object_to_string (oaf_orb_get (), 
						  PRIV(tool)->dbg->objref, &ev);
	} else {
		ior = CORBA_string_dup ("");
	}

	CORBA_exception_free (&ev);
	
	while (ctrl->iid) {
		if (strcmp (ctrl->name, "GdfBreakpointManager")) {
			Bonobo_Control objref;
			objref = gtk_object_get_data (GTK_OBJECT (tool), 
						      ctrl->name);
			set_control_debugger (objref, ior);
		}
		
		ctrl++;
	}
}

static void
execution_source_line_cb (GdfDebuggerClient *dbg, char *filename, int line, 
			  AnjutaTool *tool)
{
	/* FIXME: only remove the marker if the document is open */
 	if (PRIV(tool)->last_file) {
		anjuta_remove_marker (tool, PRIV(tool)->last_file, 
				    PRIV(tool)->last_line, "CurrentLine");
		g_free (PRIV(tool)->last_file);
	}
	anjuta_show_file (tool, filename);
	anjuta_set_line_num (tool, line);
	anjuta_add_marker (tool, filename, line, "CurrentLine");
	PRIV(tool)->last_file = g_strdup (filename);
	PRIV(tool)->last_line = line;
}

static void 
execution_exited_cb (GdfDebuggerClient *dbg, int exit_code, AnjutaTool *tool)
{
	GDL_TRACE ();
	
 	if (PRIV(tool)->last_file) {
		/* FIXME: only remove the marker if the document is open */
		anjuta_remove_marker (tool, PRIV(tool)->last_file, 
				    PRIV(tool)->last_line, "CurrentLine");
		g_free (PRIV(tool)->last_file);
		PRIV(tool)->last_file = NULL;
		PRIV(tool)->last_line = 0;
	}

	unload_debugger (tool);
}

static void
execution_running_cb (GdfDebuggerClient *client, AnjutaTool *tool)
{
	GDL_TRACE ();
	PRIV(tool)->running = 1;
	set_sensitivities (tool);
}

static void
state_change_cb (AnjutaTool *tool, GdfDebuggerClient *client)
{
	GDL_TRACE ();
	PRIV(tool)->running = 0;
	set_sensitivities (tool);
}

static void
connect_debugger_signals (AnjutaTool *tool)
{
	gtk_signal_connect (GTK_OBJECT (PRIV(tool)->dbg), 
			    "execution_source_line", 
			    GTK_SIGNAL_FUNC (execution_source_line_cb),
			    tool);
	gtk_signal_connect (GTK_OBJECT (PRIV(tool)->dbg), 
			    "execution_exited", 
			    GTK_SIGNAL_FUNC (execution_exited_cb),
			    tool);
	gtk_signal_connect (GTK_OBJECT (PRIV(tool)->dbg), 
			    "execution_started", 
			    GTK_SIGNAL_FUNC (execution_running_cb),
			    tool);
	gtk_signal_connect_object (GTK_OBJECT (PRIV(tool)->dbg), 
				   "execution_running", 
				   GTK_SIGNAL_FUNC (state_change_cb),
				   (gpointer)tool);
	gtk_signal_connect_object (GTK_OBJECT (PRIV(tool)->dbg), 
				   "execution_stopped", 
				   GTK_SIGNAL_FUNC (state_change_cb),
				   (gpointer)tool);
}

static void
disconnect_debugger_signals (AnjutaTool *tool)
{
}

static void
unload_debugger (AnjutaTool *tool) 
{
	disconnect_debugger_signals (tool);
	if (PRIV(tool)->dbg) {
		gtk_object_unref (GTK_OBJECT (PRIV(tool)->dbg));
	}
	PRIV(tool)->dbg = NULL;
	set_debuggers (tool);

	gdf_breakpoint_set_client_set_debugger (PRIV(tool)->bs, NULL);
	
	set_sensitivities (tool);
}

static void
load_debugger (AnjutaTool *tool)
{
	GdfDebuggerClientResult res;
	char *filename;

	unload_debugger (tool);

	filename = get_target_filename (tool, 
					PRIV(tool)->current_target);
	
	PRIV(tool)->dbg = gdf_debugger_client_new_for_file (filename);
	if (!PRIV(tool)->dbg) {
		anjuta_error_dialog (_("Unable to start a debugging server for "
				   "this target."));
		return;
	}
	
	set_debuggers (tool);
	connect_debugger_signals (tool);

	res = gdf_debugger_client_load_binary (PRIV(tool)->dbg, filename);
	if (res == GDF_DEBUGGER_CLIENT_OK) {
		gdf_breakpoint_set_client_set_debugger (PRIV(tool)->bs,
							PRIV(tool)->dbg);
		gdf_debugger_client_execute (PRIV(tool)->dbg, "");
	} else {
		anjuta_error_dialog (_("Could not load file into the debugger."));
		return;
	}
}

static GdfBreakpointSetClient *
get_breakpoint_set_for_client (AnjutaTool *tool, GBF_Target *target)
{
	GdfBreakpointSetClient *bs = NULL;

	bs = gdf_breakpoint_set_client_new ();
		
	return bs;
}

static void
unload_current_breakpoint_set (AnjutaTool *tool)
{
	if (PRIV(tool)->bs) {
		gtk_object_unref (GTK_OBJECT (PRIV(tool)->bs));
	}
	PRIV(tool)->bs = NULL;
}

static void
set_current_breakpoint_set (AnjutaTool *tool, GdfBreakpointSetClient *bs)
{
	CORBA_Environment ev;
	CORBA_Object ctrl;
	CORBA_Object pb;
	char *ior;

	unload_current_breakpoint_set (tool);
	
	CORBA_exception_init (&ev);	
	if (bs) {
		ior = CORBA_ORB_object_to_string (oaf_orb_get (),
						  bs->objref, &ev);
	} else {
		ior = CORBA_string_dup ("");	
	}
	
	ctrl = gtk_object_get_data (GTK_OBJECT (tool), 
				    "GdfBreakpointManager");
	pb = Bonobo_Control_getProperties (ctrl, &ev);
	bonobo_property_bag_client_set_value_string (pb, "breakpoint-set-ior",
						     ior, &ev);
	
	CORBA_free (ior);
	
	CORBA_exception_free (&ev);
	
	PRIV(tool)->bs = bs;	
}

static char *
find_file_in_target (AnjutaTool *tool, 
		     GNOME_Development_Target *target,
		     char *filename)
{
	CORBA_Environment ev;
	GNOME_Development_Project corba_prj;
	GbfProjectClient *prj = NULL;
	char *ret = NULL;
	
	CORBA_exception_init (&ev);
	
	corba_prj = GNOME_Development_Environment_Shell_getObject (tool->shell, 
								   "CurrentProject",
								   &ev);
	ret = NULL;
	GDL_TRACE ();
	
	if (!CORBA_Object_is_nil (corba_prj, &ev)) {
		GNOME_Development_SourceList *sources;
		GbfProjectClientResult res;
		int i;
		GDL_TRACE ();

		prj = gbf_project_client_new_from_corba (corba_prj);
		res = gbf_project_client_get_target_sources (prj,
							     target, 
							     &sources);
		if (res == GBF_PROJECT_CLIENT_OK) {
			GDL_TRACE ();
			for (i = 0; i < sources->_length;i++) {
				GNOME_Development_Source *s = &sources->_buffer[i];
				GDL_TRACE_EXTRA ("%s", s->name);
				
				if (!strcmp (s->name, filename)) {
					char *root;
					gbf_project_client_get_project_root (prj, &root);
					
					ret = g_strdup_printf ("%s/%s/%s",
							       root,
							       s->path,
							       s->name);
					CORBA_free (root);
					break;
				}
			}
			CORBA_free (sources);
		} else {
			GDL_TRACE ();
		}
		gtk_object_unref (GTK_OBJECT (prj));
	}	

	CORBA_exception_free (&ev);

	return ret;
}


/* Find the absolute path for a given file returned by the debugger */
static char *
get_debug_filename (AnjutaTool *tool,
		    GNOME_Development_Target *current_target, 
		    char *filename)
{
	char *ret;
	
	g_assert (filename);

	if (filename[0] == '\0' || !strcmp (filename, "???")) {
		return NULL;
	}
	
	if (g_path_is_absolute (filename)) {
		/* If the filename is already absolute, return it unchanged */
		ret = g_strdup (filename);
	} else {
		/* FIXME: If there is an existing debugger, query it
		 * for the file */
		
		/* Otherwise search for it in the project */
		ret = find_file_in_target (tool, current_target, filename);
	}

	return ret;
}

static void
breakpoint_disabled_cb (GdfBreakpointSetClient *bs, gint bp_num, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	GNOME_Development_Breakpoint *bp;
	char *filename;
	GdfBreakpointSetClientResult res;
	
	res = gdf_breakpoint_set_client_get_breakpoint_info (bs, bp_num, &bp);
	
	if (res == GDF_BREAKPOINT_SET_CLIENT_OK) {
		filename = get_debug_filename (tool, PRIV(tool)->current_target,
					       bp->file_name);
		if (filename && g_file_test (filename, G_FILE_TEST_EXISTS | G_FILE_TEST_ISFILE)) {
			
			anjuta_remove_marker (tool, filename, 
					    bp->line_num, "Breakpoint");
			anjuta_add_marker (tool, filename,
					 bp->line_num, "DisabledBreakpoint");
		}	
		if (filename) {
			g_free (filename);
		}
	}
}

static void
breakpoint_enabled_cb (GdfBreakpointSetClient *bs, gint bp_num, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	GNOME_Development_Breakpoint *bp;
	char *filename;
	GdfBreakpointSetClientResult res;
	
	res = gdf_breakpoint_set_client_get_breakpoint_info (bs, bp_num, &bp);
	
	if (res == GDF_BREAKPOINT_SET_CLIENT_OK) {
		filename = get_debug_filename (tool, PRIV(tool)->current_target,
					       bp->file_name);
		if (filename && g_file_test (filename, G_FILE_TEST_EXISTS | G_FILE_TEST_ISFILE)) {
			
			anjuta_remove_marker (tool, filename, 
					    bp->line_num, 
					    "DisabledBreakpoint");
			anjuta_add_marker (tool, filename,
					 bp->line_num, "Breakpoint");
		}
		if (filename) {
			g_free (filename);
		}
	}
}

static void
breakpoint_deleted_cb (GdfBreakpointSetClient *bs, gint bp_num, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	GNOME_Development_Breakpoint *bp;
	char *filename;
	GdfBreakpointSetClientResult res;
	
	res = gdf_breakpoint_set_client_get_breakpoint_info (bs, bp_num, &bp);
	
	if (res == GDF_BREAKPOINT_SET_CLIENT_OK) {
		filename = get_debug_filename (tool, PRIV(tool)->current_target,
					       bp->file_name);
		if (filename && g_file_test (filename, G_FILE_TEST_EXISTS | G_FILE_TEST_ISFILE)) {
			if (bp->enabled) {
				anjuta_remove_marker (tool, filename, 
						    bp->line_num, 
						    "Breakpoint");
			} else {
				anjuta_remove_marker (tool, filename,
						    bp->line_num,
						    "DisabledBreakpoint");
			}
		}	
		if (filename) {
			g_free (filename);
		}
	}
}

static void
breakpoint_set_cb (GdfBreakpointSetClient *bs, gint bp_num, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	GNOME_Development_Breakpoint *bp;
	char *filename;
	GdfBreakpointSetClientResult res;

	res = gdf_breakpoint_set_client_get_breakpoint_info (bs, bp_num, &bp);

	if (res == GDF_BREAKPOINT_SET_CLIENT_OK) {
		filename = get_debug_filename (tool, PRIV(tool)->current_target, 
					       bp->file_name);
		
		if (filename && g_file_test (filename, G_FILE_TEST_EXISTS | G_FILE_TEST_ISFILE)) {
			anjuta_show_file (tool, filename);
			anjuta_set_line_num (tool, bp->line_num);
			anjuta_add_marker (tool, filename, 
					 bp->line_num, "Breakpoint");
		} else {
			char *msg = g_strdup_printf (_("Could not find file %s"), 
						     bp->file_name);
			anjuta_error_dialog (msg);
			g_free (msg);
		}
		if (filename) {
			g_free (filename);
		}	
	} else {
		g_warning (_("Couldn't get breakpoint info for a breakpoint that was just set."));
	}
	
}

static void
unload_current_target (AnjutaTool *tool)
{
	if (PRIV (tool)->current_target_any) {
		CORBA_free (PRIV(tool)->current_target_any);		
	}

	PRIV(tool)->current_target = NULL;
	PRIV(tool)->current_target_any = NULL;
}

static void
set_current_target (AnjutaTool *tool, CORBA_any *target)
{
	GdfBreakpointSetClient *bs;

	unload_current_target (tool);

	if (target) {
		PRIV(tool)->current_target_any = target;
		PRIV(tool)->current_target = target->_value;
		
		bs = get_breakpoint_set_for_client (tool, target->_value);
		if (!bs) {
			g_warning ("Could not create a breakpoint set.\n");
			return;
		}
		
		gtk_signal_connect (GTK_OBJECT (bs), "breakpoint_set",
				    GTK_SIGNAL_FUNC (breakpoint_set_cb),
				    tool);
		gtk_signal_connect (GTK_OBJECT (bs), "breakpoint_disabled", 
				    GTK_SIGNAL_FUNC (breakpoint_disabled_cb) ,
				    tool);
		gtk_signal_connect (GTK_OBJECT (bs), "breakpoint_deleted", 
				    GTK_SIGNAL_FUNC (breakpoint_deleted_cb) ,
				    tool);
		gtk_signal_connect (GTK_OBJECT (bs), "breakpoint_enabled", 
				    GTK_SIGNAL_FUNC (breakpoint_enabled_cb),
				    tool);
		
		set_current_breakpoint_set (tool, bs);	
	} else {
		set_current_breakpoint_set (tool, NULL);
	}
	set_sensitivities (tool);
}

/* Shell event handlers */

static void
data_added_cb (BonoboListener *listener, CORBA_any *any, CORBA_Environment *ev,
	       gpointer user_data)
{
	AnjutaTool *tool = ANJUTA_TOOL (user_data);
	CORBA_char *name = BONOBO_ARG_GET_STRING (any);
	if (!strcmp (name, "CurrentTarget")) {
		CORBA_any *data;
		data = GNOME_Development_Environment_Shell_getData (tool->shell, 
								    "CurrentTarget",
								    ev);
		set_current_target (tool, data);		
	}
}

static void
data_removed_cb (BonoboListener *listener, CORBA_any *any,
		 CORBA_Environment *ev, gpointer user_data)
{
	AnjutaTool *tool = ANJUTA_TOOL (user_data);
	CORBA_char *name = BONOBO_ARG_GET_STRING (any);
	if (!strcmp (name, "CurrentTarget")) {
		set_current_target (tool, NULL);		
	}
}

static void
shell_event_cb (BonoboListener *listener, const char *event,
		CORBA_any *any, CORBA_Environment *ev,
		gpointer user_data)
{
	if (!strcmp (event, "data_added")) {
		data_added_cb (listener, any, ev, user_data);
	}

	if (!strcmp (event, "data_removed")) {
		data_removed_cb (listener, any, ev, user_data);
	}
}

/* Listener setup and destruction. */

static gboolean
connect_listeners (AnjutaTool *tool) 
{
	CORBA_Environment ev;
	BonoboListener *listener; /* FIXME: Memory management of this object */
	CORBA_Object source;
	CORBA_exception_init (&ev);	
	
	source = Bonobo_Unknown_queryInterface (tool->shell,
						"IDL:Bonobo/EventSource:1.0",
						&ev);
	
	if (!CORBA_Object_is_nil (source, &ev) && ev._major == CORBA_NO_EXCEPTION) {
		listener = bonobo_listener_new (NULL, NULL);
		PRIV(tool)->listener_id = 
			Bonobo_EventSource_addListener (source, 
							BONOBO_OBJREF (listener),
							&ev);
	} else {
		g_warning ("couldn't get event source for the shell.");
		return FALSE;
	}
	
	PRIV(tool)->shell_event_source = source;
	gtk_signal_connect (GTK_OBJECT (listener), "event_notify",
			    GTK_SIGNAL_FUNC (shell_event_cb), tool);
	
	CORBA_exception_free (&ev);
	return TRUE;
}

static void
disconnect_listeners (AnjutaTool *tool) 
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);
	Bonobo_EventSource_removeListener (PRIV(tool)->shell_event_source, 
					   PRIV(tool)->listener_id, 
					   &ev);
	bonobo_object_release_unref (PRIV(tool)->shell_event_source, &ev);
	CORBA_exception_free (&ev);
}
#endif
/* Control setup and destruction */

static void
add_debug_control (AnjutaTool *tool, GdfControl *control_desc)
{
	CORBA_Environment ev;
	Bonobo_Control ctrl;
	
	CORBA_exception_init (&ev);
	ctrl = oaf_activate_from_id (control_desc->iid, 0, NULL, &ev);
	if (!CORBA_Object_is_nil (ctrl, &ev)) {
		GNOME_Development_Environment_Shell_addControl (tool->shell,
								ctrl, 
								control_desc->name,
								control_desc->title,
								control_desc->loc,
								&ev);
		gtk_object_set_data (GTK_OBJECT (tool), 
				     control_desc->name, ctrl);
	} else {
		g_warning ("Could not load %s", control_desc->name);
	}
	

	CORBA_exception_free (&ev);
}

static gboolean
add_controls (AnjutaTool *tool) 
{
	GdfControl *ctrl = controls;
		
	while (ctrl->iid) {
		add_debug_control (tool, ctrl);
		ctrl++;
	}

	return TRUE;
}

static void
remove_controls (AnjutaTool *tool)
{
	CORBA_Environment ev;
	GdfControl *ctrl = controls;
	
	CORBA_exception_init (&ev);
	while (ctrl->iid) {
		CORBA_Object obj;
		
		obj = gtk_object_get_data (GTK_OBJECT (tool), ctrl->name);
		Bonobo_Unknown_unref (obj, &ev);
		
		GNOME_Development_Environment_Shell_removeObject (tool->shell,
								  ctrl->name,
								  &ev);
		ctrl++;
	}
	CORBA_exception_free (&ev);
}

#if 0
static void
set_sensitivities (AnjutaTool *tool)
{
	BonoboUIComponent *uic = PRIV(tool)->uic;
	if (PRIV(tool)->dbg) {
		/* gnome-debug is not good at dealing with calls to
		 * the debugger (including get_state) while the program
		 * is executing.  So we disable everything while executing */
		if (PRIV(tool)->running) {
			bonobo_ui_component_set_prop (uic,
						      "/commands/Debug",
						      "sensitive", "0", NULL);
			bonobo_ui_component_set_prop (uic,
						      "/commands/Stop", 
						      "sensitive", "0", NULL);
			bonobo_ui_component_set_prop (uic, 
						      "/commands/StepInto", 
						      "sensitive", "0", NULL);
			bonobo_ui_component_set_prop (uic, 
						      "/commands/StepOver", 
						      "sensitive", "0", NULL);
			bonobo_ui_component_set_prop (uic,
						      "/commands/StepOut", 
						      "sensitive", "0", NULL);
			bonobo_ui_component_set_prop (uic,
						      "/commands/SetBreakpoint", 
						      "sensitive", "0", NULL);
		} else {
			GNOME_Development_Debugger_StateFlag state;
			state = gdf_debugger_client_get_state (PRIV(tool)->dbg);

			/* anjuta only keeps Debugger objects around for
			 * the lifetime of the target, so this state
			 * should always be set whenever this
			 * function is invoked. */
			g_assert (state & GNOME_Development_Debugger_BINARY_LOADED);
			bonobo_ui_component_set_prop (uic,
						      "/commands/Debug",
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic,
						      "/commands/Stop", 
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic, 
						      "/commands/StepInto", 
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic, 
						      "/commands/StepOver", 
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic,
						      "/commands/StepOut", 
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic,
						      "/commands/SetBreakpoint", 
						      "sensitive", "1", NULL);
		}
	} else {
		if (PRIV(tool)->current_target && PRIV(tool)->current_target->type == GNOME_Development_PROGRAM) {
			bonobo_ui_component_set_prop (uic, "/commands/Debug", 
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic,
						      "/commands/SetBreakpoint", 
						      "sensitive", "1", NULL);

		} else {
			bonobo_ui_component_set_prop (uic, "/commands/Debug",
						      "sensitive", "0", NULL);
			bonobo_ui_component_set_prop (uic,
						      "/commands/SetBreakpoint", 
						      "sensitive", "0", NULL);
		}
		bonobo_ui_component_set_prop (uic, "/commands/Stop", 
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/StepInto", 
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/StepOver", 
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/StepOut", 
					      "sensitive", "0", NULL);
	}
}
#endif

static void
merge_ui (AnjutaTool *tool)
{
	BonoboUIComponent *uic;
	
	uic = bonobo_ui_component_new ("anjuta-debugger");
	bonobo_ui_component_set_container (uic, tool->ui_container);
	bonobo_ui_util_set_ui (uic, ANJUTA_DATADIR, "anjuta-debugger.xml", 
			       "anjuta-debugger");
#if 0
	bonobo_ui_component_add_verb_list_with_data (uic, verbs, tool);
#endif

	PRIV(tool)->uic = uic;

#if 0
	set_sensitivities (tool);
#endif
}

static gboolean
impl_init (AnjutaTool *tool, 
	   gpointer closure)
{
	CORBA_Environment ev;
	
	g_return_val_if_fail (tool != NULL, FALSE);
	g_return_val_if_fail (ANJUTA_IS_TOOL (tool), FALSE);

	CORBA_exception_init (&ev);

	merge_ui (tool);
	
	if (!add_controls (tool)) {
		g_warning ("Could not start the debugger tool: Couldn't create controls.");
		CORBA_exception_free (&ev);
		return FALSE;
	}

#if 0
	if (!connect_listeners (tool)) {
		g_warning ("Could not start the debugger tool: Couldn't connect to shell event source.");
		remove_controls (tool);
		CORBA_exception_free (&ev);
		return FALSE;
	}
#endif

	CORBA_exception_free (&ev);

	return TRUE;
}

static void
impl_cleanup (AnjutaTool *tool,
	      gpointer closure)
{
	remove_controls (tool);
#if 0
	disconnect_listeners (tool);
#endif
	
	/* g_free (tool->data); */
	bonobo_ui_component_unset_container(PRIV(tool)->uic);
}

ANJUTA_SHLIB_TOOL (DEBUGGER_COMPONENT_IID, "Anjuta Debugger Plugin",
		   impl_init, impl_cleanup, g_new0 (DebuggerToolPriv, 1));
