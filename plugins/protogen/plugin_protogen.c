/*
 * Protogen plugin
 *
 * Authors:
 *   Steffen Kern (alfi@pn.org)
 *   Dirk Vangestel (dirk.vangestel@advalvas.be)
 */

#include "../../src/gide.h"
#include "../../src/gI_plugin.h"
#include "../../src/gI_tools.h"
#include "../../src/gI_window.h"
#include "../../src/anjuta-tools.h"
#include "protogen.h"

#define PROTOGEN_NAME			_("Tools - Prototype generator")
#define PROTOGEN_TITLE			_("Prototype generator Plugin")
#define PROTOGEN_DESCR			_("Generate function prototypes.")

static void
plugin_protogen(
	Tool*				tool,
	ToolState*			state
)
{
	gen_proto();
}

static gboolean
plugin_protogen_sens(
	Tool*				tool,
	ToolState*			state
)
{
	return TRUE;
}

static int
can_unload(
	PluginData*			pd
)
{
	return 1;
}

static void
cleanup_plugin(
	PluginData*			pd
)
{
	anjuta_tool_remove(PROTOGEN_NAME);
}

PluginInitResult
init_plugin(
	CommandContext*			context,
	PluginData*			pd
)
{
	GtkObject*			tool;

	if(plugin_version_mismatch(context, pd, VERSION))
	{
		return PLUGIN_QUIET_ERROR;
	}

	tool = gI_tool_new(PROTOGEN_NAME, (void*)plugin_protogen);
	gI_tool_set_menu_data(TOOL(tool), plugin_protogen_sens,
		"Tools/Prototype generator...", NULL);
	anjuta_tool_add(TOOL(tool));

	if(plugin_data_init(pd, can_unload, cleanup_plugin,
		PROTOGEN_TITLE, PROTOGEN_DESCR))
	{
		return PLUGIN_OK;
	}
	else
	{
		return PLUGIN_ERROR;
	}
}
