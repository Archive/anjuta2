/* $Header$ */
/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gide.h"
#include "parser.h"

glong
c_parse_special(
	gchar*				buf,
	glong				cc,
	c_status*			c_status
)
{
	if(cc > 0 && buf[cc] == '*' && buf[cc - 1] == '/' &&
		!c_status->hyph && !c_status->dhyph)
	{
		/* Handle opening c-stype comment */
		c_status->comment++;
	}
	else if(cc > 0 && buf[cc] == '/' && buf[cc - 1] == '*' &&
		!c_status->hyph && !c_status->dhyph)
	{
		/* Handle closing c-style comment */
		c_status->comment--;
	}
	else if(cc > 0 && buf[cc] == '/' && buf[cc - 1] == '/' &&
		!c_status->hyph && !c_status->dhyph)
	{
		/* c++ style comment, terminate string at the beginning of it */
		buf[cc - 1] = '\0';
	}
	else if(cc > 1 && buf[cc] == '\'' && !c_status->dhyph)
	{
		if((buf[cc - 1] == '\\' && buf[cc - 2] == '\\') ||
			(buf[cc - 1] != '\\'))
		{
			/* Handle non-escaped single hyphen */
			if(c_status->hyph)
			{
				c_status->hyph = 0;
			}
			else
			{
				c_status->hyph = 1;
			}
		}
	}
	else if(cc > 1 && buf[cc] == '"' && !c_status->hyph)
	{
		if((buf[cc - 1] == '\\' && buf[cc - 2] == '\\') ||
			(buf[cc - 1] != '\\'))
		{
			/* Handle non-escaped double hyphen */
			if(c_status->dhyph)
			{
				c_status->dhyph = 0;
			}
			else
			{
				c_status->dhyph = 1;
			}
		}
	}
	else if(!c_status->comment && !c_status->hyph && !c_status->dhyph)
	{
		if(buf[cc] == '{')
		{
			c_status->curly_brackets++;
		}
		else if(buf[cc] == '}')
		{
			c_status->curly_brackets--;
		}
		else if(buf[cc] == '(')
		{
			c_status->round_brackets++;
		}
		else if(buf[cc] == ')')
		{
			c_status->round_brackets--;
		}
		else if(buf[cc] == '[')
		{
			c_status->square_brackets++;
		}
		else if(buf[cc] == ']')
		{
			c_status->square_brackets--;
		}
	}

	return 1;
}


glong
init_cstatus(
	c_status*			c_status
)
{
	c_status->curly_brackets = 0;
	c_status->round_brackets = 0;
	c_status->square_brackets = 0;
	c_status->comment = 0;
	c_status->hyph = 0;
	c_status->dhyph = 0;

	return 1;
}
