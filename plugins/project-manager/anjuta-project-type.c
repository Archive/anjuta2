/* Anjuta
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-macros.h>
#include "anjuta-project-type.h"

GNOME_CLASS_BOILERPLATE (AnjutaProjectType, anjuta_project_type, GObject, G_TYPE_OBJECT);

const char *
anjuta_project_type_get_category_name (AnjutaProjectType *type)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_PROJECT_TYPE (type), NULL);

	return ANJUTA_PROJECT_TYPE_GET_CLASS (type)->get_category_name (type);
}

const char *
anjuta_project_type_get_project_name (AnjutaProjectType *type)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_PROJECT_TYPE (type), NULL);

	return ANJUTA_PROJECT_TYPE_GET_CLASS (type)->get_project_name (type);
}

const char *
anjuta_project_type_get_description (AnjutaProjectType *type)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_PROJECT_TYPE (type), NULL);

	return ANJUTA_PROJECT_TYPE_GET_CLASS (type)->get_description (type);
}

const char *
anjuta_project_type_get_backend (AnjutaProjectType *type)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_PROJECT_TYPE (type), NULL);

	return ANJUTA_PROJECT_TYPE_GET_CLASS (type)->get_backend (type);
}

int
anjuta_project_type_get_druid_page_count (AnjutaProjectType *type,
					  GError **error)
{
	g_return_val_if_fail (type != NULL, -1);
	g_return_val_if_fail (ANJUTA_IS_PROJECT_TYPE (type), -1);
	g_return_val_if_fail (error == NULL || *error == NULL, -1);

	return ANJUTA_PROJECT_TYPE_GET_CLASS (type)->get_druid_page_count (type,
									   error);
}

GtkWidget *
anjuta_project_type_get_druid_page (AnjutaProjectType *type,
				    int page,
				    GError **error)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_PROJECT_TYPE (type), NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	return ANJUTA_PROJECT_TYPE_GET_CLASS (type)->get_druid_page (type,
								     page,
								     error);
}

void
anjuta_project_type_create_project (AnjutaProjectType *type,
				    AnjutaProjectInfo *info,
				    GError **error)
{
	g_return_if_fail (type != NULL);
	g_return_if_fail (ANJUTA_IS_PROJECT_TYPE (type));
	g_return_if_fail (info != NULL);
	g_return_if_fail (error == NULL || *error == NULL);

	ANJUTA_PROJECT_TYPE_GET_CLASS (type)->create_project (type,
							      info,
							      error);
}

static void
anjuta_project_type_class_init (AnjutaProjectTypeClass *klass)
{
	parent_class = g_type_class_peek_parent (klass);
}

static void
anjuta_project_type_instance_init (AnjutaProjectType *type)
{
}
