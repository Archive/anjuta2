/* Anjuta
 * Copyright (C) 2001 JP Rosevear
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <libxml/tree.h>
#include <bonobo/bonobo-ui-util.h>
#include <bonobo/bonobo-file-selector-util.h>
#include <gconf/gconf-client.h>
#include <gdl/gdl.h>
#include <gbf/gbf-backend.h>
#include <gbf/gbf-project-model.h>
#include <gbf/gbf-project-view.h>
#include <gbf/gbf-build-info.h>
#include <gbf/gbf-project-util.h>
#include <glade/glade-xml.h>
#include <libanjuta/libanjuta.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomevfs/gnome-vfs.h>
#include "project-druid.h"
#include "anjuta-project-manager.h"

#define RECENT_PROJECTS_LIMIT "/apps/anjuta2/plugins/project_manager/recent_projects_limit"

typedef struct {
	AnjutaTool parent;

	AnjutaProjectManager *manager;

	GtkWidget *project_view;
	GbfProjectModel *project_model;

	GtkWidget *build_info;

	GbfProject *project;
	GbfProjectTarget *current_target;
	gchar *project_root;
	xmlDocPtr project_doc;

	GdlRecent *recent;
} ProjectTool;

typedef struct {
	AnjutaToolClass parent;
} ProjectToolClass;

static void
open_file (ProjectTool *tool,
	   const char  *filename,
	   int          line_num)
{
	gboolean res;

	res = anjuta_show_file (ANJUTA_TOOL (tool), filename);
	if (!res) {
		anjuta_dialog_error (_("Unable to open file."));
		return;
	}

	if (line_num != 0)
		anjuta_set_line_num (ANJUTA_TOOL (tool), line_num);
}

static void
set_current_target (ProjectTool *tool, GbfProjectTarget *target)
{
	if (tool->current_target) {
		anjuta_shell_remove_value (ANJUTA_TOOL (tool)->shell, 
					   "ProjectManager::CurrentTarget",
					   NULL);

		gbf_project_target_free (tool->current_target);
		tool->current_target = NULL;
	}

	if (target) {
		tool->current_target = target;
		anjuta_shell_add (ANJUTA_TOOL (tool)->shell,
				  "ProjectManager::CurrentTarget",
				  G_TYPE_POINTER,
				  tool->current_target,
				  NULL);
	}
}

static void
update_build_commands (AnjutaTool *tool,
		       gboolean    enable)
{
	const char *sensitivity = enable ? "1" : "0";

	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildPrepare",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildConfigure",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildClean",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildAll",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildInstall",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildRun",
				      "sensitive", sensitivity, NULL);
}

static void
build_start_cb (GbfProject *project,
		gpointer    user_data)
{
	update_build_commands (ANJUTA_TOOL (user_data), FALSE);
}

static void
build_stop_cb (GbfProject *project,
	       gboolean    success,
	       gpointer    user_data)
{
	update_build_commands (ANJUTA_TOOL (user_data), TRUE);
}

static void
set_build (ProjectTool *proj_tool, const gchar *path)
{
	AnjutaTool *tool = ANJUTA_TOOL (proj_tool);
	GbfProject *project = NULL;
	GSList *l;
	GError *err = NULL;
	const char *sensitivity;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (ANJUTA_IS_TOOL (tool));

	set_current_target (proj_tool, NULL);

	/* Close any open project first. */
	if (proj_tool->project) {
		g_object_set (G_OBJECT (proj_tool->project_model),
			      "project", NULL, NULL);
		g_object_set (G_OBJECT (proj_tool->build_info),
			      "project", NULL, NULL);

		anjuta_shell_remove_value (tool->shell, 
					   "ProjectManager::CurrentProject",
					   NULL);
		g_object_unref (proj_tool->project);
		proj_tool->project = NULL;

		g_free (proj_tool->project_root);
		proj_tool->project_root = NULL;

		xmlFreeDoc (proj_tool->project_doc);
		proj_tool->project_doc = NULL;
	}

	if (path != NULL) {
		char *basename;
		xmlChar *backend_id = NULL;
		xmlNodePtr node, prj_node = NULL;
		GbfBackend *backend = NULL;
		char *dirname;

		/* Check if the path contains a file, it exists and is has a 
		 * .anjuta extension. */		
		basename = g_path_get_basename (path);
		if (!g_file_test (path, G_FILE_TEST_EXISTS) ||
		    !g_str_has_suffix (basename, ".anjuta")) {
			char *msg = g_strdup_printf (_("File '%s' is not an anjuta2 project file"),
						     path);
			g_free (basename);
			anjuta_dialog_error (msg);
			g_free (msg);
			return;
		}
		g_free (basename);

		/* Load the .anjuta file. */
		proj_tool->project_doc = xmlParseFile (path);
		if (!proj_tool->project_doc) {
			char *msg = g_strdup_printf (_("Unable to parse project file '%s'"),
						     path);
			anjuta_dialog_error (msg);
			g_free (msg);
			return;
		}

		/* Find the <project> node. */
		node = proj_tool->project_doc->xmlChildrenNode;
		while (node) {
			if (!xmlStrcmp (node->name, (const xmlChar *) "project")) {
				prj_node = node;
				break;
			}
			node = node->next;
		}
		if (!prj_node) {
			char *msg = g_strdup_printf (_("File '%s' is not a valid anjuta2 project file"),
						     path);
			anjuta_dialog_error (msg);
			g_free (msg);
			xmlFreeDoc (proj_tool->project_doc);
			return;
		}
		
		/* Get the backend id from the document. */
		node = prj_node->xmlChildrenNode;
		while (node) {
			if (!xmlStrcmp (node->name, (const xmlChar *) "backend")) {
				backend_id = xmlNodeGetContent (node);
				break;
			}
			node = node->next;
		}
		if (!backend_id) {
			char *msg = g_strdup_printf (_("File '%s' is not a valid anjuta2 project file"),
						     path);
			anjuta_dialog_error (msg);
			g_free (msg);
			xmlFreeDoc (proj_tool->project_doc);
			return;
		}

		gbf_backend_init ();

		for (l = gbf_backend_get_backends (); l; l = l->next) {
			backend = l->data;
			if (!strcmp (backend->id, backend_id))
				break;
			backend = NULL;
		}

		if (!backend) {
			char *msg = g_strdup_printf (_("Project backend '%s' not found"),
						     backend_id);
			anjuta_dialog_error (msg);
			g_free (msg);
			xmlFreeDoc (proj_tool->project_doc);
			xmlFree (backend_id);
			return;
		}
		xmlFree (backend_id);

		project = gbf_backend_new_project (backend->id);
		if (!project) {
			anjuta_dialog_error (_("Could not create project object"));
			return;
		}

		/* Try to load the project */
		dirname = g_path_get_dirname (path);
		gbf_project_load (project, dirname, &err);
		g_free (dirname);
		if (err != NULL) {
			/* FIXME: Add better error reporting. */
			gchar *str;
			if (g_error_matches (err, gbf_project_error_quark (),
					     GBF_PROJECT_ERROR_DOESNT_EXIST)) {
				str = g_strdup_printf (_("No project found at location '%s'"),
						       path);
			} else if (g_error_matches (err, gbf_project_error_quark (),
						    GBF_PROJECT_ERROR_PROJECT_MALFORMED)) {
				str = g_strdup_printf (_("Not a valid project: '%s'"),
						       err->message);
			} else {
				str = g_strdup_printf (_("Unable to load project: '%s'"),
						       err->message);
			}
			anjuta_dialog_error (str);
			g_free (str);
			g_error_free (err);
			return;
		}

		proj_tool->project = project;
		anjuta_shell_add (tool->shell, 
				  "ProjectManager::CurrentProject",
				  G_TYPE_POINTER,
				  project,
				  NULL);

		/* save root for future reference */
		proj_tool->project_root = g_strdup (path);

		/* Connect to build signals. */
		g_signal_connect (G_OBJECT (project), "build_start",
				  G_CALLBACK (build_start_cb), proj_tool);
		g_signal_connect (G_OBJECT (project), "build_stop",
				  G_CALLBACK (build_stop_cb), proj_tool);
	}

	g_object_set (G_OBJECT (proj_tool->project_model), "project", project, NULL);
	g_object_set (G_OBJECT (proj_tool->build_info), "project", project, NULL);

	/* Update GUI. */
	sensitivity = project != NULL ? "1" : "0";
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectClose",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectNewGroup",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectNewTarget",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectAddSource",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectRemoveSource",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildParameters",
				      "sensitive", sensitivity, NULL);
	update_build_commands (tool, project != NULL);
}

static void
recent_project (GdlRecent  *recent,
		const char *uri,
		gpointer    data)
{
	ProjectTool *proj_tool = (ProjectTool*) data;

	set_build (proj_tool, uri);
}

static void
cancel_cb (GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy (GTK_WIDGET (data));
}

static void
finish_cb (GtkWidget *widget, ProjectTool *proj_tool)
{
	AnjutaProjectDruid *druid = ANJUTA_PROJECT_DRUID (widget);
	AnjutaProjectInfo *info;
	GError *err = NULL;
	xmlDocPtr doc;
	char *filename;

	info = anjuta_project_druid_get_info (druid);
	gtk_widget_destroy (widget->parent);
	
	anjuta_project_type_create_project (info->type, info, &err);
	if (err != NULL) {
		anjuta_dialog_error (_("Error creating project"));
		g_error_free (err);
		return;
	}

	doc = xmlNewDoc ("1.0");
	doc->children = xmlNewDocNode (doc, NULL, "project", NULL);
	xmlNewChild (doc->children, NULL, "backend",
		     anjuta_project_type_get_backend (info->type));

	filename = g_strconcat (info->location, "/", info->package, ".anjuta", NULL);

	xmlSaveFormatFile (filename, doc, TRUE);
	xmlFreeDoc (doc);

	set_build (proj_tool, filename);
	gdl_recent_add (proj_tool->recent, filename);
	g_free (filename);

	g_free (info->name);
	g_free (info->package);
	g_free (info->description);
	g_free (info->location);
	g_free (info);
}

static void 
project_new (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = (AnjutaTool *)data;
	ProjectTool *proj_tool = (ProjectTool*) data;
	GtkWidget *druid;
	GtkWidget *window;

	window = g_object_new (GTK_TYPE_WINDOW, 
			       "type", GTK_WINDOW_TOPLEVEL,
			       "title", _("Anjuta Project Druid"),
			       "modal", TRUE,
			       NULL);

	druid = anjuta_project_druid_new 
		(anjuta_project_manager_get_project_types (proj_tool->manager));

	g_signal_connect (druid, "cancel", G_CALLBACK (cancel_cb), window);
	g_signal_connect (druid, "finish", G_CALLBACK (finish_cb), data);

	gtk_container_add (GTK_CONTAINER (window), druid);
	gtk_window_set_transient_for (GTK_WINDOW (window), GTK_WINDOW (tool->shell));
	gtk_window_set_modal (GTK_WINDOW (window), TRUE);
	gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER_ON_PARENT);

	gtk_widget_show (window);
}

static void
backend_set_text (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer *cell,
		  GtkTreeModel *model,
		  GtkTreeIter *iter,
		  gpointer data)
{
	GbfBackend *backend;

	gtk_tree_model_get (model, iter, 0, &backend, -1);
	g_object_set (GTK_CELL_RENDERER (cell), "text", backend->description, NULL);
}

static void
project_import (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = (AnjutaTool *) data;
	ProjectTool *proj_tool = (ProjectTool *) data;
	char *uri;
	GSList *l;
	GbfBackend *backend = NULL;
	GError *err = NULL;
	GSList *backends = NULL;
	GbfProject *project;
	char *path, *dirname;

	uri = bonobo_file_selector_open (NULL, TRUE, 
					 _("Open Project..."), 
					 NULL, NULL);

	if (uri) {
		path = gnome_vfs_get_local_path_from_uri (uri);
		if (!path) {
			anjuta_dialog_error (_("Only local uris are supported for now"));
			return;
		}
		dirname = g_path_get_dirname (path);

		gbf_backend_init ();

		for (l = gbf_backend_get_backends (); l; l = l->next) {
			backend = l->data;

			project = gbf_backend_new_project (backend->id);
			if (!project) {
				g_message ("Could not create project: %s", backend->id);
				continue;
			}

			/* Probe to see if the backend can load the project. */
			if (gbf_project_probe (project, dirname, &err)) {
				backends = g_slist_append (backends, backend);
			}

			g_object_unref (project);
		}

		if (g_slist_length (backends) == 0) {
			anjuta_dialog_error (_("No backends available which can import this project"));
		} else {
			GtkWidget *dialog;
			GtkWidget *tree;
			GtkTreeModel *model;
			GtkCellRenderer *renderer;
			GtkTreeViewColumn *column;
			GtkTreeIter iter;

			char *file = g_strconcat (DATADIR, "/anjuta2/glade/", 
						  "anjuta-project-manager.glade", NULL);
			GladeXML *gui = glade_xml_new (file, "project_import_dialog", NULL);
			g_free (file);
			if (!gui) {
				g_error ("Could not find project-druid.glade, reinstall anjuta");
				return;
			}

			dialog = glade_xml_get_widget (gui, "project_import_dialog");
			tree = glade_xml_get_widget (gui, "backend_tree");

			model = GTK_TREE_MODEL (gtk_list_store_new (1, G_TYPE_POINTER));
			gtk_tree_view_set_model (GTK_TREE_VIEW (tree), model);
			column = gtk_tree_view_column_new ();
			gtk_tree_view_column_set_title (column, "Backends");
			renderer = gtk_cell_renderer_text_new ();
			gtk_tree_view_column_pack_start (column, renderer, TRUE);
			gtk_tree_view_column_set_cell_data_func (column, renderer,
								 backend_set_text,
								 NULL, NULL);
			gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

			for (l = backends; l != NULL; l = l->next) {
				backend = l->data;
				gtk_list_store_append (GTK_LIST_STORE (model),
						       &iter);
				gtk_list_store_set (GTK_LIST_STORE (model),
						    &iter, 0, backend, -1);
			}

			gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (tool->shell));
			gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
			gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER_ON_PARENT);
			if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
				xmlDocPtr doc;
				char *filename, *save_uri;
				GtkTreeSelection *treesel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
				gtk_tree_selection_get_selected (treesel, NULL, &iter);
				gtk_tree_model_get (model, &iter, 0, &backend, -1);

				doc = xmlNewDoc ("1.0");
				doc->children = xmlNewDocNode (doc, NULL, "project", NULL);
				xmlNewChild (doc->children, NULL, "backend", backend->id);

				filename = g_strconcat ("project", ".anjuta", NULL);
				save_uri = bonobo_file_selector_save (GTK_WINDOW (tool->shell),
								      TRUE, _("Save Project"),
								      NULL, dirname, filename);
				g_free (filename);

				if (save_uri) {
					xmlSaveFormatFile (save_uri, doc, TRUE);
					xmlFreeDoc (doc);
					filename = gnome_vfs_get_local_path_from_uri (save_uri);
					set_build (proj_tool, filename);
					g_free (filename);
					g_free (save_uri);
				}
			}
			gtk_widget_destroy (dialog);
		}

		g_free (path);
		g_free (dirname);
		g_free (uri);
	}
}

static void 
project_open (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = (AnjutaTool *)data;
	ProjectTool *proj_tool = (ProjectTool*)data;
	char *uri;

	uri = bonobo_file_selector_open (GTK_WINDOW (tool->shell), TRUE,
					 _("Open Project"), NULL, NULL);

	if (uri) {
		/* FIXME: Use gnome-vfs throughout this file instead of
		 * converting every file:// uri to a local path (because
		 * g_file_test doesn't accept uris; libxml2 probably doesn't
		 * either). */
		char *path = gnome_vfs_get_local_path_from_uri (uri);
		if (!path) {
			anjuta_dialog_error (_("Only local uris are supported for now"));
			return;
		}

		set_build (proj_tool, path);
		gdl_recent_add (proj_tool->recent, path);

		g_free (path);
		g_free (uri);
	}
}

static void 
project_close (GtkWidget *widget, gpointer data)
{
	ProjectTool *proj_tool = (ProjectTool *) data;

	set_build (proj_tool, NULL);
}

static GtkWindow *
try_get_toplevel_widget (GtkWidget *widget)
{
	GtkWidget *toplevel;

	if (!widget)
		return NULL;

	toplevel = gtk_widget_get_toplevel (widget);
	if (GTK_WIDGET_TOPLEVEL (toplevel))
		return GTK_WINDOW (toplevel);

	return NULL;
}
	
static void
project_new_group (GtkWidget *widget, gpointer user_data)
{
	ProjectTool *proj_tool = user_data;
	gchar *parent_group;
	GbfTreeData *data;

	/* get default parent group */
	data = gbf_project_view_find_selected (GBF_PROJECT_VIEW (proj_tool->project_view),
					       GBF_TREE_NODE_GROUP);
	if (data)
		parent_group = data->id;
	else
		parent_group = NULL;

	gbf_project_util_new_group (proj_tool->project_model,
				    try_get_toplevel_widget (proj_tool->project_view),
				    parent_group);

	gbf_tree_data_free (data);
}

static void
project_new_target (GtkWidget *widget, gpointer user_data)
{
	ProjectTool *proj_tool = user_data;
	gchar *default_group;
	GbfTreeData *data;

	/* get default group */
	data = gbf_project_view_find_selected (GBF_PROJECT_VIEW (proj_tool->project_view),
					       GBF_TREE_NODE_GROUP);
	if (data)
		default_group = data->id;
	else
		default_group = NULL;

	gbf_project_util_new_target (proj_tool->project_model,
				     try_get_toplevel_widget (proj_tool->project_view),
				     default_group);

	gbf_tree_data_free (data);
}

static void
project_add_source (GtkWidget *widget, gpointer user_data)
{
	AnjutaTool *tool = user_data;
	ProjectTool *proj_tool = user_data;
	gchar *uri;
	gchar *default_target;
	GbfTreeData *data;

	/* get default target */
	data = gbf_project_view_find_selected (GBF_PROJECT_VIEW (proj_tool->project_view),
					       GBF_TREE_NODE_TARGET);
	if (data)
		default_target = data->id;
	else
		default_target = NULL;

	/* get default file to add */
	uri = anjuta_get_current_uri (tool);

	gbf_project_util_add_source (proj_tool->project_model,
				     try_get_toplevel_widget (proj_tool->project_view),
				     default_target,
				     uri);

	g_free (uri);
	gbf_tree_data_free (data);
}

static void
project_remove_source (GtkWidget *widget, gpointer user_data) 
{
	ProjectTool *tool = (ProjectTool *) user_data;
	GbfTreeData *data;
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;
	
	/* examine the project view to get the currently selected source */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tool->project_view));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		gtk_tree_model_get (model, &iter,
				    GBF_PROJECT_MODEL_COLUMN_DATA, &data,
				    -1);
		if (data->type == GBF_TREE_NODE_TARGET_SOURCE) {
			GError *err = NULL;
			
			gbf_project_remove_source (tool->project, data->id, &err);
			if (err) {
				gchar *msg;
				msg = g_strdup_printf ("Error removing source: %s", err->message);
				anjuta_dialog_error (msg);
				g_free (msg);
				g_error_free (err);
			}
			
		} else {
			anjuta_dialog_error ("No source file is selected");
			
		}
		gbf_tree_data_free (data);

	} else {
		anjuta_dialog_error ("Nothing selected");

	}
}

static void
build (ProjectTool *proj_tool,
       GbfBuildType type)
{
	GbfProject *project = proj_tool->project;
	GError *err = NULL;

	gbf_project_build (project, type, &err);
	if (err) {
		g_warning ("error building project\n");
		g_error_free (err);
	}
}

static void
build_prepare (GtkWidget *widget,
	       gpointer   data)
{
	build (data, GBF_BUILD_PREPARE);
}

static void
build_configure (GtkWidget *widget,
		 gpointer   data)
{
	build (data, GBF_BUILD_CONFIGURE);
}

static void
build_clean (GtkWidget *widget,
	     gpointer   data)
{
	build (data, GBF_BUILD_CLEAN);
}

static void
build_all (GtkWidget *widget,
	   gpointer   data)
{
	build (data, GBF_BUILD_ALL);
}

static void
build_install (GtkWidget *widget,
	       gpointer   data)
{
	build (data, GBF_BUILD_INSTALL);
}

static void
build_run (GtkWidget *widget,
	   gpointer   data)
{
}

static void
build_params (GtkWidget *widget,
	      gpointer   data)
{
}

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("ProjectNew", project_new),
	BONOBO_UI_UNSAFE_VERB ("ProjectImport", project_import),
	BONOBO_UI_UNSAFE_VERB ("ProjectOpen", project_open),
	BONOBO_UI_UNSAFE_VERB ("ProjectClose", project_close),

	BONOBO_UI_UNSAFE_VERB ("ProjectNewGroup", project_new_group),
	BONOBO_UI_UNSAFE_VERB ("ProjectNewTarget", project_new_target),
	BONOBO_UI_UNSAFE_VERB ("ProjectAddSource", project_add_source),
	BONOBO_UI_UNSAFE_VERB ("ProjectRemoveSource", project_remove_source),

	BONOBO_UI_UNSAFE_VERB ("BuildPrepare", build_prepare),
	BONOBO_UI_UNSAFE_VERB ("BuildConfigure", build_configure),
	BONOBO_UI_UNSAFE_VERB ("BuildClean", build_clean),
	BONOBO_UI_UNSAFE_VERB ("BuildAll", build_all), 
	BONOBO_UI_UNSAFE_VERB ("BuildInstall", build_install),
	BONOBO_UI_UNSAFE_VERB ("BuildRun", build_run),
	BONOBO_UI_UNSAFE_VERB ("BuildParameters", build_params),

	BONOBO_UI_VERB_END
};

static void
uri_activated_cb (GtkWidget  *widget,
		  const char *uri,
		  gpointer    user_data)
{
	open_file (user_data, uri, 0);
}

static void
target_selected_cb (GtkWidget  *widget,
		    const char *target_id,
		    gpointer    user_data)
{
	ProjectTool *proj_tool = user_data;
	GbfProject *project = proj_tool->project;
	GbfProjectTarget *target;
	GError *err = NULL;

	target = gbf_project_get_target (project, target_id, &err);
	if (err) {
		g_print ("error retrieving target: %s\n", target_id);
		g_error_free (err);
		return;
	}

	set_current_target (proj_tool, target);
}

static void
init_project_view (AnjutaTool *tool)
{
	GtkWidget *scrolled_window;
	ProjectTool *proj_tool = (ProjectTool *)tool;

	proj_tool->project_model = gbf_project_model_new (NULL);
	proj_tool->project_view = gbf_project_view_new ();
	gtk_tree_view_set_model (GTK_TREE_VIEW (proj_tool->project_view),
				 GTK_TREE_MODEL (proj_tool->project_model));
	gtk_widget_show (proj_tool->project_view);

        scrolled_window = gtk_scrolled_window_new (NULL, NULL);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                        GTK_POLICY_AUTOMATIC,
                                        GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled_window),
					     GTK_SHADOW_IN);
        gtk_container_add (GTK_CONTAINER (scrolled_window), proj_tool->project_view);
        gtk_widget_show (scrolled_window);

	g_signal_connect (proj_tool->project_view, "uri_activated",
			  G_CALLBACK (uri_activated_cb), tool);
	g_signal_connect (proj_tool->project_view, "target_selected",
			  G_CALLBACK (target_selected_cb), tool);

	anjuta_shell_add_widget (tool->shell,
				 scrolled_window,
				 "ProjectManager::ProjectView",
				 _("Project"),
				 NULL);
}

static void
warning_selected_cb (GtkWidget  *widget,
		     const char *filename,
		     int         line,
		     gpointer    user_data)
{
	open_file (user_data, filename, line);
}

static void
init_build_info (AnjutaTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;

	proj_tool->build_info = gbf_build_info_new ();
	gtk_widget_show (proj_tool->build_info);

	anjuta_shell_add_widget (tool->shell,
				 proj_tool->build_info,
				 "ProjectManager::BuildInfo",
				 _("Build"),
				 NULL);

	g_signal_connect (G_OBJECT (proj_tool->build_info),
			  "warning_selected",
			  G_CALLBACK (warning_selected_cb),
			  tool);
	g_signal_connect (G_OBJECT (proj_tool->build_info),
			  "error_selected",
			  G_CALLBACK (warning_selected_cb),
			  tool);
}

static void
session_load_cb (AnjutaShell *shell,
		 AnjutaTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;
	const char **args;
	int i;

	/* If the user startup anjuta2 with the intention to display a source
	 * file then don't load the previous session but only the files on the
	 * commandline. */
	anjuta_shell_get (tool->shell,
			  "Shell::ProgramArguments",
			  G_TYPE_POINTER,
			  &args,
			  NULL);
	if (args) {
		for (i = 0; args[i] != NULL; i++) {
			if (g_str_has_suffix (args[i], ".anjuta")) {
				set_build (proj_tool, args[i]);
				break;
			}
		}
	}
}

static void
session_save_cb (AnjutaShell *shell,
		 AnjutaTool *tool)
{
}

static void
shell_set (AnjutaTool *tool)
{
	GConfClient *client;
	GdlRecent *recent;
	ProjectTool *proj_tool = (ProjectTool*)tool;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (ANJUTA_IS_TOOL (tool));

	proj_tool->manager = ANJUTA_PROJECT_MANAGER (anjuta_project_manager_new ());
	anjuta_shell_add (tool->shell,
			  "ProjectManager",
			  ANJUTA_TYPE_PROJECT_MANAGER,
			  proj_tool->manager,
			  NULL);

	g_signal_connect (G_OBJECT (tool->shell),
			  "session_load",
			  G_CALLBACK (session_load_cb),
			  tool);
	g_signal_connect (G_OBJECT (tool->shell),
			  "session_save",
			  G_CALLBACK (session_save_cb),
			  tool);

	anjuta_tool_merge_ui (tool, "anjuta-project-manager",
			      DATADIR,
			      "anjuta-project-manager.xml",
			      verbs, tool);
	
	init_project_view (tool);
	init_build_info (tool);
	
	/* Create GdlRecent object for projects history. */
	client = gconf_client_get_default ();
	recent = gdl_recent_new ("/apps/anjuta2/plugins/project_manager/recent_projects",
				 "/menu/File/DocumentOps/FileRecent/RecentProjects",
				 gconf_client_get_int (client,
						       RECENT_PROJECTS_LIMIT,
						       NULL),
				 GDL_RECENT_LIST_ALPHABETIC);
	g_object_unref (G_OBJECT (client));
	g_signal_connect (G_OBJECT (recent),
			  "activate",
			  G_CALLBACK (recent_project),
			  tool);
	gdl_recent_set_ui_component (recent, tool->uic);

	proj_tool->recent = recent;
}

static void
dispose (GObject *obj)
{
	AnjutaTool *tool = ANJUTA_TOOL (obj);
	ProjectTool *proj_tool = (ProjectTool*)obj;

	if (proj_tool->project_view) {
		g_object_unref (proj_tool->project_model);
		proj_tool->project_model = NULL;

		anjuta_shell_remove_value (tool->shell,
					   "ProjectManager::ProjectView",
					   NULL);
		proj_tool->project_view = NULL;
	}

	if (proj_tool->build_info) {
		anjuta_shell_remove_value (tool->shell,
					   "ProjectManager::BuildInfo",
					   NULL);
		proj_tool->build_info = NULL;
	}

	if (proj_tool->project) {
		anjuta_shell_remove_value (tool->shell,
					   "ProjectManager::CurrentProject",
					   NULL);
		g_object_unref (proj_tool->project);
		proj_tool->project = NULL;
	}

	if (proj_tool->recent) {
		g_object_unref (proj_tool->recent);
		proj_tool->recent = NULL;
	}

	anjuta_tool_unmerge_ui (tool);

	if (proj_tool->manager) {
		anjuta_shell_remove_value (tool->shell,
					   "ProjectManager",
					   NULL);
		g_object_unref (proj_tool->manager);
		proj_tool->manager = NULL;
	}
}

static void
project_tool_instance_init (GObject *object)
{
}

static void
project_tool_class_init (GObjectClass *klass)
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);

	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (ProjectTool, project_tool);

ANJUTA_SIMPLE_PLUGIN (ProjectTool, project_tool);
