/* Anjuta
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-macros.h>
#include "anjuta-project-manager.h"

struct _AnjutaProjectManagerPrivate {
	GSList *project_types;
};

GNOME_CLASS_BOILERPLATE (AnjutaProjectManager, anjuta_project_manager, GObject, G_TYPE_OBJECT);

void
anjuta_project_manager_add_project_type (AnjutaProjectManager *manager,
					 AnjutaProjectType *type)
{
	g_return_if_fail (manager != NULL);
	g_return_if_fail (ANJUTA_IS_PROJECT_MANAGER (manager));
	g_return_if_fail (type != NULL);
	g_return_if_fail (ANJUTA_IS_PROJECT_TYPE (type));

	manager->priv->project_types = g_slist_append (manager->priv->project_types,
						       type);
}

void
anjuta_project_manager_remove_project_type (AnjutaProjectManager *manager,
					    AnjutaProjectType *type)
{
	g_return_if_fail (manager != NULL);
	g_return_if_fail (ANJUTA_IS_PROJECT_MANAGER (manager));
	g_return_if_fail (type != NULL);
	g_return_if_fail (ANJUTA_IS_PROJECT_TYPE (type));

	manager->priv->project_types = g_slist_remove (manager->priv->project_types,
						       type);
}

GSList *
anjuta_project_manager_get_project_types (AnjutaProjectManager *manager)
{
	g_return_val_if_fail (manager != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_PROJECT_MANAGER (manager), NULL);

	return manager->priv->project_types;
}

GObject *
anjuta_project_manager_new (void)
{
	return g_object_new (ANJUTA_TYPE_PROJECT_MANAGER, NULL);
}

static void
anjuta_project_manager_class_init (AnjutaProjectManagerClass *klass)
{
	parent_class = g_type_class_peek_parent (klass);
}

static void
anjuta_project_manager_instance_init (AnjutaProjectManager *manager)
{
	manager->priv = g_new0 (AnjutaProjectManagerPrivate, 1);
	manager->priv->project_types = NULL;
}
