/* Anjuta
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ANJUTA_PROJECT_MANAGER_H
#define ANJUTA_PROJECT_MANAGER_H

#include <glib-object.h>
#include "anjuta-project-type.h"

G_BEGIN_DECLS

#define ANJUTA_TYPE_PROJECT_MANAGER		(anjuta_project_manager_get_type ())
#define ANJUTA_PROJECT_MANAGER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ANJUTA_TYPE_PROJECT_MANAGER, AnjutaProjectManager))
#define ANJUTA_PROJECT_MANAGER_CLASS(obj)	(G_TYPE_CHECK_CLASS_CAST ((klass), ANJUTA_TYPE_PROJECT_MANAGER, AnjutaProjectManagerClass))
#define ANJUTA_IS_PROJECT_MANAGER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ANJUTA_TYPE_PROJECT_MANAGER))
#define ANJUTA_IS_PROJECT_MANAGER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), ANJUTA_TYPE_PROJECT_MANAGER))

typedef struct _AnjutaProjectManager		AnjutaProjectManager;
typedef struct _AnjutaProjectManagerPrivate	AnjutaProjectManagerPrivate;
typedef struct _AnjutaProjectManagerClass	AnjutaProjectManagerClass;

struct _AnjutaProjectManager {
	GObject parent;

	AnjutaProjectManagerPrivate *priv;
};

struct _AnjutaProjectManagerClass {
	GObjectClass parent_class;
};

GType anjuta_project_manager_get_type            (void);
GObject *anjuta_project_manager_new              (void);

void anjuta_project_manager_add_project_type     (AnjutaProjectManager *manager,
						  AnjutaProjectType    *type);
void anjuta_project_manager_remove_project_type  (AnjutaProjectManager *manager,
						  AnjutaProjectType    *type);
GSList *anjuta_project_manager_get_project_types (AnjutaProjectManager *manager);

G_END_DECLS

#endif /* ANJUTA_PROJECT_MANAGER_H */
