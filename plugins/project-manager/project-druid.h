/* Anjuta
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ANJUTA_PROJECT_DRUID_H
#define ANJUTA_PROJECT_DRUID_H

#include <gtk/gtkvbox.h>
#include "anjuta-project-type.h"

G_BEGIN_DECLS

#define ANJUTA_TYPE_PROJECT_DRUID		(anjuta_project_druid_get_type ())
#define ANJUTA_PROJECT_DRUID(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ANJUTA_TYPE_PROJECT_DRUID, AnjutaProjectDruid))
#define ANJUTA_PROJECT_DRUID_CLASS(obj)		(G_TYPE_CHECK_CLASS_CAST ((klass), ANJUTA_TYPE_PROJECT_DRUID, AnjutaProjectDruidClass))
#define ANJUTA_IS_PROJECT_DRUID(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ANJUTA_TYPE_PROJECT_DRUID))
#define ANJUTA_IS_PROJECT_DRUID_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), ANJUTA_TYPE_PROJECT_DRUID))

typedef struct _AnjutaProjectDruid        AnjutaProjectDruid;
typedef struct _AnjutaProjectDruidPrivate AnjutaProjectDruidPrivate;
typedef struct _AnjutaProjectDruidClass   AnjutaProjectDruidClass;

struct _AnjutaProjectDruid {
	GtkVBox parent;

	AnjutaProjectDruidPrivate *priv;
};

struct _AnjutaProjectDruidClass {
	GtkVBoxClass parent_class;

	void (*cancel) (AnjutaProjectDruid *druid);
	void (*finish) (AnjutaProjectDruid *druid);
};

GType      anjuta_project_druid_get_type (void);
GtkWidget *anjuta_project_druid_new (GSList *project_types);

AnjutaProjectInfo *anjuta_project_druid_get_info (AnjutaProjectDruid *druid);

G_END_DECLS

#endif /* ANJUTA_PROJECT_DRUID_H */
