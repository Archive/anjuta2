/* Anjuta
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ANJUTA_PROJECT_TYPE_H
#define ANJUTA_PROJECT_TYPE_H

#include <glib-object.h>
#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

#define ANJUTA_TYPE_PROJECT_TYPE		(anjuta_project_type_get_type ())
#define ANJUTA_PROJECT_TYPE(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ANJUTA_TYPE_PROJECT_TYPE, AnjutaProjectType))
#define ANJUTA_PROJECT_TYPE_CLASS(obj)		(G_TYPE_CHECK_CLASS_CAST ((klass), ANJUTA_TYPE_PROJECT_TYPE, AnjutaProjectTypeClass))
#define ANJUTA_IS_PROJECT_TYPE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ANJUTA_TYPE_PROJECT_TYPE))
#define ANJUTA_IS_PROJECT_TYPE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), ANJUTA_TYPE_PROJECT_TYPE))
#define ANJUTA_PROJECT_TYPE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ANJUTA_TYPE_PROJECT_TYPE, AnjutaProjectTypeClass))

typedef struct _AnjutaProjectInfo		AnjutaProjectInfo;
typedef struct _AnjutaProjectType		AnjutaProjectType;
typedef struct _AnjutaProjectTypeClass		AnjutaProjectTypeClass;

struct _AnjutaProjectInfo {
	char *name;
	char *package;
	char *description;
	char *location;
	AnjutaProjectType *type;
};

struct _AnjutaProjectType {
	GObject parent;
};

struct _AnjutaProjectTypeClass {
	GObjectClass parent_class;

	/* Virtual Table */
	const char * (*get_category_name) (AnjutaProjectType *type);
	const char * (*get_project_name)  (AnjutaProjectType *type);
	const char * (*get_description)   (AnjutaProjectType *type);
	const char * (*get_backend)       (AnjutaProjectType *type);

	int (*get_druid_page_count)       (AnjutaProjectType *type,
					   GError           **error);
	GtkWidget * (*get_druid_page)     (AnjutaProjectType *type,
					   int                page,
					   GError           **error);

	void (*create_project)            (AnjutaProjectType *type,
					   AnjutaProjectInfo *info,
					   GError           **error);
};

GType anjuta_project_type_get_type                (void);

const char *anjuta_project_type_get_category_name (AnjutaProjectType *type);
const char *anjuta_project_type_get_project_name  (AnjutaProjectType *type);
const char *anjuta_project_type_get_description   (AnjutaProjectType *type);
const char *anjuta_project_type_get_backend       (AnjutaProjectType *type);
int anjuta_project_type_get_druid_page_count      (AnjutaProjectType *type,
						   GError           **error);
GtkWidget *anjuta_project_type_get_druid_page     (AnjutaProjectType *type,
						   int                page,
						   GError           **error);
void anjuta_project_type_create_project           (AnjutaProjectType *type,
						   AnjutaProjectInfo *info,
						   GError           **error);

G_END_DECLS

#endif /* ANJUTA_PROJECT_TYPE_H */
