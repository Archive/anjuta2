/* Anjuta Symbol Browser Tool, based on GnomeSymbolBrowser
 *
 * Copyright (C) 2001 Naba Kumar
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>

#include <gnome.h>
#include <bonobo.h>
#include <gdl/gdl.h>
#include <gbf/gbf-project.h>
#include <bonobo-activation/bonobo-activation.h>
#include <libanjuta/libanjuta.h>

typedef struct {
	AnjutaTool parent;
	
	Bonobo_Control ctrl;

	int project_watch;
	int document_watch;

	GNOME_Development_SymbolBrowser symbol_browser;
} SymbolBrowserTool;

typedef struct {
	AnjutaToolClass parent;
} SymbolBrowserToolClass;

static void
event_cb (BonoboListener    *listener,
	  char              *event,
	  CORBA_any         *any,
	  CORBA_Environment *ev,
	  gpointer           user_data)
{
	AnjutaTool *tool = ANJUTA_TOOL (user_data);

	if (!strcmp (event, "go-to")) {
		gchar *file, *ln;
		glong line;
		int linelen;

		CORBA_char *location = BONOBO_ARG_GET_STRING (any);

		/* separate the location into a filename and line number. */
		int pos = strchr (location, ':') - location;
		file = g_malloc (pos + 1);
		strncpy (file, location, pos);
		file[pos] = '\0';
		linelen = strlen (location) - (pos + 1);
		ln = g_malloc (linelen + 1);
		strncpy (ln, location + pos + 1, linelen);
		ln[linelen] = '\0';
		line = atoi (ln);
		g_free (ln);

		if (!anjuta_show_file (tool, file)) {
			anjuta_dialog_error (_("Unable to open file."));
			g_free (file);
			return;
		}

		g_free (file);

		if (line > -1)
			anjuta_set_line_num (tool, line);
	}
}



static void
project_added (AnjutaTool *anjuta_tool, 
	       const char *name, 
	       const GValue *value,
	       gpointer user_data)
{
	CORBA_Environment ev;
	GbfProject *project;
	char *dir;
	SymbolBrowserTool *tool = (SymbolBrowserTool *)user_data;

	project = g_value_get_pointer (value);
	g_assert (GBF_IS_PROJECT (project));

	/* get directory of current project. */
	g_object_get (G_OBJECT (project), "project-dir", &dir, NULL);

	CORBA_exception_init (&ev);

	/* load the symbols for the current project. */
	GNOME_Development_SymbolBrowser_openDirectory (tool->symbol_browser,
						       dir,
						       &ev);
	g_assert (!BONOBO_EX (&ev));

	CORBA_exception_free (&ev);
}

static void
project_removed (AnjutaTool *anjuta_tool,
		 const char *name,
		 gpointer user_data)
{
	SymbolBrowserTool *tool = (SymbolBrowserTool *)user_data;
	CORBA_Environment ev;
	CORBA_exception_init (&ev);
	
	/* clear the symbol-browser. */
	GNOME_Development_SymbolBrowser_clear (tool->symbol_browser,
					       &ev);
	g_assert (!BONOBO_EX (&ev));

	CORBA_exception_free (&ev);
}

static void
document_added (AnjutaTool *anjuta_tool,
		const char *name,
		const GValue *value,
		gpointer user_data)
{
	SymbolBrowserTool *tool = (SymbolBrowserTool *)user_data;
	CORBA_Environment ev;
	AnjutaDocument *document;
	const char *uri;

	CORBA_exception_init (&ev);
	
	document = g_value_get_object (value);
	
	/* get uri of current document. */
	uri = anjuta_document_get_uri (document);
	
	/* load the symbols for the current file. */
	if (uri) {
		GNOME_Development_SymbolBrowser_setFile (tool->symbol_browser,
							 uri,
							 &ev);
		g_assert (!BONOBO_EX (&ev));
		/*
		  } else {
		  GNOME_Development_SymbolBrowser_setFile (symbol_browser,
		  CORBA_OBJECT_NIL,
		  ev);
		*/
	}
	CORBA_exception_free (&ev);
}
		
static void
document_removed (AnjutaTool *anjuta_tool,
		  const char *name,
		  gpointer user_data)
{
	/* clear the symbol-browser. */
	/* GNOME_Development_SymbolBrowser_setFile (symbol_browser, CORBA_OBJECT_NIL,							       &ev);
	   g_assert (!BONOBO_EX (ev));
	*/
}

static void
add_event_listener (SymbolBrowserTool *tool,
		    Bonobo_Control ctrl)
{
	BonoboListener *listener;
	CORBA_Object source;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	/* register event callback for go-to events from the symbol browser. */
	listener = bonobo_listener_new (NULL, NULL);
	g_signal_connect (G_OBJECT (listener), "event_notify",
			  G_CALLBACK (event_cb), tool);

	source = Bonobo_Unknown_queryInterface (ctrl,
						"IDL:Bonobo/EventSource:1.0",
						&ev);
	if (!CORBA_Object_is_nil (source, &ev) && ev._major == CORBA_NO_EXCEPTION) {
		Bonobo_EventSource_addListener (source,
						bonobo_object_corba_objref (BONOBO_OBJECT (listener)),
									    &ev);
	} else
		g_error ("couldn't get event source for symbol browser");
	
	bonobo_object_release_unref (source, &ev);
	g_assert (!BONOBO_EX (&ev));

	CORBA_exception_free (&ev);

	tool->project_watch = anjuta_tool_add_watch
		(ANJUTA_TOOL (tool),
		 "CurrentProject",
		 project_added,
		 project_removed,
		 tool);

	tool->document_watch = anjuta_tool_add_watch
		(ANJUTA_TOOL (tool),
		 "CurrentDocument",
		 document_added,
		 document_removed,
		 tool);
}

static void
init_symbol_browser (AnjutaTool *anjuta_tool)
{
	SymbolBrowserTool *tool = (SymbolBrowserTool*)anjuta_tool;
	CORBA_Environment ev;
	Bonobo_Control ctrl;

	CORBA_exception_init (&ev);
	ctrl = bonobo_get_object ("OAFIID:GNOME_Development_SymbolBrowser_Control",
				  "IDL:Bonobo/Control:1.0", &ev);

	if (!CORBA_Object_is_nil (ctrl, &ev)) {
		tool->symbol_browser = Bonobo_Unknown_queryInterface 
			(ctrl, "IDL:GNOME/Development/SymbolBrowser:1.0", &ev);

		anjuta_shell_add_control (anjuta_tool->shell,
					  ctrl,
					  "SymbolBrowser",
					  _("Symbols"),
					  NULL);

		add_event_listener (tool, ctrl);
		Bonobo_Unknown_unref(ctrl, &ev);
	} else
		g_warning ("Could not initialize symbol browser.");
	
	CORBA_exception_free (&ev);
}

static void
shell_set (AnjutaTool *tool)
{
	g_return_if_fail (tool != NULL);
	g_return_if_fail (ANJUTA_IS_TOOL (tool));

	init_symbol_browser (tool);
}

static void
dispose (GObject *obj)
{
	AnjutaTool *anjuta_tool = ANJUTA_TOOL (obj);
	SymbolBrowserTool *tool = (SymbolBrowserTool*)anjuta_tool;

	if (tool->project_watch) {
		anjuta_tool_remove_watch (anjuta_tool, 
					  tool->project_watch, 
					  FALSE);
		tool->project_watch = 0;
	}
	
	if (tool->document_watch) {
		anjuta_tool_remove_watch (anjuta_tool, 
					  tool->document_watch, 
					  FALSE);
		tool->document_watch = 0;
	}  

	if (tool->ctrl != CORBA_OBJECT_NIL) {
		anjuta_shell_remove_value (anjuta_tool->shell, 
					   "SymbolBrowser",
					   NULL);
		tool->ctrl = CORBA_OBJECT_NIL;
	}
	
	if (tool->symbol_browser != CORBA_OBJECT_NIL) {
		bonobo_object_release_unref (tool->symbol_browser, NULL);
		tool->symbol_browser = CORBA_OBJECT_NIL;
	}
}

static void
symbol_browser_tool_instance_init (GObject *object)
{
}

static void
symbol_browser_tool_class_init (GObjectClass *klass)
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);
	
	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (SymbolBrowserTool, symbol_browser_tool);

ANJUTA_SIMPLE_PLUGIN (SymbolBrowserTool, symbol_browser_tool);
