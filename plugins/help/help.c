/*
 * Manhelp plugin
 *
 * Authors:
 *   The Bear <bear@berkeleycs.ml.org>
 *   Edwin Purvee (epurvee@yahoo.com)
 */

#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <glade/glade.h>
#include <libanjuta/libanjuta.h>
#include "help.h"

#define GLADE_FILE			"man_plugin.glade"

static void help_fill_man_page(GladeXML* xml, gchar* function);
void on_man_eventbox_selection_received(GtkWidget* widget,
	GtkSelectionData* selection_data, gpointer data);
void on_man_eventbox_button_release_event(GtkWidget* widget, gpointer data);
void on_man_indexbutton_clicked(GtkWidget* widget, gpointer data);
void on_man_gotobutton_clicked(GtkWidget* widget, gpointer data);
void on_man_textentry_activate(GtkWidget* widget, gpointer data);
void on_man_aproposbutton_clicked(GtkWidget* widget, gpointer data);
static void man_apropos(GladeXML* gui, gchar* function);

/*
 * When the user selects something on the manual viewer, show that page
 */
void
on_man_eventbox_selection_received(
	GtkWidget*			widget,
	GtkSelectionData*		selection_data,
	gpointer			data
)
{
	GladeXML*			xml;

	if(selection_data->length <= 0)
	{
		return;
	}

	widget = gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW);
	g_return_if_fail(GTK_WIDGET_TOPLEVEL(widget));

	xml = gtk_object_get_user_data(GTK_OBJECT(widget));

	help_fill_man_page(xml, selection_data->data);

	gtk_selection_owner_set(NULL, GDK_SELECTION_PRIMARY, GDK_CURRENT_TIME);
}

/*
 * Convert a selection to text
 */
void
on_man_eventbox_button_release_event(
	GtkWidget*			widget,
	gpointer			data
)
{
	static GdkAtom			targets_atom = GDK_NONE;

	if(targets_atom == GDK_NONE)
	{
		targets_atom = gdk_atom_intern("TEXT", FALSE);
	}
	gtk_main_iteration();
	gtk_selection_convert(widget, GDK_SELECTION_PRIMARY, targets_atom,
		GDK_CURRENT_TIME);
}

/*
 * Show the index of the current section
 */
void
on_man_indexbutton_clicked(
	GtkWidget*			widget,
	gpointer			data
)
{
	GtkWidget*			wid;
	GladeXML*			xml;
	GtkWidget*			child;
	gchar*				text;
	GtkWidget*			text_widget;
	GdkFont*			bold;

	widget = gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW);
	g_return_if_fail(GTK_WIDGET_TOPLEVEL(widget));

	xml = gtk_object_get_user_data(GTK_OBJECT(widget));

	wid = glade_xml_get_widget(xml, "man_optionmenu");
	g_return_if_fail(GTK_IS_BIN(wid));
	g_return_if_fail(GTK_BIN(wid)->child);

	text_widget = glade_xml_get_widget(xml, "man_text");
	g_return_if_fail(GTK_IS_TEXT(text_widget));

	child = GTK_BIN(wid)->child;
	g_return_if_fail(GTK_IS_LABEL(child));

	gtk_label_get(GTK_LABEL(child), &text);

	if(GTK_LABEL(child)->label[0] == ' ')
	{
		gtk_text_set_editable(GTK_TEXT(text_widget), TRUE);
		gtk_text_set_point(GTK_TEXT(text_widget), 0);
		gtk_text_forward_delete(GTK_TEXT(text_widget),
			gtk_text_get_length(GTK_TEXT(text_widget)));
		/* FIXME: need to free the font */
		bold = gdk_font_load(
			"-misc-fixed-bold-r-normal-*-*-120-*-*-c-*-iso8859-1");
		gtk_text_insert(GTK_TEXT(text_widget), bold,
			&text_widget->style->black, NULL,
			_("You need to select a section first") , -1);
		gtk_text_set_editable(GTK_TEXT(text_widget), FALSE);
		return;
	}

	text = g_strdup_printf("\'(%c)\'", GTK_LABEL(child)->label[0]);
	man_apropos(xml, text);
	g_free(text);
}

/*
 * Show manual page
 */
void
on_man_gotobutton_clicked(
	GtkWidget*			widget,
	gpointer			data
)
{
	on_man_textentry_activate(widget, data);
}

/*
 * Show manual page
 */
void
on_man_textentry_activate(
	GtkWidget*			widget,
	gpointer			data
)
{
	GladeXML*			xml;
	GtkWidget*			wid;

	widget = gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW);
	g_return_if_fail(GTK_WIDGET_TOPLEVEL(widget));

	xml = gtk_object_get_user_data(GTK_OBJECT(widget));

	wid = glade_xml_get_widget(xml, "man_textentry");

	help_fill_man_page(xml,
		gtk_entry_get_text(GTK_ENTRY(wid)));
}

/*
 * Show related info
 */
void
on_man_aproposbutton_clicked(
	GtkWidget*			widget,
	gpointer			data
)
{
	GladeXML*			xml;
	GtkWidget*			wid;

	widget = gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW);
	g_return_if_fail(GTK_WIDGET_TOPLEVEL(widget));

	xml = gtk_object_get_user_data(GTK_OBJECT(widget));

	wid = glade_xml_get_widget(xml, "man_textentry");

	man_apropos(xml,
		gtk_entry_get_text(GTK_ENTRY(wid)));
}

/*
 * Load the output of 'apropos' into the text widget.
 */
static void
man_apropos(
	GladeXML*			gui,
	gchar*				function
)
{
	gchar*				cmdstr;
	GtkWidget*			text_widget;
	gchar				line[81];
	gchar*				temp_string;
	GdkFont*			normal;
	GdkFont*			bold;
	FILE*				fp;

	cmdstr = g_strdup_printf("apropos %s", function);

	text_widget = glade_xml_get_widget(gui, "man_text");
	g_return_if_fail(GTK_IS_TEXT(text_widget));

	/* FIXME: We should let the user select which font they want */
	normal = gdk_font_load(
		"-misc-fixed-medium-r-normal-*-*-120-*-*-c-*-iso8859-1");
	bold = gdk_font_load(
		"-misc-fixed-bold-r-normal-*-*-120-*-*-c-*-iso8859-1");

	fp = popen(cmdstr, "r");
	g_return_if_fail(fp);
	g_return_if_fail(!feof(fp));

	if(fgets(line, sizeof(line), fp) == 0)
	{
		/* no apropos available */
		/* Show some error message */
		gtk_text_set_editable(GTK_TEXT(text_widget), TRUE);
		gtk_text_set_point(GTK_TEXT(text_widget), 0);
		gtk_text_forward_delete(GTK_TEXT(text_widget),
			gtk_text_get_length(GTK_TEXT(text_widget)));
		temp_string = g_strdup_printf("No apropos available for %s\n"
			"Check whether the 'apropos' command works\n"
			"(hint: do a 'man makewhatis' on the command line)\n",
			function);
		gtk_text_insert(GTK_TEXT(text_widget), bold,
			&text_widget->style->black, NULL,
			temp_string , -1);
		g_free(temp_string);
		gtk_text_set_editable(GTK_TEXT(text_widget), FALSE);

		pclose(fp);
		g_free(cmdstr);
		return;
	}

	gtk_text_set_editable(GTK_TEXT(text_widget), TRUE);

	/* We have to clear all the old text before we can write */
	gtk_text_set_point(GTK_TEXT(text_widget), 0);
	gtk_text_forward_delete(GTK_TEXT(text_widget),
		gtk_text_get_length(GTK_TEXT(text_widget)));

	gtk_text_freeze(GTK_TEXT(text_widget));

	do
	{
		gtk_text_insert(GTK_TEXT(text_widget),
			normal, NULL, NULL, line , -1);

		/* process gtk stuff */
		while(gtk_events_pending())
		{
			gtk_main_iteration();
		}
	}
	while(fgets(line, sizeof(line), fp));

	g_return_if_fail(pclose(fp) == 0);

	gtk_text_thaw(GTK_TEXT(text_widget));
	gtk_text_set_editable(GTK_TEXT(text_widget), FALSE);

	g_free(cmdstr);
}

/*
 * Load the manual text into the text widget.
 */
static void
help_fill_man_page(
	GladeXML*			gui,
	gchar*				function
)
{
	gchar*				cmdstr;
	GtkWidget*			text_widget;
	GtkWidget*			section_widget;
	GtkWidget*			label;
	gchar				line[81];
	gint				n;
	gint				i;
	gboolean			bold_flag = FALSE;
	gchar*				temp_string;
	GdkFont*			normal;
	GdkFont*			bold;
	FILE*				fp;

	section_widget = glade_xml_get_widget(gui, "man_optionmenu");
	g_return_if_fail(GTK_IS_OPTION_MENU(section_widget));
	g_return_if_fail(GTK_BIN(section_widget)->child);

	/* We use the first character of the option menu item to
	   determine which section the user wants */
	label = GTK_BIN(section_widget)->child;
	g_return_if_fail(GTK_IS_LABEL(label));

	cmdstr = g_strdup_printf("man %c %s",
		GTK_LABEL(label)->label[0], function);

	text_widget = glade_xml_get_widget(gui, "man_text");
	g_return_if_fail(GTK_IS_TEXT(text_widget));

	/* FIXME: We should let the user select which font they want */
	normal = gdk_font_load(
		"-misc-fixed-medium-r-normal-*-*-120-*-*-c-*-iso8859-1");
	bold = gdk_font_load(
		"-misc-fixed-bold-r-normal-*-*-120-*-*-c-*-iso8859-1");

	fp = popen(cmdstr, "r");
	g_return_if_fail(fp);
	g_return_if_fail(!feof(fp));

	/* skip first line */
	fgets(line, sizeof(line), fp);
	if(!anjuta_is_empty_string (line))
	{
		/* no help available */
		/* Show some error message */
		gtk_text_set_editable(GTK_TEXT(text_widget), TRUE);
		gtk_text_set_point(GTK_TEXT(text_widget), 0);
		gtk_text_forward_delete(GTK_TEXT(text_widget),
			gtk_text_get_length(GTK_TEXT(text_widget)));
		temp_string = g_strdup_printf("No help available for %s\n",
			function);
		gtk_text_insert(GTK_TEXT(text_widget), bold,
			&text_widget->style->black, NULL,
			temp_string , -1);
		g_free(temp_string);
		gtk_text_set_editable(GTK_TEXT(text_widget), FALSE);

		pclose(fp);
		g_free(cmdstr);
		return;
	}

	gtk_text_set_editable(GTK_TEXT(text_widget), TRUE);

	/* We have to clear all the old text before we can write */
	gtk_text_set_point(GTK_TEXT(text_widget), 0);
	gtk_text_forward_delete(GTK_TEXT(text_widget),
		gtk_text_get_length(GTK_TEXT(text_widget)));

	gtk_text_freeze(GTK_TEXT(text_widget));

	while(fgets(line, sizeof(line), fp))
	{
		n = strlen(line);
		/*
		 * The format returned by man formats bold chars by the sequence
		 * char, backspace, char. The following loop parses that sequence
		 */
		for(i = 0; i < n; i++)
		{
			if(bold_flag)
			{
				temp_string = g_strdup_printf("%c", line[i]);
				gtk_text_insert(GTK_TEXT(text_widget), bold,
					&text_widget->style->black, NULL,
					temp_string , -1);
				g_free(temp_string);
				bold_flag = FALSE;
			}
			else if(line[i] == 8)
			{
				gtk_text_backward_delete(GTK_TEXT(text_widget),
					1);
				bold_flag = TRUE;
			}
			else
			{
				gtk_text_insert(GTK_TEXT(text_widget), normal,
					&text_widget->style->black, NULL,
					&line[i], 1);
			}
		}

		/* process gtk stuff */
		while(gtk_events_pending())
		{
			gtk_main_iteration();
		}
	}

	g_return_if_fail(pclose(fp) == 0);

	gtk_text_thaw(GTK_TEXT(text_widget));
	gtk_text_set_editable(GTK_TEXT(text_widget), FALSE);

	g_free(cmdstr);
}

/*
 * Show the gui for the manual viewer
 */
static void
help_show(
	char*				function
)
{
	GtkWidget*			wid;
	GladeXML*			gui;

	gui = glade_xml_new(ANJUTA_GLADEDIR "/" GLADE_FILE, NULL);
	if(!gui)
	{
		printf("Could not find " GLADE_FILE "\n");
		return;
	}

	wid = glade_xml_get_widget(gui, "man_window");
	gtk_object_set_user_data(GTK_OBJECT(wid), gui);

	glade_xml_signal_autoconnect(gui);

	if(function)
	{
		help_fill_man_page(gui, function);
	}
}


void
help(
	AnjutaTool*			tool
)
{
	help_show(NULL);
}

#if 0
/*
 * This function is called when the user chooses help from the menu
 */
static void
help_man_page(
	GtkWidget*			widget,
	gpointer			data
)
{
	help_show(NULL);
}

/*
 * This function is called when the user uses the popup menu to access help
 */
static void
show_manpage(
	Tool*				tool,
	ToolState*			state
)
{
	AnjutaDocument*			doc;

	doc = state->document;
	g_return_if_fail(doc);

	if(gI_text_has_selection(doc))
	{
		gchar*			mp;

		mp = gI_text_get_selection(doc);

		help_show(mp);

		g_free(mp);
	}
	else
	{
		gchar*			mp;
		gint			point;
		gint			noc;
		gchar*			ptr;

		/* get the next gchars, max. 50 */
		point = gI_text_get_point(doc);
		if(point + 50 > gI_text_get_length(doc))
		{
			noc = gI_text_get_length(doc) - point;
		}
		else
		{
			noc = 50;
		}

		mp = gtk_editable_get_chars(GTK_EDITABLE(doc),
			point, point + noc);

		ptr = mp;

		while(*ptr != ' ' && *ptr != '(' && *ptr != ';')
		{
			++ptr;
		}

		*ptr = '\0';

		if(!anjuta_is_empty_string (mp))
		{
			help_show(mp);
		}

		g_free(mp);
	}
}

static void
man_help(
	Tool*				tool,
	ToolState*			state
)
{
	if(state->activator == TS_ACTIVATOR_MENU)
	{
		help_man_page(NULL, NULL);
	}
	else if(state->activator == TS_ACTIVATOR_POPUP)
	{
		show_manpage(tool, state);
	}
}
#endif
