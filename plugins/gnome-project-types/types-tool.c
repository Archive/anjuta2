/* Anjuta
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libanjuta/libanjuta.h>
#include "anjuta-gnome-application-type.h"
#include "../project-manager/anjuta-project-manager.h"

typedef struct {
	AnjutaTool parent;

	AnjutaProjectType *gnome_app_type;
} TypesTool;

typedef struct {
	AnjutaToolClass parent;
} TypesToolClass;

static void
shell_set (AnjutaTool *tool)
{
	TypesTool *type_tool = (TypesTool*)tool;
	AnjutaProjectManager *manager;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (ANJUTA_IS_TOOL (tool));

	anjuta_shell_get (tool->shell,
			  "ProjectManager",
			  ANJUTA_TYPE_PROJECT_MANAGER,
			  &manager,
			  NULL);
	g_return_if_fail (manager != NULL);
	g_return_if_fail (ANJUTA_IS_PROJECT_MANAGER (manager));

	type_tool->gnome_app_type = anjuta_gnome_application_type_new ();
	anjuta_project_manager_add_project_type (manager, type_tool->gnome_app_type);
}

static void
dispose (GObject *obj)
{
	AnjutaTool *tool = ANJUTA_TOOL (obj);
	TypesTool *type_tool = (TypesTool*)obj;
	AnjutaProjectManager *manager;

	anjuta_shell_get (tool->shell,
			  "ProjectManager",
			  ANJUTA_TYPE_PROJECT_MANAGER,
			  &manager,
			  NULL);
	g_return_if_fail (manager != NULL);
	g_return_if_fail (ANJUTA_IS_PROJECT_MANAGER (manager));

	if (type_tool->gnome_app_type) {
		anjuta_project_manager_remove_project_type (manager, 
							    type_tool->gnome_app_type);
		g_object_unref (type_tool->gnome_app_type);
		type_tool->gnome_app_type = NULL;
	}
}

static void
types_tool_instance_init (GObject *object)
{
}

static void
types_tool_class_init (GObjectClass *klass)
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);

	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (TypesTool, types_tool);

ANJUTA_SIMPLE_PLUGIN (TypesTool, types_tool);
