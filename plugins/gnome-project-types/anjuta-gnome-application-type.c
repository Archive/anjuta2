/* Anjuta
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include "anjuta-gnome-application-type.h"

struct _AnjutaGnomeApplicationTypePrivate {
};

GNOME_CLASS_BOILERPLATE (AnjutaGnomeApplicationType, anjuta_gnome_application_type, 
			 AnjutaProjectType, ANJUTA_TYPE_PROJECT_TYPE);

static const char *
impl_get_category_name (AnjutaProjectType *type)
{
	return _("GNOME");
}

static const char *
impl_get_project_name (AnjutaProjectType *type)
{
	return _("Application (2.0)");
}

static const char *
impl_get_description (AnjutaProjectType *type)
{
	return _("Generates a basic GNOME 2.0 project with an automake backend.");
}

static const char *
impl_get_backend (AnjutaProjectType *type)
{
	return "gbf-am:GbfAmProject";
}

static int
impl_get_druid_page_count (AnjutaProjectType *type,
			   GError **error)
{
	return 0;
}

static GtkWidget *
impl_get_druid_page (AnjutaProjectType *type,
		     int page,
		     GError **error)
{
	return NULL;
}

static void
impl_create_project (AnjutaProjectType *type,
		     AnjutaProjectInfo *info,
		     GError **error)
{
	char **args;
	char *scriptdir;
	GError *err = NULL;
	int status;
	char *errmsg, *outmsg;

	g_message ("create_project_impl");

	args = g_new0 (char *, 7);
	args[0] = g_strdup ("/usr/bin/python");
	args[1] = g_strdup ("anjuta-gnome-application-create.py");
	args[2] = g_strdup (info->name);
	args[3] = g_strdup (info->package);
	args[4] = g_strdup (info->description);
	args[5] = g_strdup (info->location);
	args[6] = NULL;

	scriptdir = g_strconcat (DATADIR, "/anjuta2/plugins/gnome-project-types", NULL);
	g_message (scriptdir);

	/* Spawn the python project creation script. */
	if (!g_spawn_sync (scriptdir, args, NULL, 0, NULL, NULL, &errmsg, &outmsg, &status, &err)) {
		g_message (errmsg);
		g_message (outmsg);
		g_message (err->message);
		*error = err;
		g_message ("error spawning script");
	}

	g_free (scriptdir);
	g_strfreev (args);
}

static void
anjuta_gnome_application_type_dispose (GObject *object)
{
	AnjutaGnomeApplicationType *type = ANJUTA_GNOME_APPLICATION_TYPE (object);

	g_free (type->priv);
}

AnjutaProjectType *
anjuta_gnome_application_type_new (void)
{
	return ANJUTA_PROJECT_TYPE (g_object_new (ANJUTA_TYPE_GNOME_APPLICATION_TYPE, NULL));
}

static void
anjuta_gnome_application_type_class_init (AnjutaGnomeApplicationTypeClass *klass)
{
	GObjectClass *object_class;
	AnjutaProjectTypeClass *project_class;

	parent_class = g_type_class_peek_parent (klass);
	object_class = G_OBJECT_CLASS (klass);
	project_class = ANJUTA_PROJECT_TYPE_CLASS (klass);

	object_class->dispose = anjuta_gnome_application_type_dispose;

	project_class->get_category_name = impl_get_category_name;
	project_class->get_project_name = impl_get_project_name;
	project_class->get_description = impl_get_description;
	project_class->get_backend = impl_get_backend;
	project_class->get_druid_page_count = impl_get_druid_page_count;
	project_class->get_druid_page = impl_get_druid_page;
	project_class->create_project = impl_create_project;
}

static void
anjuta_gnome_application_type_instance_init (AnjutaGnomeApplicationType *type)
{
	type->priv = g_new0 (AnjutaGnomeApplicationTypePrivate, 1);
}
