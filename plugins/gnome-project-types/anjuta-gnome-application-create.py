# Create a new Gnome application source tree based on autoconf/automake
#
# Copyright (C) 2002 Jeroen Zwartepoorte
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import sys, os, string, pwd, time

usage = 'usage: anjuta-gnome-application-create name package description location'

templatepath = 'gnome-application-templates'

files = [
    ['autogen.sh.template', 'autogen.sh'],
    ['configure.in.template', 'configure.in'],
    ['COPYING.template', 'COPYING'],
    ['AUTHORS.template', 'AUTHORS'],
    ['MAINTAINERS.template', 'MAINTAINERS'],
    ['NEWS.template', 'NEWS'],
    ['README.template', 'README'],
    ['ChangeLog.template', 'ChangeLog'],
    ['Makefile.am.template', 'Makefile.am'],
    ['src_Makefile.am.template', os.path.join('src', 'Makefile.am')],
    ['app.c.template', os.path.join('src', sys.argv[2] + '.c')],
    ['window.h.template', os.path.join('src', 'window.h')],
    ['window.c.template', os.path.join('src', 'window.c')]]

def main(args):
    if len(args) != 4:
        sys.stderr.write(usage + '\n')
        sys.exit(1)

    # create a src dir if needed.
    try:
        os.mkdir(os.path.join(args[3], 'src'))
    except OSError: pass

    # some replacement variables.
    prefix = string.replace(args[1], '-', '_')
    parts = string.split(args[1], '-')
    Prefix = ''
    for s in parts:
        Prefix += string.capitalize(s)
    PREFIX = string.upper(prefix)
    user = pwd.getpwuid(os.getuid())[4]
    year = str(time.gmtime()[0])
    try:
        email = os.environ['EMAIL_ADDRESS']
    except KeyError:
        email = 'set EMAIL_ADDRESS environment variable'
        pass
    date = string.join([str(time.gmtime()[0]),
                        str(time.gmtime()[1]),
                        str(time.gmtime()[2])], '-')

    # copy source files.
    for file in files:
        data = open(os.path.join(templatepath, file[0])).read()
        data = string.replace(data, '$packagename$', args[1])
        data = string.replace(data, '$prefix$', prefix)
        data = string.replace(data, '$Prefix$', Prefix)
        data = string.replace(data, '$PREFIX$', PREFIX)
        data = string.replace(data, '$user$', user)
        data = string.replace(data, '$year$', year)
        data = string.replace(data, '$email$', email)
        data = string.replace(data, '$date$', date)
        filename = os.path.join(args[3], file[1])
        open(filename, 'w').write(data)

    # chmod a+x autogen.sh.
    filename = os.path.join(args[3], 'autogen.sh')
    os.chmod(filename, 0775)

if __name__ == '__main__':
    main(sys.argv[1:])
