/* Anjuta
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ANJUTA_GNOME_APPLICATION_TYPE_H
#define ANJUTA_GNOME_APPLICATION_TYPE_H

#include "../project-manager/anjuta-project-type.h"

G_BEGIN_DECLS

#define ANJUTA_TYPE_GNOME_APPLICATION_TYPE		(anjuta_gnome_application_type_get_type ())
#define ANJUTA_GNOME_APPLICATION_TYPE(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ANJUTA_TYPE_GNOME_APPLICATION_TYPE, AnjutaGnomeApplicationType))
#define ANJUTA_GNOME_APPLICATION_TYPE_CLASS(obj)	(G_TYPE_CHECK_CLASS_CAST ((klass), ANJUTA_TYPE_GNOME_APPLICATION_TYPE, AnjutaGnomeApplicationTypeClass))
#define ANJUTA_IS_GNOME_APPLICATION_TYPE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ANJUTA_TYPE_GNOME_APPLICATION_TYPE))
#define ANJUTA_IS_GNOME_APPLICATION_TYPE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), ANJUTA_TYPE_GNOME_APPLICATION_TYPE))

typedef struct _AnjutaGnomeApplicationType		AnjutaGnomeApplicationType;
typedef struct _AnjutaGnomeApplicationTypePrivate	AnjutaGnomeApplicationTypePrivate;
typedef struct _AnjutaGnomeApplicationTypeClass		AnjutaGnomeApplicationTypeClass;

struct _AnjutaGnomeApplicationType {
	AnjutaProjectType parent;

	AnjutaGnomeApplicationTypePrivate *priv;
};

struct _AnjutaGnomeApplicationTypeClass {
	AnjutaProjectTypeClass parent_class;
};

GType anjuta_gnome_application_type_get_type         (void);
AnjutaProjectType *anjuta_gnome_application_type_new (void);

G_END_DECLS

#endif /* ANJUTA_GNOME_APPLICATION_TYPE_H */
