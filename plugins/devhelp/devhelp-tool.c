/* Anjuta Project Manager component, based on gnome-build2 */

#include <config.h>

#include <string.h>
#include <libanjuta/libanjuta.h>
#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-event-source.h>
#include <bonobo/bonobo-i18n.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-ui-component.h>
#include <bonobo/bonobo-ui-util.h>
#include <bonobo/bonobo-widget.h>
#include <bonobo/bonobo-window.h>
#include <libanjuta/anjuta-doc.h>
#include "GNOME_DevHelp.h"

#include "devhelp-view.h"

typedef struct {
	AnjutaTool parent;

	Bonobo_Unknown controller;

	Bonobo_Control index;
	GtkWidget *search;
	GtkWidget *browser;
} HelpTool;
 
typedef struct {
	AnjutaToolClass parent;
} HelpToolClass;

static void current_word_cb (GtkWidget *widget, gpointer data, char *cmd);

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("CurrentWord", current_word_cb),
        BONOBO_UI_VERB_END
};

static gpointer parent_class = NULL;

static void 
current_word_cb (GtkWidget *widget, gpointer data, char *cmd)
{
	CORBA_Environment ev;
	AnjutaTool *tool = ANJUTA_TOOL (data);
	HelpTool *help_tool = data;
	char *str;
	
	str = anjuta_get_current_word (tool);

	CORBA_exception_init (&ev);
#if 0
	GNOME_Development_Shell_bringToFront (private->shell,
					      "DevhelpSearch", 
					      &ev);
#endif

	GNOME_DevHelp_Controller_search (help_tool->controller, str, &ev);
	CORBA_exception_free (&ev);
	
	g_free (str);
}

static void
controller_event_cb (BonoboListener *listener,
		     const char *event_name,
		     const CORBA_any *any,
		     CORBA_Environment *ev,
		     gpointer user_data)
{
	HelpTool *help_tool = (HelpTool*)user_data;
	
	if (!strcmp (event_name, "GNOME/DevHelp:URI:changed")) {
#if 0
		GNOME_Development_Shell_bringToFront (private->shell,
						      "HtmlBrowser",
						      ev);
#endif
		devhelp_view_open_uri (DEVHELP_VIEW (help_tool->browser),
				       any->_value,
				       NULL);
	}
}

static gboolean 
init_search_page (AnjutaTool *tool)
{
	CORBA_Environment ev;
	GtkWidget *vbox;
	Bonobo_Control remote_control;
	GtkWidget *w;
	HelpTool *help_tool = (HelpTool*)tool;

	CORBA_exception_init (&ev);

	vbox = gtk_vbox_new (FALSE, 0);

	remote_control = GNOME_DevHelp_Controller_getSearchEntry
		(help_tool->controller, &ev);
	w = bonobo_widget_new_control_from_objref (remote_control, 
						   BONOBO_OBJREF (bonobo_window_get_ui_container (BONOBO_WINDOW (tool->shell))));
	gtk_box_pack_start (GTK_BOX (vbox), w, FALSE, FALSE, 0);

	remote_control = GNOME_DevHelp_Controller_getSearchResultList
		(help_tool->controller, &ev);
	w = bonobo_widget_new_control_from_objref (remote_control, 
						   BONOBO_OBJREF (bonobo_window_get_ui_container (BONOBO_WINDOW (tool->shell))));
	
	gtk_box_pack_start (GTK_BOX (vbox), w, TRUE, TRUE, 0);
	
	gtk_widget_show_all (vbox);
	
	anjuta_shell_add_widget (tool->shell,
				 vbox,
				 "HelpSearch",
				 _("Help Search"),
				 NULL);

	CORBA_exception_free (&ev);
	
	return TRUE;
}

static gboolean 
setup_controller (AnjutaTool *tool)

{
	CORBA_Environment ev;
	GNOME_DevHelp_Controller obj;
	HelpTool *help_tool = (HelpTool*)tool;

	CORBA_exception_init (&ev);
	help_tool->controller = obj = bonobo_get_object
		("OAFIID:GNOME_DevHelp_Controller", 
		 "IDL:GNOME/DevHelp/Controller:1.0",
		 &ev);
	
	if (!BONOBO_EX (&ev)) {
		Bonobo_Control ctrl;
		
		bonobo_event_source_client_add_listener 
			(obj, controller_event_cb, 
			 NULL, NULL, tool);		

		ctrl = GNOME_DevHelp_Controller_getBookIndex (obj, &ev);
		
		help_tool->index = ctrl;
		
		anjuta_shell_add_control (tool->shell, help_tool->index,
					  "HelpIndex", "Help Index",
					  NULL);
		
		init_search_page (tool);
		
		CORBA_exception_free (&ev);

	}

	return TRUE;
}

static void
link_clicked_cb (GtkWidget *view, char *uri, HelpTool *help_tool)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);
	GNOME_DevHelp_Controller_openURI (help_tool->controller, uri, &ev);
	CORBA_exception_free (&ev);
}

static gboolean
setup_view (AnjutaTool *tool)
{
	GtkWidget *view;
	GtkWidget *scrolled;
	HelpTool *help_tool = (HelpTool*)tool;
	
	view = devhelp_view_new ();

 	g_signal_connect (HTML_VIEW (view)->document,
			  "link_clicked", 
			  G_CALLBACK (link_clicked_cb),
			  tool);

	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled),
					     GTK_SHADOW_IN);

	gtk_container_add (GTK_CONTAINER (scrolled), view);
	
	gtk_widget_show (view);
	gtk_widget_show (scrolled);

	anjuta_shell_add_widget (tool->shell, scrolled,
				 "HelpBrowser",
				 _("Browser"),
				 NULL);
	help_tool->browser = view;

	return TRUE;
}

static void
shell_set (AnjutaTool *tool)
{
	HelpTool *help_tool = (HelpTool*)tool;
	CORBA_Environment ev;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (ANJUTA_IS_TOOL (tool));
	
	CORBA_exception_init (&ev);

	anjuta_tool_merge_ui (tool, 
			      "anjuta-devhelp-tool",
			      DATADIR,
			      "anjuta-devhelp.xml",
			      verbs,
			      tool);

	setup_controller (tool);
	setup_view (tool);

	GNOME_DevHelp_Controller_addMenus (help_tool->controller,
					   BONOBO_OBJREF (bonobo_window_get_ui_container (BONOBO_WINDOW (tool->shell))),
					   &ev);
	
	CORBA_exception_free (&ev);
}

static void
dispose (GObject *obj)
{
	CORBA_Environment ev;
	AnjutaTool *tool = ANJUTA_TOOL (obj);
	HelpTool *help_tool = (HelpTool*)obj;

	if (help_tool->index != CORBA_OBJECT_NIL) {
		anjuta_shell_remove_value (tool->shell,
					   "HelpIndex",
					   NULL);
		help_tool->index = CORBA_OBJECT_NIL;
	}
	
	if (help_tool->search) {
		anjuta_shell_remove_value (tool->shell,
					   "HelpSearch",
					   NULL);
		help_tool->search = NULL;
	}
	
	if (help_tool->browser) {
		anjuta_shell_remove_value (tool->shell,
					   "HelpBrowser",
					   NULL);
		help_tool->browser = NULL;
	}

	anjuta_tool_unmerge_ui (tool);

	CORBA_exception_free (&ev);
}

static void
help_tool_instance_init (GObject *object)
{
}

static void
help_tool_class_init (GObjectClass *klass)
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);
	
	parent_class = g_type_class_peek_parent (klass);
	
	tool_class->shell_set = shell_set;
	
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (HelpTool, help_tool);

ANJUTA_SIMPLE_PLUGIN (HelpTool, help_tool);
