/* Anjuta
 * 
 * Copyright (C) 2001 Roberto Majadas "telemaco" <phoenix@nova.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */


/*
 * Anjuta Shell Output plugin
 *
 * With this plugin yo can run command like in a shell .
 * An you can use pipes and pipe anjuta's documents .
 * 
 */

#include <config.h>

#include <bonobo-activation/bonobo-activation.h>
#include <libbonobo.h>
#include <libbonoboui.h>
#include <gnome.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <gdl/GDL.h>
#include <gdl/gdl.h>
#include <libanjuta/libanjuta.h>
#include <libanjuta/anjuta-document-manager.h>

#define SHELL_OUTPUT_BUFFER_SIZE 1024
#define SHELL_OUTPUT_SH_NAME "sh"
#define SHELL_OUTPUT_SH_PATH "/bin/sh"

typedef struct {
	AnjutaTool parent;
} ShellOutputTool;

typedef struct {
	AnjutaToolClass parent;
} ShellOutputToolClass;

typedef struct _shell_output_dialog shell_output_dialog ;
typedef struct _shell_output_info shell_output_info ;

struct _shell_output_dialog {	
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *entry;
	GtkWidget *file_entry1;
	GtkWidget *file_entry2;
	GtkWidget *optionmenu;
	GtkWidget *optionmenu_menu;	
};

struct _shell_output_info {
	AnjutaDocument *doc ;
	gchar *work_directory;
	gchar *temp_file_name;	
};

static shell_output_dialog *sod ;
static shell_output_info *soi ;
static AnjutaTool* shell_output_tool ;

static gpointer parent_class = NULL;

/* Shell Output prototipes */

static void shell_output (GtkWidget *widget, gpointer data);
static void shell_output_add_dialog_options ();
static void shell_output_create_dialog_table ();
static void shell_output_create_dialog ();
static void shell_output_activate_option (GtkWidget *widget, gpointer data);
static void shell_output_close_dialog ();
static void shell_output_popen (GString *buffer_to_pipe, const gchar *command);
GString * shell_output_data_for_pipe_by_editorbuffer ();
GString * shell_output_data_for_pipe_by_persist_stream ();
GString * shell_output_data_for_pipe ();
static void shell_output_command_run ();
static void shell_output_open_output_file (gchar *file);
gboolean command_out_cb (GIOChannel *chan, GIOCondition cond, gpointer data);


static void
shell_output_add_dialog_options ()
{
	AnjutaDocument *document;
	AnjutaDocumentManager *docman;
	CORBA_Environment ev;
	GtkWidget *optionmenu_menu_item ;
	gchar *moniker;
	gchar *cat_option;
	gchar *filename;
	gint i ;
	gint num_docs;

	soi->doc = NULL ; /* inicialization */
		
	optionmenu_menu_item = gtk_menu_item_new_with_label (_("run command"));
	gtk_widget_show (optionmenu_menu_item);
	gtk_menu_append (GTK_MENU (sod->optionmenu_menu), optionmenu_menu_item);
	gtk_signal_connect (GTK_OBJECT (optionmenu_menu_item), "activate",
					GTK_SIGNAL_FUNC (shell_output_activate_option),NULL);	

	anjuta_shell_get (shell_output_tool->shell,
			  "DocumentManager",
			  ANJUTA_TYPE_DOCUMENT_MANAGER,
			  &docman,
			  NULL);

	num_docs = anjuta_document_manager_num_documents (docman);

	for (i=0 ; i<num_docs ; i++){		
		   document = anjuta_document_manager_get_nth_document(docman, 
													i);
		   g_print ("uri: %s\n", anjuta_document_get_uri (document));
		   
		   filename = g_strdup (anjuta_document_get_uri (document));

		   if (anjuta_document_get_uri (document)) {
			   
				 cat_option = g_strdup_printf ("cat %s |",
										 g_basename(filename));
				 optionmenu_menu_item = gtk_menu_item_new_with_label (cat_option);
				 gtk_widget_show (optionmenu_menu_item);
				 gtk_menu_append (GTK_MENU (sod->optionmenu_menu),
							   optionmenu_menu_item);
				 gtk_signal_connect (GTK_OBJECT (optionmenu_menu_item),
								 "activate",
								 GTK_SIGNAL_FUNC (shell_output_activate_option),
								 document);
				 g_free(cat_option);
		   }
		   
		   g_free(filename);
	}
	
	gtk_option_menu_set_menu (GTK_OPTION_MENU (sod->optionmenu),
						 sod->optionmenu_menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU(sod->optionmenu),
						    0);
}


static void
shell_output_create_dialog_table ()
{
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *combo_entry;
		
	table = gtk_table_new (3, 2, FALSE);
	gtk_widget_show (table);	
	gtk_box_pack_start (GTK_BOX (sod->vbox), table, TRUE, TRUE, 0);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);
	label = gtk_label_new (_("Work directory"));
	gtk_widget_show (label);	
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
				   (GtkAttachOptions) (GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	label = gtk_label_new (_("Temporary output file"));
	gtk_widget_show (label);	
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3,
				   (GtkAttachOptions) (GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);	
	sod->optionmenu = gtk_option_menu_new ();
	gtk_widget_show (sod->optionmenu);	
	gtk_table_attach (GTK_TABLE (table), sod->optionmenu, 0, 1, 0, 1,
				   (GtkAttachOptions) (GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	sod->optionmenu_menu = gtk_menu_new (); 
	shell_output_add_dialog_options ();
	sod->entry = gtk_entry_new ();
	gtk_widget_show (sod->entry);
	gtk_table_attach (GTK_TABLE (table), sod->entry, 1, 2, 0, 1,
				   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	sod->file_entry1 = gnome_file_entry_new (NULL, NULL);
	gtk_widget_show (sod->file_entry1);
	gtk_table_attach (GTK_TABLE (table), sod->file_entry1, 1, 2, 1, 2,
				   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	gnome_file_entry_set_directory_entry (GNOME_FILE_ENTRY (sod->file_entry1), TRUE);
	gnome_file_entry_set_default_path (GNOME_FILE_ENTRY (sod->file_entry1),
								g_get_home_dir ());
	combo_entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (sod->file_entry1));
	gtk_widget_show (combo_entry);
	gtk_entry_set_text (GTK_ENTRY(combo_entry), g_get_home_dir ()); 
	sod->file_entry2 = gnome_file_entry_new (NULL, NULL);
	gtk_widget_show (sod->file_entry2);
	gtk_table_attach (GTK_TABLE (table), sod->file_entry2, 1, 2, 2, 3,
				   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	gnome_file_entry_set_default_path (GNOME_FILE_ENTRY (sod->file_entry2),
								g_get_tmp_dir ());
	combo_entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (sod->file_entry2));
	gtk_widget_show (combo_entry);
	gnome_file_entry_set_default_path (GNOME_FILE_ENTRY (sod->file_entry2),
								g_get_tmp_dir ());
	gtk_entry_set_text (GTK_ENTRY(combo_entry), tempnam (g_get_tmp_dir (), "anjuta-"));
}

static void
shell_output_dialog_response (GtkWidget *widget, gint response_id,
			      gpointer data)
{
	if (response_id == GTK_RESPONSE_OK)
		shell_output_command_run ();
	else if (response_id == GTK_RESPONSE_CANCEL)
		shell_output_close_dialog ();
}

static void
shell_output_create_dialog ()
{
	GtkWidget *label;
	GtkWidget *dialog_vbox;

	GtkWidget *frame;
	
	sod->dialog = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (sod->dialog), _("Shell output"));
	gtk_dialog_add_buttons (GTK_DIALOG (sod->dialog),
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_OK, GTK_RESPONSE_OK,
				NULL);

	dialog_vbox = GTK_DIALOG (sod->dialog)->vbox;
	gtk_widget_show (dialog_vbox);
	sod->vbox = gtk_vbox_new (FALSE, 5);
	gtk_widget_show (sod->vbox);
	gtk_box_pack_start (GTK_BOX (dialog_vbox), sod->vbox, TRUE, TRUE, 0);
	frame = gtk_frame_new (NULL);
 	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (sod->vbox), frame, FALSE, FALSE, 0);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_OUT);
	label = gtk_label_new (_("\nPlease , insert command that you want to run\n Remember , you can pipe documents and commands like in a shell \n"));
	gtk_widget_show (label);	
	gtk_container_add (GTK_CONTAINER (frame), label);
	shell_output_create_dialog_table ();

	g_signal_connect (G_OBJECT (sod->dialog), "response",
			  G_CALLBACK (shell_output_dialog_response),
			  NULL);

	gtk_signal_connect (GTK_OBJECT (sod->dialog), "delete_event",
			    GTK_SIGNAL_FUNC (shell_output_close_dialog), NULL);

	gtk_widget_show (sod->dialog);
}

static void
shell_output_activate_option (GtkWidget *widget, gpointer data)
{	
	soi->doc = data ;
}

gboolean
command_out_cb (GIOChannel *chan, GIOCondition cond, gpointer data)
{
	FILE *temp_file;
	gchar c;
	gint read = 1;
			
	temp_file = fopen (soi->temp_file_name, "w");
	
	while (read > 0){
		g_io_channel_read (chan, &c, 1, &read);		
		if (read > 0)			
			fputc (c, temp_file);				
	}
	fclose (temp_file);
	shell_output_open_output_file (soi->temp_file_name);
	g_io_channel_unref (chan);
	g_io_channel_close (chan);
	g_free (soi->work_directory);
	g_free (soi->temp_file_name);
	g_free (soi);		

	return TRUE;
}

GString *
shell_output_data_for_pipe_by_editorbuffer ()
{
	GNOME_Development_EditorBuffer	buffer;
	GNOME_Development_EditorBuffer_iobuf   *iobuf;
	CORBA_Environment ev;
	glong length;
	GString *result;

	CORBA_exception_init(&ev);
		
	buffer = anjuta_get_editor_interface 
		(soi->doc, "IDL:GNOME/Development/EditorBuffer:1.0");
	
	if(CORBA_Object_is_nil(buffer, &ev)){
		bonobo_object_release_unref (buffer, &ev);		
		CORBA_exception_free (&ev);		
		return NULL;
	}
	
	result = g_string_new (NULL);		
	length = GNOME_Development_EditorBuffer_getLength (buffer, &ev);
	if (length == 0)
		return result ;
	GNOME_Development_EditorBuffer_getChars (buffer, 0, length, &iobuf, &ev);
	result->str = g_strndup (iobuf->_buffer, iobuf->_length);
	result->len = iobuf->_length ;	
	CORBA_free (iobuf);
	bonobo_object_release_unref (buffer, &ev);
		
	CORBA_exception_free (&ev);
	return result ;
}

GString *
shell_output_data_for_pipe_by_persist_stream ()
{
	Bonobo_PersistStream buffer_ps ;
	BonoboStream *ret = NULL;
	Bonobo_Stream_iobuf *iobufps;
	CORBA_Environment ev;
	glong length;
	GString *result;

	CORBA_exception_init(&ev);
		
	ret = bonobo_stream_mem_create (NULL, 0, FALSE, TRUE);
	buffer_ps = anjuta_get_editor_interface 
		(soi->doc, "IDL:Bonobo/PersistStream:1.0");
	
	if (CORBA_Object_is_nil(buffer_ps, &ev)){
		bonobo_object_release_unref (buffer_ps, &ev);
		CORBA_exception_free (&ev);
		return NULL;
	}
	result = g_string_new (NULL);	
	Bonobo_PersistStream_save (buffer_ps,
						  BONOBO_OBJREF (BONOBO_OBJECT (ret)),
						  "text/plain",
						  &ev);
	length = bonobo_stream_client_get_length (BONOBO_OBJREF (BONOBO_OBJECT (ret)),
									  &ev);
	if (length == 0)
		return result;

	Bonobo_Stream_seek ( BONOBO_OBJREF (BONOBO_OBJECT (ret)),
					 0, Bonobo_Stream_SeekSet, &ev);
	Bonobo_Stream_read (BONOBO_OBJREF (BONOBO_OBJECT (ret)),
					length, &iobufps, &ev);
	result->str = g_strndup (iobufps->_buffer, iobufps->_length);
	result->len = iobufps->_length;
	CORBA_free (iobufps);
	bonobo_object_release_unref (buffer_ps, &ev);
	CORBA_exception_free(&ev);
	
	return result;			
	
}
GString *
shell_output_data_for_pipe ()
{			

    	GString *result ;
	
	if (soi->doc == NULL){
		result = g_string_new (NULL); 
		return result;
	} else {
		result = shell_output_data_for_pipe_by_editorbuffer ();
		if (result==NULL){ 		
			result = shell_output_data_for_pipe_by_persist_stream();
			if (result==NULL){
				g_print ("anjuta can't read from component\n");
				return NULL;
			} 
		}
		return result ;
	}
}

static void
shell_output_command_run ()
{
	gchar    *warning        = NULL;
	const gchar *command_string = NULL;	
	GString  *buffer_to_pipe = NULL;
		
	command_string = gtk_entry_get_text (GTK_ENTRY (sod->entry));
	soi->work_directory = g_strdup (gnome_file_entry_get_full_path
						  (GNOME_FILE_ENTRY (sod->file_entry1),
						   TRUE));
	soi->temp_file_name = g_strdup (gnome_file_entry_get_full_path
							  (GNOME_FILE_ENTRY (sod->file_entry2),
							   FALSE));
    	if (soi->work_directory == NULL) {
		warning = g_strdup (_("There aren't any work directory"));
	} else if (soi->temp_file_name == NULL) {
		warning = g_strdup (_("There aren't any temporal file"));
	} else if (command_string == NULL || (strlen (command_string) == 0)) {
		warning = g_strdup (_("There aren't any commands"));
	} else if (!(buffer_to_pipe = shell_output_data_for_pipe ())) {
		/* If the return is NULL this mean that the component is not
		   soportated */
		warning = g_strdup (_("Anjuta can't read from this text-component\n"));

		gtk_widget_destroy (sod->dialog);
		g_free (sod);
		g_free (soi->work_directory);
		g_free (soi->temp_file_name);
		g_free (soi);
	} else {
		chdir (soi->work_directory);
		shell_output_popen (buffer_to_pipe, command_string);
		gtk_widget_destroy (sod->dialog);
		g_free (sod);
	}

	/*
	 * If there was an error, the dialog box has been prepared, so run it.
	 */
	if (warning) {
		GtkWidget *dialog;

		dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
						 GTK_MESSAGE_WARNING,
						 GTK_BUTTONS_OK,
						 warning);
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		g_free (warning);
	}
}

static void
shell_output_popen (GString *buffer_to_pipe, const gchar *command)
{
	FILE     *stream ;
	GIOChannel *chan_out;
	gint     fdpipe [2];
	gint     fdpipe2 [2];
	gchar    *arg[4] ;
	gchar    *command_to_run;
	gint     pid;

	if (pipe (fdpipe) == -1)	
		return;
	if (pipe (fdpipe2) == -1)
		return;
	if (buffer_to_pipe->len > 0)
		command_to_run = g_strdup_printf ("cat | %s", command);
	else
		command_to_run = g_strdup_printf (" %s ", command);
	
	pid = fork();
	if (pid == 0){
		dup2(fdpipe[0], 0);
		close (fdpipe[0]);
		close (fdpipe[1]);
		dup2(fdpipe2[1], 1);
		close (fdpipe2[0]);
		close (fdpipe2[1]);

		arg[0]=SHELL_OUTPUT_SH_NAME;
		arg[1]="-c";
		arg[2]=command_to_run;
		arg[3]=NULL;
		
		execvp (SHELL_OUTPUT_SH_PATH, arg);

		g_warning ("A undetermined PIPE problem occurred");
		_exit (1);
	} else {
		close (fdpipe[0]);
		close (fdpipe2[1]);
		
		chan_out = g_io_channel_unix_new (fdpipe2[0]);
		g_io_add_watch (chan_out, G_IO_IN, command_out_cb, NULL);

		if (buffer_to_pipe->len > 0){
			fcntl (fdpipe[1], F_SETFD, FD_CLOEXEC);
			stream = fdopen (fdpipe[1], "w");
			fprintf (stream, buffer_to_pipe->str);
			fclose (stream);			
		}

		g_string_free ( buffer_to_pipe, TRUE ) ;			
	}
}

static void
shell_output_open_output_file (gchar *file)
{
	AnjutaDocumentManager *docman;
	CORBA_Environment ev;
	
	anjuta_shell_get (shell_output_tool->shell, 
			  "DocumentManager",
			  ANJUTA_TYPE_DOCUMENT_MANAGER,
			  &docman,
			  NULL);

	anjuta_document_manager_open (docman,
				      file,
				      NULL);
}

static void
shell_output_close_dialog ()
{
	gtk_widget_destroy (sod->dialog);
	g_free (sod);
	g_free (soi);
}

static void
shell_output(
	GtkWidget*			widget,
	gpointer			data
)
{
	shell_output_tool = ANJUTA_TOOL(data);	

	sod = g_new (shell_output_dialog, 1);
	soi = g_new (shell_output_info, 1);
		
	shell_output_create_dialog ();
		
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("ShellOutput", shell_output),
	BONOBO_UI_VERB_END
};

static void
shell_set (AnjutaTool *tool)
{
	anjuta_tool_merge_ui (tool,
			      "anjuta-shelloutput-tool",
			      ANJUTA_DATADIR,
			      "anjuta-shelloutput-plugin.xml",
			      verbs, 
			      tool);
}

static void
dispose (GObject *obj)
{
	anjuta_tool_unmerge_ui (ANJUTA_TOOL (obj));
}

static void
shell_output_tool_instance_init (GObject *object)
{
}

static void
shell_output_tool_class_init (GObjectClass *klass)
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);
	
	parent_class = g_type_class_peek_parent (klass);
	
	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (ShellOutputTool, shell_output_tool);

ANJUTA_SIMPLE_PLUGIN (ShellOutputTool, shell_output_tool);
