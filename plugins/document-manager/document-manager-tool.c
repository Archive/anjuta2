/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * document-manager-tool.c
 * 
 * Copyright (C) 2000 Dave Camp
 * Copyright (C) 2002, 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/bonobo-file-selector-util.h>
#include <gconf/gconf-client.h>
#include <gdl/gdl-tools.h>
#include <libanjuta/anjuta-document-manager.h>
#include <libanjuta/anjuta-tool.h>
#include <libanjuta/anjuta-session.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include "anjuta-document-manager.h"
#include "anjuta-document.h"

#define DEFAULT_PATH_KEY "/apps/anjuta2/plugins/document_manager/default_path"

typedef struct {
	AnjutaTool parent;
	
	AnjutaDocumentManager *docman;
	AnjutaDocument *current_document;

	gchar *default_path;
	GSList *docs;

	GSList *files;
	int file_index;
} DocumentTool;

typedef struct {
	AnjutaToolClass parent;
} DocumentToolClass;

static void
file_new (BonoboUIComponent *uic, gpointer data, const char *cmd)
{
	DocumentTool *tool = data;
	
	anjuta_document_manager_new_document (tool->docman, 
					      "text/plain", 
					      NULL);
}

static void
file_open (BonoboUIComponent *uic, gpointer data, const char *cmd)
{
	DocumentTool *tool = data;
	char **files;
	
	files = bonobo_file_selector_open_multi (NULL, TRUE,
					      _("Open File"),
					      NULL, tool->default_path);
	
	if (files) {
		GnomeVFSURI *uri;
		char *file;
		int i = 0;
		
		/* set default_path to the path of the first file */
		if (tool->default_path) {
			g_free (tool->default_path);
			tool->default_path = NULL;
		}
		uri = gnome_vfs_uri_new (files [0]);
		if (uri) {
			tool->default_path = gnome_vfs_uri_extract_dirname (uri);
			gnome_vfs_uri_unref (uri);
		}
		
		file = files[i++];
		while (file) {
			anjuta_document_manager_open (tool->docman,
						      file, 
						      NULL);
			g_free (file);
			file = files[i++];
		}
		g_free (files);
	}
}

static void
file_reload (BonoboUIComponent *uic, gpointer data, const char *cmd)
{
	DocumentTool *tool = data;
	
	if (tool->current_document) {
		anjuta_bonobo_document_reload 
			(ANJUTA_BONOBO_DOCUMENT (tool->current_document));
	}
}

static void
file_save (BonoboUIComponent *uic, gpointer data, const char *cmd)
{
	DocumentTool *tool = data;

	if (tool->current_document) {
		anjuta_document_save (tool->current_document, NULL);
	}
}

static void 
file_save_as (BonoboUIComponent *uic, gpointer data, const char *cmd)
{
	DocumentTool *tool = data;
	
	if (tool->current_document) {
		anjuta_bonobo_document_save_as_dialog (ANJUTA_BONOBO_DOCUMENT (tool->current_document));
	}
}


static void
file_close (BonoboUIComponent *uic, gpointer data, const char *cmd)
{
	DocumentTool *tool = data;
	
	if (tool->current_document) {
		anjuta_document_manager_close (tool->docman,
					       tool->current_document,
					       NULL);
	}
}

static void
file_close_all (BonoboUIComponent *uic, gpointer data, const char *cmd)
{
	/* FIXME */
	g_warning ("file_close_all() not implemented");
}

static void 
file_save_all (BonoboUIComponent *uic, gpointer data, const char *cmd)
{
	DocumentTool *tool = data;

	anjuta_document_manager_save_all (tool->docman, NULL);
}

static BonoboUIVerb verbs [] = {
	BONOBO_UI_VERB ("FileNew", file_new),
	BONOBO_UI_VERB ("FileOpen", file_open),
        BONOBO_UI_VERB ("FileReload", file_reload),
        BONOBO_UI_VERB ("FileSave", file_save), 
        BONOBO_UI_VERB ("FileSaveAs", file_save_as),
        BONOBO_UI_VERB ("FileSaveAll", file_save_all),

	BONOBO_UI_VERB ("FileClose", file_close),
        BONOBO_UI_VERB ("FileCloseAll", file_close_all),
	
	BONOBO_UI_VERB_END
};


static void
set_current_document (AnjutaTool *tool, AnjutaDocument *doc)
{
	DocumentTool *doctool = (DocumentTool*)tool;

	doctool->current_document = doc;

	if (doc) {
		anjuta_shell_add (tool->shell, 
				  "DocumentManager::CurrentDocument",
				  ANJUTA_TYPE_BONOBO_DOCUMENT, doc,
				  NULL);
	} else {
		anjuta_shell_remove_value (tool->shell, 
					   "DocumentManager::CurrentDocument",
					   NULL);
	}
}

static void 
current_document_changed_cb (AnjutaNotebookDocumentManager *docman, 
			     AnjutaDocument *doc,
			     AnjutaTool *tool)
{
	set_current_document (tool, doc);
}

static void
document_added_cb (AnjutaNotebookDocumentManager *docman,
		   AnjutaDocument *doc,
		   AnjutaTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;

	doc_tool->docs = g_slist_append (doc_tool->docs, doc);
}

static void
document_removed_cb (AnjutaNotebookDocumentManager *docman,
		     AnjutaDocument *doc,
		     AnjutaTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;

	doc_tool->docs = g_slist_remove (doc_tool->docs, doc);
}

static gboolean
idle_load_cb (AnjutaTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	int i = doc_tool->file_index;

	if (g_slist_nth (doc_tool->files, i) != NULL) {
		anjuta_document_manager_open (doc_tool->docman,
					      g_slist_nth_data (doc_tool->files, i),
					      NULL);
		doc_tool->file_index++;
	} else {
		/* NOTE: program arguments are g_strdup so g_free is ok, but
		 * session arguments are xmlNodeGetContents and should really
		 * be xmlFree'ed. Just g_free them for now. */
		g_slist_foreach (doc_tool->files, (GFunc)g_free, NULL);
		g_slist_free (doc_tool->files);
		return FALSE;
	}

	return TRUE;
}

static void
session_load_cb (AnjutaShell *shell,
		 AnjutaTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	const char **args;
	int i;
	GSList *files = NULL;
	AnjutaSession *session;
	xmlNodePtr group, docs, doc;

	/* If the user startup anjuta2 with the intention to display a source
	 * file then don't load the previous session but only the files on the
	 * commandline. */
	anjuta_shell_get (tool->shell,
			  "Shell::ProgramArguments",
			  G_TYPE_POINTER,
			  &args,
			  NULL);
	if (args) {
		for (i = 0; args[i] != NULL; i++) {
			if (args[i][0] == '-' ||
			    g_str_has_suffix (args[i], ".anjuta"))
				continue;
			else
				files = g_slist_append (files, g_strdup (args[i]));
		}
		if (g_slist_length (files) > 0) {
			doc_tool->files = files;
			doc_tool->file_index = 0;
			g_idle_add ((GSourceFunc)idle_load_cb, tool);
		}
		return;
	}

	/* Load the source files from the previous session. */
	anjuta_shell_get (tool->shell,
			  "Shell::CurrentSession",
			  ANJUTA_TYPE_SESSION,
			  &session,
			  NULL);
	group = anjuta_session_get_group (session, "document-manager");
	if (group) {
		/* Get <documents> node. */
		for (docs = group->children; docs; docs = docs->next) {
			if (docs->type == XML_ELEMENT_NODE)
				if (!strcmp (docs->name, "documents"))
					break;
		}

		if (docs) {
			for (doc = docs->children; doc != NULL; doc = doc->next) {
				if (doc->type == XML_ELEMENT_NODE) {
					files = g_slist_append (files,
								xmlNodeGetContent (doc));
				}
			}
			doc_tool->files = files;
			doc_tool->file_index = 0;
			g_idle_add ((GSourceFunc)idle_load_cb, tool);
		}
	}
}

static void
session_save_cb (AnjutaShell *shell,
		 AnjutaTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	AnjutaSession *session;
	xmlNodePtr group, docs_node, child;
	GSList *docs, *l;
	AnjutaDocument *doc;

	/* Get the <documents> session node. */
	anjuta_shell_get (tool->shell,
			  "Shell::CurrentSession",
			  ANJUTA_TYPE_SESSION,
			  &session,
			  NULL);
	group = anjuta_session_get_group (session, "document-manager");
	if (!group) {
		group = anjuta_session_new_group (session, "document-manager");
		docs_node = xmlNewChild (group, NULL, "documents", NULL);
	} else {
		/* Get <documents> node. */
		for (docs_node = group->children; docs_node; docs_node = docs_node->next) {
			if (docs_node->type == XML_ELEMENT_NODE)
				if (!strcmp (docs_node->name, "documents"))
					break;
		}
	}

	/* Save session data & close documents. */
	docs = g_slist_copy (doc_tool->docs);
	for (l = docs; l != NULL; l = l->next) {
		doc = ANJUTA_DOCUMENT (l->data);
		if (anjuta_bonobo_document_is_untitled (ANJUTA_BONOBO_DOCUMENT (doc)))
			continue;

		/* Create new <document> node with session info. */
		child = xmlNewChild (docs_node, NULL, "document", anjuta_document_get_uri (doc));
	}
	g_slist_free (docs);
}

static void
shell_set (AnjutaTool *anjuta_tool)
{
	DocumentTool *tool = (DocumentTool*)anjuta_tool;
	BonoboUIContainer *container;
	GdkPixbuf *pixbuf;
	GtkWidget *preferences;
	GConfClient *gconf_client;

	g_signal_connect (G_OBJECT (anjuta_tool->shell),
			  "session_load",
			  G_CALLBACK (session_load_cb),
			  tool);
	g_signal_connect (G_OBJECT (anjuta_tool->shell),
			  "session_save",
			  G_CALLBACK (session_save_cb),
			  tool);

	anjuta_tool_merge_ui (anjuta_tool,
			      "anjuta-document-manager",
			      ANJUTA_DATADIR,
			      "anjuta-document-manager.xml",
			      verbs,
			      tool);

	container = bonobo_window_get_ui_container
		(BONOBO_WINDOW (anjuta_tool->shell));

	tool->docman = 
		ANJUTA_DOCUMENT_MANAGER (anjuta_notebook_document_manager_new
					 (BONOBO_OBJREF (container),
					  anjuta_tool->uic));

	g_signal_connect (G_OBJECT (tool->docman), 
			  "current_document_changed",
			  G_CALLBACK (current_document_changed_cb),
			  tool);
	g_signal_connect (G_OBJECT (tool->docman),
			  "document_added",
			  G_CALLBACK (document_added_cb),
			  tool);
	g_signal_connect (G_OBJECT (tool->docman),
			  "document_removed",
			  G_CALLBACK (document_removed_cb),
			  tool);

	anjuta_document_manager_new_document (tool->docman,
					      "text/plain",
					      NULL);

	gtk_widget_show (GTK_WIDGET (tool->docman));

	anjuta_shell_add_widget (anjuta_tool->shell,
				 GTK_WIDGET (tool->docman), 
				 "DocumentManager",
				 _("Documents"),
				 NULL);

	preferences = anjuta_notebook_document_manager_get_prefs_page ();
	anjuta_shell_add_preferences (anjuta_tool->shell,
				      preferences,
				      "DocumentManager::Preferences",
				      _("General"),
				      _("Documents"),
				      NULL);

	/* Get default path for open file dialog. */
	gconf_client = gconf_client_get_default ();
	tool->default_path = gconf_client_get_string (gconf_client, DEFAULT_PATH_KEY, NULL);
	g_object_unref (gconf_client);
}

static const char *
get_doc_label (AnjutaDocument *document)
{
	GtkWidget *label;
	const char *str;

	label = g_object_get_data (G_OBJECT (document),
				   "AnjutaNotebookDocumentManager::label");
	str = gtk_label_get_text (GTK_LABEL (label));

	return str;
}

static GtkResponseType
file_close_dialog (AnjutaDocument *current)
{
	char *filename;
	GtkResponseType ret;
	GtkWidget *dialog;
	GtkWidget *button;

	if (!anjuta_document_get_uri (current)) {
		char *label;

		label = g_strdup (get_doc_label (current));
		filename = g_path_get_basename (label);
		g_free (label);
	} else {
		filename = g_strdup (anjuta_document_get_uri (current));
	}

	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_QUESTION,
					 GTK_BUTTONS_NONE,
					 _("Do you want to save the changes you made to the document \"%s\"? \n\n"
					   "Your changes will be lost if you don't save them."),
					 filename);

	/* Add "Don't save" button. */
	button = gdl_button_new_with_stock_image (_("Do_n't save"), GTK_STOCK_NO);
	g_return_val_if_fail (button != NULL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				      button,
				      GTK_RESPONSE_NO);
	gtk_widget_show (button);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_SAVE,
			       GTK_RESPONSE_YES);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_YES);

	ret = gtk_dialog_run (GTK_DIALOG (dialog));

	gtk_widget_destroy (dialog);

	if (ret == GTK_RESPONSE_YES) {
		anjuta_document_save (current, NULL);
	}

	g_free (filename);

	return ret;
}

static gboolean
tool_shutdown (AnjutaTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	GSList *docs, *l;
	AnjutaDocument *doc;
	GtkResponseType resp;

	/* Save/discard any modified files first. */
	docs = g_slist_copy (doc_tool->docs);
	for (l = docs; l != NULL; l = l->next) {
		doc = ANJUTA_DOCUMENT (l->data);
		if (anjuta_bonobo_document_is_changed (ANJUTA_BONOBO_DOCUMENT (doc))) {
			resp = file_close_dialog (doc);
			if (resp == GTK_RESPONSE_NO) {
				anjuta_notebook_document_manager_remove_doc (ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (doc_tool->docman), doc);
			} else if (resp == GTK_RESPONSE_CANCEL) {
				g_slist_free (docs);
				return FALSE;
			}
		}
	}
	g_slist_free (docs);

	return TRUE;
}

static void
dispose (GObject *obj)
{
	DocumentTool *tool = (DocumentTool*)obj;

	if (tool->current_document) {
		set_current_document (ANJUTA_TOOL (tool), NULL);
	}

	if (tool->docs) {
		g_slist_free (tool->docs);
		tool->docs = NULL;
	}

	if (tool->docman) {
		anjuta_shell_remove_value (ANJUTA_TOOL (tool)->shell, 
					   "DocumentManager::Preferences", NULL);
		anjuta_shell_remove_value (ANJUTA_TOOL (tool)->shell, 
					   "DocumentManager", NULL);
		
		gtk_widget_destroy (GTK_WIDGET (tool->docman));
		anjuta_tool_unmerge_ui (ANJUTA_TOOL (tool));
		tool->docman = NULL;
	}

	if (tool->default_path) {
		GConfClient *gconf_client = gconf_client_get_default ();
		gconf_client_set_string (gconf_client, DEFAULT_PATH_KEY,
					 tool->default_path, NULL);
		g_object_unref (gconf_client);
		g_free (tool->default_path);
		tool->default_path = NULL;
	}
}

static void
document_tool_instance_init (GObject *object)
{
	DocumentTool *doc_tool = (DocumentTool *)object;

	doc_tool->docs = NULL;
}

static void
document_tool_class_init (GObjectClass *klass)
{
	AnjutaToolClass *tool_class = ANJUTA_TOOL_CLASS (klass);

	tool_class->shell_set = shell_set;
	tool_class->shutdown = tool_shutdown;
	klass->dispose = dispose;
}

ANJUTA_TOOL_BOILERPLATE (DocumentTool, document_tool);

ANJUTA_SIMPLE_PLUGIN (DocumentTool, document_tool);
