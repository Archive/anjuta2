/* Anjuta
 * 
 * Copyright (C) 2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include "anjuta-document-obj.h"
#include <libanjuta/libanjuta.h>
#include "anjuta-document.h"
#include <libbonobo.h>
#include <libgnome/gnome-macros.h>

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

BONOBO_BOILERPLATE (AnjutaDocumentObj, anjuta_document_obj, 
		    GNOME_Development_Document, 
		    BonoboObject, BONOBO_TYPE_OBJECT, 
		    BONOBO_REGISTER_TYPE_FULL);

AnjutaDocumentObj *
anjuta_document_obj_construct (AnjutaDocumentObj *docobj, AnjutaDocument *doc)
{
	docobj->doc = doc;

	return docobj;
}

AnjutaDocumentObj *
anjuta_document_obj_new (AnjutaDocument *doc)
{
	AnjutaDocumentObj *docobj = g_object_new (ANJUTA_DOCUMENT_OBJ_TYPE,
						  NULL);

	return anjuta_document_obj_construct (docobj, doc);
}

/* implementation functions */

static char *
impl_get_filename (PortableServer_Servant servant,
		   CORBA_Environment *ev)
{
	AnjutaDocumentObj *docobj = ANJUTA_DOCUMENT_OBJ (bonobo_object_from_servant (servant));
	char *filename = anjuta_document_get_filename (docobj->doc);
	filename = filename ? filename : "";
	
	return CORBA_string_dup (filename);
}

static char *
impl_get_mime_type (PortableServer_Servant servant,
		    CORBA_Environment *ev)
{
	AnjutaDocumentObj *docobj = ANJUTA_DOCUMENT_OBJ (bonobo_object_from_servant (servant));

	return CORBA_string_dup (anjuta_document_get_mime_type (docobj->doc));
}

static Bonobo_Control
impl_get_editor (PortableServer_Servant servant,
		 CORBA_Environment *ev)
{
	AnjutaDocumentObj *docobj = ANJUTA_DOCUMENT_OBJ (bonobo_object_from_servant (servant));
	Bonobo_Control ctrl;
	
	ctrl = anjuta_document_get_control (ANJUTA_DOCUMENT (docobj->doc));
	
	return bonobo_object_dup_ref (ctrl, ev);
}

static Bonobo_Unknown
impl_get_editor_interface (PortableServer_Servant servant,
			   const CORBA_char *repo_id,
			   CORBA_Environment *ev)
{
	AnjutaDocumentObj *docobj = ANJUTA_DOCUMENT_OBJ (bonobo_object_from_servant (servant));
	Bonobo_Control ctrl;
	Bonobo_Unknown obj;
	

	ctrl = anjuta_document_get_control (ANJUTA_DOCUMENT (docobj->doc));
	obj = Bonobo_Unknown_queryInterface (ctrl, repo_id, ev);
	
	return obj;
}

static void 
anjuta_document_obj_finalize (GObject *object)
{
	GNOME_CALL_PARENT (G_OBJECT_CLASS, finalize, (object));
}

static void
anjuta_document_obj_class_init (AnjutaDocumentObjClass *klass)
{
	GObjectClass *object_class = (GObjectClass *)klass;
	POA_GNOME_Development_Document__epv *epv = &klass->epv;
	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = anjuta_document_obj_finalize;

	epv->getFilename = impl_get_filename;
	epv->getMimeType = impl_get_mime_type;
	epv->getEditor = impl_get_editor;
	epv->getEditorInterface = impl_get_editor_interface;
}

static void 
anjuta_document_obj_instance_init (AnjutaDocumentObj *bs)
{
}


