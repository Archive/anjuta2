/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * anjuta-document.h
 * 
 * Copyright (C) 1998-2000 Steffen Kern
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ANJUTA_BONOBO_DOCUMENT_H__
#define __ANJUTA_BONOBO_DOCUMENT_H__

#include <libbonoboui.h>

G_BEGIN_DECLS

#define ANJUTA_TYPE_BONOBO_DOCUMENT        (anjuta_bonobo_document_get_type ())
#define ANJUTA_BONOBO_DOCUMENT(o)          (GTK_CHECK_CAST ((o), ANJUTA_TYPE_BONOBO_DOCUMENT, AnjutaBonoboDocument))
#define ANJUTA_BONOBO_DOCUMENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_TYPE_BONOBO_DOCUMENT, AnjutaBonoboDocumentClass))
#define ANJUTA_IS_BONOBO_DOCUMENT(o)       (GTK_CHECK_TYPE ((o), ANJUTA_TYPE_BONOBO_DOCUMENT))
#define ANJUTA_IS_BONOBO_DOCUMENT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_TYPE_BONOBO_DOCUMENT))

typedef struct _AnjutaBonoboDocument            AnjutaBonoboDocument;
typedef struct _AnjutaBonoboDocumentClass       AnjutaBonoboDocumentClass;
typedef struct _AnjutaBonoboDocumentPrivate     AnjutaBonoboDocumentPrivate;

struct _AnjutaBonoboDocumentClass {
	GtkVBoxClass vbox;

	void (* changed)    (AnjutaBonoboDocument *document,
			     int                   change_type);
	void (* readonly)   (AnjutaBonoboDocument *document);
	void (* unreadonly) (AnjutaBonoboDocument *document);
	void (* source)     (AnjutaBonoboDocument *document,
			     char                 *filename);
	void (* cursor)     (AnjutaBonoboDocument* document);
	void (* focus)      (AnjutaBonoboDocument* document);
};

struct _AnjutaBonoboDocument {
	GtkVBox parent;
	
	Bonobo_UIContainer ui_container;
	BonoboControlFrame *control_frame;
	GtkWidget *bonobo_widget;
	Bonobo_PersistFile persist_file;
	Bonobo_PersistStream persist_stream;
	
	gboolean file_loaded;
	char *uri;
	char *mime_type;
	gboolean untitled;

	AnjutaBonoboDocumentPrivate *priv;
};

/* creation */
GType          anjuta_bonobo_document_get_type           (void);
GtkWidget   *  anjuta_bonobo_document_new                (Bonobo_UIContainer    ui_container);

/* file access */
void           anjuta_bonobo_document_make_temp          (AnjutaBonoboDocument *document,
							  const char           *mime_type);
gboolean       anjuta_bonobo_document_load_template      (AnjutaBonoboDocument *document,
							  const char           *uri);
gboolean       anjuta_bonobo_document_load_uri           (AnjutaBonoboDocument *document,
							  const char           *uri);
void           anjuta_bonobo_document_reload             (AnjutaBonoboDocument *document);
void           anjuta_bonobo_document_save_uri           (AnjutaBonoboDocument *document,
							  const char           *uri);
void           anjuta_bonobo_document_save_as_dialog     (AnjutaBonoboDocument *document);

/* Functions to get/set state */
gboolean       anjuta_bonobo_document_is_free            (AnjutaBonoboDocument *document);
gboolean       anjuta_bonobo_document_is_changed         (AnjutaBonoboDocument *document);
void           anjuta_bonobo_document_set_changed_state  (AnjutaBonoboDocument *document,
							  gboolean              state);
gboolean       anjuta_bonobo_document_is_readonly        (AnjutaBonoboDocument *document);
void           anjuta_bonobo_document_set_readonly_state (AnjutaBonoboDocument *document,
							  gboolean              state);
gboolean       anjuta_bonobo_document_is_busy            (AnjutaBonoboDocument *document);
void           anjuta_bonobo_document_set_busy_state     (AnjutaBonoboDocument *document,
							  gboolean              state);
void           anjuta_bonobo_document_set_last_mod       (AnjutaBonoboDocument *document,
							  int                   last_mod);
gboolean       anjuta_bonobo_document_is_untitled        (AnjutaBonoboDocument *document);
void           anjuta_bonobo_document_check_changed      (AnjutaBonoboDocument *document);
void           anjuta_bonobo_document_set_cfg_values     (AnjutaBonoboDocument *document);
gboolean       anjuta_bonobo_document_supports_modified  (AnjutaBonoboDocument *document);

G_END_DECLS

#endif /* __ANJUTA_BONOBO_DOCUMENT_H__ */
