/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * anjuta-document-manager.h
 * 
 * Copyright (C) 2000-2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ANJUTA_NOTEBOOK_DOCUMENT_MANAGER_H__
#define __ANJUTA_NOTEBOOK_DOCUMENT_MANAGER_H__

#include <gtk/gtknotebook.h>
#include <libanjuta/anjuta-document.h>

G_BEGIN_DECLS
 
typedef struct _AnjutaNotebookDocumentManager      AnjutaNotebookDocumentManager;
typedef struct _AnjutaNotebookDocumentManagerClass AnjutaNotebookDocumentManagerClass;
typedef struct _AnjutaNotebookDocumentManagerPriv  AnjutaNotebookDocumentManagerPriv;

#define ANJUTA_TYPE_NOTEBOOK_DOCUMENT_MANAGER        (anjuta_notebook_document_manager_get_type ())
#define ANJUTA_NOTEBOOK_DOCUMENT_MANAGER(o)          (GTK_CHECK_CAST ((o), ANJUTA_TYPE_NOTEBOOK_DOCUMENT_MANAGER, AnjutaNotebookDocumentManager))
#define ANJUTA_NOTEBOOK_DOCUMENT_MANAGER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_TYPE_NOTEBOOK_DOCUMENT_MANAGER, AnjutaNotebookDocumentManagerClass))
#define ANJUTA_IS_NOTEBOOK_DOCUMENT_MANAGER(o)       (GTK_CHECK_TYPE ((o), ANJUTA_TYPE_NOTEBOOK_DOCUMENT_MANAGER))
#define ANJUTA_IS_NOTEBOOK_DOCUMENT_MANAGER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_TYPE_NOTEBOOK_DOCUMENT_MANAGER))

struct _AnjutaNotebookDocumentManager {
	GtkNotebook parent;

	AnjutaDocument *current_document;

	GList *documents;
	BonoboUIComponent *ui_component;
	Bonobo_UIContainer ui_container;

	AnjutaNotebookDocumentManagerPriv *priv;
};

struct _AnjutaNotebookDocumentManagerClass {
	GtkNotebookClass parent_class;

	void (* document_added)   (AnjutaNotebookDocumentManager *docman,
				   AnjutaDocument                *document);
	void (* document_removed) (AnjutaNotebookDocumentManager *docman,
				   AnjutaDocument                *document);
};

GType      anjuta_notebook_document_manager_get_type       (void);
GtkWidget *anjuta_notebook_document_manager_new            (Bonobo_UIContainer ui_container,
							    BonoboUIComponent *ui_component);

GtkWidget *anjuta_notebook_document_manager_get_prefs_page (void);

void       anjuta_notebook_document_manager_remove_doc     (AnjutaNotebookDocumentManager *docman,
							    AnjutaDocument    *document);

G_END_DECLS

#endif /* __ANJUTA_NOTEBOOK_DOCUMENT_MANAGER_H__ */
