/* $Header$ */
/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *               2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef FILE_OPS_H
#define FILE_OPS_H

#include "anjuta-document-manager.h"

void            file_new          (GtkWidget             *widget,
				   gpointer               data);
AnjutaDocument *file_open         (GtkWidget             *widget,
				   gpointer               data);
void            file_open_ok      (GtkWidget             *widget,
				   GtkFileSelection      *sel);
void            file_reload       (GtkWidget             *widget,
				   gpointer               data);
void            file_save         (GtkWidget             *widget,
				   gpointer               data);
void            file_save_as      (GtkWidget             *widget,
				   gpointer               data);
void            file_save_cancel  (GtkWidget             *widget,
				   GtkFileSelection      *sel);
void            file_close        (GtkWidget             *widget,
				   gpointer               data);
void            file_exit         (GtkWidget             *widget,
				   gpointer               data);
void            file_autosave     (GtkWidget             *widget,
				   gpointer               data);
void            file_close_all    (GtkWidget             *widget,
				   gpointer               data);
void            file_save_all     (GtkWidget             *widget,
				   gpointer               data);
GtkResponseType file_close_dialog (AnjutaDocumentManager *docman,
				   AnjutaDocument        *current);
glong           check_doc_changed (AnjutaDocument        *doc);

#endif
