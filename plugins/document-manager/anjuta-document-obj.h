/* Anjuta
 * 
 * Copyright (C) 2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */


#ifndef ANJUTA_DOCUMENT_OBJ_H
#define ANJUTA_DOCUMENT_OBJ_H

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-event-source.h>
#include <libanjuta/libanjuta.h>

G_BEGIN_DECLS

#define ANJUTA_DOCUMENT_OBJ_TYPE                (anjuta_document_obj_get_type ())
#define ANJUTA_DOCUMENT_OBJ(o)                  (GTK_CHECK_CAST ((o), ANJUTA_DOCUMENT_OBJ_TYPE, AnjutaDocumentObj))
#define ANJUTA_DOCUMENT_OBJ_CLASS(k)            (GTK_CHECK_CLASS_CAST((k), ANJUTA_DOCUMENT_OBJ_TYPE, AnjutaDocumentObjClass))
#define ANJUTA_IS_DOCUMENT_OBJ(o)               (GTK_CHECK_TYPE ((o), ANJUTA_DOCUMENT_OBJ_TYPE))
#define ANJUTA_IS_DOCUMENT_OBJ_CLASS(k)         (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_DOCUMENT_OBJ_TYPE))

typedef struct _AnjutaDocumentObj      AnjutaDocumentObj;

struct _AnjutaDocumentObj {
	BonoboObject parent;
	struct _AnjutaDocument *doc;
};

typedef struct {
	BonoboObjectClass parent;
	POA_GNOME_Development_Document__epv epv;
} AnjutaDocumentObjClass;

GType              anjuta_document_obj_get_type     (void);
AnjutaDocumentObj *anjuta_document_obj_new          (struct _AnjutaDocument *anjuta_doc);
AnjutaDocumentObj *anjuta_document_obj_construct    (AnjutaDocumentObj *doc,
						     struct _AnjutaDocument *anjuta_doc);

G_END_DECLS

#endif
