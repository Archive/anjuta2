/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * anjuta-document-manager.c
 * 
 * Copyright (C) 1998-2000 Steffen Kern
 * Copyright (C) 2000 Dave Camp
 * Copyright (C) 2002, 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <libanjuta/anjuta-document-manager.h>
#include <libanjuta/anjuta-utils.h>
#include <libanjuta/gconf-property-editor.h>
#include <libgnome/gnome-macros.h>
#include <libgnomeui/gnome-uidefs.h>
#include <libgnomevfs/gnome-vfs-directory.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include <glade/glade-xml.h>
#include <gdl/gdl-icons.h>
#include <gdl/gdl-recent.h>
#include <gdl/gdl-tools.h>
#include "anjuta-document.h"
#include "anjuta-document-manager.h"

#define ANJUTA_DOCMAN_PREFIX             "/apps/anjuta2/plugins/document_manager"
#define ANJUTA_DOCMAN_RECENT_FILES       ANJUTA_DOCMAN_PREFIX "/recent_files"
#define ANJUTA_DOCMAN_RECENT_FILES_LIMIT ANJUTA_DOCMAN_PREFIX "/recent_files_limit"
#define ANJUTA_DOCMAN_TAB_LOCATION       ANJUTA_DOCMAN_PREFIX "/tab_location"
#define ANJUTA_DOCMAN_DEFAULT_MIME_TYPE  ANJUTA_DOCMAN_PREFIX "/default_mime_type"

struct _AnjutaNotebookDocumentManagerPriv {
	unsigned long untitled_count;

	guint recent_notify;
	guint tab_location_notify;

	GdlRecent *recent_files;
	GdlIcons *icons;

	GHashTable *templates;
	GHashTable *verbs;

	GConfClient *client;
};

typedef struct {
	char *uri;
	char *mime_type;
	char *label;
	char *tip;
} NewItemMenuData;

static void docman_finalize (GObject *object);

static void docman_switch_notebookpage(GtkWidget* widget,
				       GtkNotebookPage* page,
				       gint page_num,
				       gpointer data);
static void docman_recent_files (GdlRecent  *recent,
				 const char *uri,
				 gpointer    data);

static void docman_doc_modified (GtkWidget *widget, gpointer data);
static void docman_doc_unmodified (GtkWidget *widget, gpointer data);
static void docman_doc_uri_changed (GtkWidget *widget, 
				    const char *uri, 
				    gpointer data);
static void docman_doc_destroy (GtkWidget *widget, gpointer data);
static void set_current_document (AnjutaNotebookDocumentManager *docman,
				  AnjutaDocument *doc);
static void load_new_menu (AnjutaNotebookDocumentManager *docman);

gpointer parent_class;

/* public routines */

GtkWidget *
anjuta_notebook_document_manager_new (Bonobo_UIContainer ui_container,
				      BonoboUIComponent *ui_component)
{
	AnjutaNotebookDocumentManager *dm;

	dm = g_object_new (ANJUTA_TYPE_NOTEBOOK_DOCUMENT_MANAGER, NULL);

	dm->ui_container = ui_container;
	dm->ui_component = ui_component;

	load_new_menu (dm);

	gdl_recent_set_ui_component (dm->priv->recent_files, ui_component);

	return GTK_WIDGET (dm);
}

/* Document Manipulation */


static const char *
get_doc_label (AnjutaNotebookDocumentManager *docman, 
	       AnjutaDocument *document)
{
	GtkWidget *label;
	const char *str;

	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (document != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT (document), NULL);
	
	label = g_object_get_data (G_OBJECT (document), 
				    "AnjutaNotebookDocumentManager::label");
	str = gtk_label_get_text (GTK_LABEL (label));
	
	return str;
}

static void
set_doc_uri (AnjutaNotebookDocumentManager *docman,
	       AnjutaDocument *document,
	       const char *filename)
{
	gchar *basename;
	GdkPixbuf *pixbuf;
	GtkWidget *label;
	GtkWidget *icon;

	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));
	g_return_if_fail (filename != NULL);
	
	basename = g_path_get_basename (filename);
	label = g_object_get_data (G_OBJECT (document), 
				   "AnjutaNotebookDocumentManager::label");
	gtk_label_set_text (GTK_LABEL (label), basename);
	g_free (basename);

	pixbuf = gdl_icons_get_uri_icon (docman->priv->icons, filename);
	icon = g_object_get_data (G_OBJECT (document),
				  "AnjutaNotebookDocumentManager::icon");
	gtk_image_set_from_pixbuf (GTK_IMAGE (icon), pixbuf);
	g_object_unref (pixbuf);
}

void
anjuta_notebook_document_manager_remove_doc (AnjutaNotebookDocumentManager *docman,
					     AnjutaDocument *document)
{
	int index;
	
	g_object_ref (document);
	index = gtk_notebook_page_num (GTK_NOTEBOOK (docman), 
				       GTK_WIDGET (document));
	if (index > -1) {
		gtk_notebook_remove_page (GTK_NOTEBOOK (docman), index);
		
		if (anjuta_bonobo_document_is_untitled (ANJUTA_BONOBO_DOCUMENT (document))) {
			docman->priv->untitled_count--;
		}

		g_signal_emit_by_name (docman, "document_removed", document);

		docman->documents = 
			g_list_remove (docman->documents, document);
	}
	g_object_unref (document);
}

static GtkResponseType
file_close_dialog (AnjutaNotebookDocumentManager *docman, 
		   AnjutaDocument *current)
{
	char *filename;
	GtkResponseType ret;
	GtkWidget *dialog;
	GtkWidget *button;

	if (!anjuta_document_get_uri (ANJUTA_DOCUMENT (current))) {
		char *label;

		label = g_strdup (get_doc_label (docman, current));
		filename = g_path_get_basename (label);
		g_free (label);
	} else {
		filename = g_strdup (anjuta_document_get_uri (ANJUTA_DOCUMENT (current)));
	}

	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_QUESTION,
					 GTK_BUTTONS_NONE,
					 _("Do you want to save the changes you made to the document \"%s\"? \n\n"
					   "Your changes will be lost if you don't save them."),
					 filename);

	/* Add "Don't save" button. */
	button = gdl_button_new_with_stock_image (_("Do_n't save"), GTK_STOCK_NO);
	g_return_val_if_fail (button != NULL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				      button,
				      GTK_RESPONSE_NO);
	gtk_widget_show (button);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_SAVE,
			       GTK_RESPONSE_YES);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_YES);

	ret = gtk_dialog_run (GTK_DIALOG (dialog));

	gtk_widget_destroy (dialog);

	if (ret == GTK_RESPONSE_YES) {
		anjuta_document_save (ANJUTA_DOCUMENT (current), NULL);
	}

	g_free (filename);

	return ret;
}

static void
close_document_cb (GtkButton *button,
		   AnjutaNotebookDocumentManager *docman)
{
	AnjutaDocument *document;

	document = g_object_get_data (G_OBJECT (button), "document");

	if (anjuta_bonobo_document_is_changed (ANJUTA_BONOBO_DOCUMENT (document))) {
		if (file_close_dialog (docman, document) == GTK_RESPONSE_CANCEL)
			return; 
	}

	anjuta_notebook_document_manager_remove_doc (docman, document);

	if (!docman->documents) {
		anjuta_document_manager_new_document (ANJUTA_DOCUMENT_MANAGER (docman),
						      "text/plain",
						      NULL);
	}

}

static void
add_doc (AnjutaNotebookDocumentManager *docman, AnjutaDocument *document)
{
	GtkWidget *tab_hbox;
	GtkWidget *label;
	GtkWidget *button;
	GtkWidget *pixmap;
	GdkPixbuf *pixbuf;
	char *label_str;
	const char *uri;
	const char *mime_type;
	GtkWidget *icon;
	
	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));

	/* Get or create filename & label string. */
	uri = anjuta_document_get_uri (ANJUTA_DOCUMENT (document));
	if (uri) {
		label_str = g_path_get_basename (uri);
	} else {
		label_str = g_strdup_printf ("Untitled %ld", ++docman->priv->untitled_count);
		ANJUTA_BONOBO_DOCUMENT (document)->untitled = TRUE;
	}

	mime_type = anjuta_document_get_mime_type (document);
	pixbuf = gdl_icons_get_mime_icon (docman->priv->icons, mime_type);
	icon = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (pixbuf);

	/* Remove untitled document first if it's the current active document. */
	if (docman->current_document && uri &&
	    anjuta_bonobo_document_is_untitled (ANJUTA_BONOBO_DOCUMENT (docman->current_document)) &&
	    !anjuta_bonobo_document_is_changed (ANJUTA_BONOBO_DOCUMENT (docman->current_document))) {
		anjuta_notebook_document_manager_remove_doc (docman, docman->current_document);
	}

	/* Add the document to the list */
	docman->documents = g_list_append (docman->documents, document);

	label = gtk_label_new (label_str);
	g_object_set_data (G_OBJECT (document),
			   "AnjutaNotebookDocumentManager::label", 
			   label);
	g_object_set_data (G_OBJECT (document),
			   "AnjutaNotebookDocumentManager::icon",
			   icon);

	g_free (label_str);

	/* Build the tab widget close button */
	tab_hbox = gtk_hbox_new (FALSE, 2);
	pixmap = gtk_image_new_from_stock (GTK_STOCK_CLOSE, GTK_ICON_SIZE_MENU);;
	button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	gtk_container_add (GTK_CONTAINER (button), pixmap);
	gtk_widget_set_size_request (button, 18, 18);

	g_object_set_data (G_OBJECT (button), "document", 
			   (gpointer) document);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (close_document_cb), docman);

	gtk_box_pack_start (GTK_BOX (tab_hbox), icon, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (tab_hbox), label, TRUE, FALSE, 0);
	gtk_box_pack_end (GTK_BOX (tab_hbox), button, FALSE, FALSE, 0);

	gtk_widget_show_all (tab_hbox);

	gtk_notebook_append_page (GTK_NOTEBOOK (docman),
				  GTK_WIDGET (document), tab_hbox);
	
	/* Listen for changes */
	g_signal_connect (document, "modified", 
			  G_CALLBACK (docman_doc_modified), 
			  docman);
	g_signal_connect (document, "unmodified",
			  G_CALLBACK (docman_doc_unmodified),
			  docman);
	g_signal_connect (document, "uri_changed",
			  G_CALLBACK (docman_doc_uri_changed),
			  docman);
	

	g_signal_connect (G_OBJECT(document), "destroy",
			  G_CALLBACK (docman_doc_destroy),
			  (gpointer) docman);

	gtk_widget_show (GTK_WIDGET (document));
	gtk_widget_grab_focus (GTK_WIDGET(document));

	/* Add the document to the recent files list. */
	if (!anjuta_bonobo_document_is_untitled (ANJUTA_BONOBO_DOCUMENT (document)))
		gdl_recent_add (docman->priv->recent_files, uri);

	/* Flip to the new page, will set off the signal */
	anjuta_document_manager_show_document 
		(ANJUTA_DOCUMENT_MANAGER (docman), document);

	/* Well, it won't always set off the signal.  Not the first time. */
	if (!docman->current_document) {
		set_current_document (docman, document);
	}

	g_signal_emit_by_name (docman, "document_added", document);
}

/* private routines */

static void
docman_dispose (GObject *object)
{
	AnjutaNotebookDocumentManager *docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (object);
	GList *l;

	if (docman->priv->client) {
		gconf_client_notify_remove (docman->priv->client,
					    docman->priv->recent_notify);
		gconf_client_notify_remove (docman->priv->client,
					    docman->priv->tab_location_notify);
		g_object_unref (docman->priv->client);
		docman->priv->client = NULL;
	}

	if (docman->priv->recent_files) {
		g_object_unref (docman->priv->recent_files);
		docman->priv->recent_files = NULL;
	}

	if (docman->priv->icons) {
		g_object_unref (docman->priv->icons);
		docman->priv->icons = NULL;
	}

	if (docman->documents) {
		for (l = docman->documents; l != NULL; l = l->next) {
			AnjutaBonoboDocument *doc = ANJUTA_BONOBO_DOCUMENT (l->data);
			gtk_object_destroy (GTK_OBJECT (doc));
		}
		g_list_free (docman->documents);
		docman->documents = NULL;
	}
}

static void
docman_finalize (GObject *object) 
{
	AnjutaNotebookDocumentManager *docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (object);

	g_hash_table_destroy (docman->priv->templates);
	g_hash_table_destroy (docman->priv->verbs);

	g_free (docman->priv);

        G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
anjuta_notebook_document_manager_class_init (AnjutaNotebookDocumentManagerClass *klass) 
{
	GObjectClass *object_class = (GObjectClass*) klass;
	parent_class = g_type_class_peek_parent (klass);
	
	object_class->dispose = docman_dispose;
	object_class->finalize = docman_finalize;

	g_signal_new ("document_added",
		      G_TYPE_FROM_CLASS (klass),
		      G_SIGNAL_RUN_LAST,
		      G_STRUCT_OFFSET (AnjutaNotebookDocumentManagerClass,
				       document_added),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__OBJECT,
		      G_TYPE_NONE, 1,
		      ANJUTA_TYPE_DOCUMENT);
	g_signal_new ("document_removed",
		      G_TYPE_FROM_CLASS (klass),
		      G_SIGNAL_RUN_LAST,
		      G_STRUCT_OFFSET (AnjutaNotebookDocumentManagerClass,
				       document_removed),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__OBJECT,
		      G_TYPE_NONE, 1,
		      ANJUTA_TYPE_DOCUMENT);
}

static void
recent_limit_notify_cb (GConfClient *client,
			guint cnxn_id,
			GConfEntry *entry,
			gpointer user_data)
{
	GConfValue *value;
	AnjutaNotebookDocumentManager *dm = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (user_data);
	
	value = gconf_entry_get_value (entry);
	if (value) {
		gdl_recent_set_limit (dm->priv->recent_files,
				      gconf_value_get_int (value));
	}
}

static void
set_tab_pos_from_gconf (AnjutaNotebookDocumentManager *dm)
{
	char *str;

	str = gconf_client_get_string (dm->priv->client,
				       ANJUTA_DOCMAN_TAB_LOCATION, NULL);
	
	if (str) {
		GEnumClass *class;
		GEnumValue *val;
		int position;

		class = g_type_class_ref (gtk_position_type_get_type());
		
		val = g_enum_get_value_by_name (class, str);
		if (!val) {
			position = GTK_POS_BOTTOM;
		} else {
			position = val->value;
		}

		g_type_class_unref (class);

		gtk_notebook_set_tab_pos (GTK_NOTEBOOK (dm), position);
	}
}	

static void
tab_location_notify_cb (GConfClient *client,
			guint cnxn_id,
			GConfEntry *entry,
			gpointer user_data)
{
	set_tab_pos_from_gconf (ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (user_data));
}

static void
anjuta_notebook_document_manager_instance_init (AnjutaNotebookDocumentManager *dm)
{
	dm->priv = g_new0 (AnjutaNotebookDocumentManagerPriv, 1);

	dm->priv->untitled_count = 0;

	dm->priv->client = gconf_client_get_default ();
	gconf_client_add_dir (dm->priv->client, 
			      ANJUTA_DOCMAN_PREFIX,
			      GCONF_CLIENT_PRELOAD_NONE,
			      NULL);

	dm->priv->recent_files = gdl_recent_new (ANJUTA_DOCMAN_RECENT_FILES,
						 "/menu/File/DocumentOps/FileRecent/RecentFiles",
						 gconf_client_get_int (dm->priv->client,
								       ANJUTA_DOCMAN_RECENT_FILES_LIMIT,
								       NULL),
						 GDL_RECENT_LIST_NUMERIC);

	dm->priv->icons = gdl_icons_new (24, 16.0);

	dm->priv->recent_notify = gconf_client_notify_add
		(dm->priv->client,
		 ANJUTA_DOCMAN_RECENT_FILES_LIMIT,
		 recent_limit_notify_cb,
		 dm, NULL, NULL);
	dm->priv->tab_location_notify = gconf_client_notify_add
		(dm->priv->client,
		 ANJUTA_DOCMAN_TAB_LOCATION,
		 tab_location_notify_cb,
		 dm, NULL, NULL);

	gtk_notebook_set_scrollable (GTK_NOTEBOOK (dm), TRUE);

	g_signal_connect_after (G_OBJECT (dm), "switch_page",
				G_CALLBACK (docman_switch_notebookpage),
				dm);

	set_tab_pos_from_gconf (dm);

	g_signal_connect (G_OBJECT (dm->priv->recent_files),
			  "activate",
			  G_CALLBACK (docman_recent_files),
			  dm);
}

static void
docman_switch_notebookpage(GtkWidget* widget, GtkNotebookPage* page,
			   gint page_num, gpointer data)
{
        AnjutaDocument *document;
	AnjutaNotebookDocumentManager *docman;
	
	g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));
	
        docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (data);
        
        g_return_if_fail (docman);

        document = anjuta_document_manager_get_nth_document
		(ANJUTA_DOCUMENT_MANAGER (docman), page_num);
	
	if (!document) {
		g_warning ("Couldn't find document\n");
		return;
	}
	
        gtk_widget_grab_focus (GTK_WIDGET(document));
	
	set_current_document (docman, document);
}

static void
docman_recent_files (GdlRecent *recent,
		     const char *uri,
		     gpointer data)
{
	AnjutaNotebookDocumentManager *docman;

	g_return_if_fail (data != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (data));
	g_return_if_fail (uri != NULL);

	docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (data);

	anjuta_document_manager_open (ANJUTA_DOCUMENT_MANAGER (docman), 
				      uri, 
				      NULL);
}

static void
docman_doc_modified (GtkWidget *widget, gpointer data)
{
	AnjutaNotebookDocumentManager *docman;

	g_return_if_fail (data != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (data));

	docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (data);

	bonobo_ui_component_set_prop (docman->ui_component,
				      "/commands/FileSave",
				      "sensitive", "1", NULL);
	bonobo_ui_component_set_prop (docman->ui_component, 
				      "/commands/FileRevert",
				      "sensitive", "1", NULL);
}

static void
docman_doc_unmodified (GtkWidget *widget, gpointer data)
{
	AnjutaNotebookDocumentManager *docman;

	g_return_if_fail (data != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (data));

	docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (data);

	bonobo_ui_component_set_prop (docman->ui_component,
				      "/commands/FileSave",
				      "sensitive", "0", NULL);
	bonobo_ui_component_set_prop (docman->ui_component, 
				      "/commands/FileRevert",
				      "sensitive", "0", NULL);
}

static void
docman_doc_uri_changed (GtkWidget *widget, 
			const char *filename, 
			gpointer data)
{
        AnjutaNotebookDocumentManager *docman;
        AnjutaDocument *document;

        g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (data));
        g_return_if_fail (ANJUTA_IS_DOCUMENT (widget));

        docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (data);
        document = ANJUTA_DOCUMENT (widget);

        if (filename) {
                set_doc_uri (docman, document, filename);
		
		if (ANJUTA_BONOBO_DOCUMENT (document)->untitled) {
			ANJUTA_BONOBO_DOCUMENT (document)->untitled = FALSE;
			docman->priv->untitled_count--;
		}
        }
}

static void
docman_doc_destroy (GtkWidget *widget, gpointer data)
{
        AnjutaNotebookDocumentManager *docman;
        AnjutaDocument *document;
        gint length, i;
        
        g_return_if_fail(data);
        
        g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));
        g_assert (ANJUTA_IS_DOCUMENT (widget));	

        docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (data);
        document = ANJUTA_DOCUMENT (widget);

        length = anjuta_document_manager_num_documents
		(ANJUTA_DOCUMENT_MANAGER (docman));
        for (i=0; i < length; i++) {
                if ((gpointer)document
                     == g_list_nth_data(docman->documents, i)) {
                        docman->documents =
                                g_list_remove (docman->documents, document);
                        break;
                }
        }

	if (anjuta_document_manager_num_documents (ANJUTA_DOCUMENT_MANAGER (docman)) == 0) {
		set_current_document (docman, NULL);
		/*document = ANJUTA_DOCUMENT (anjuta_bonobo_document_new (docman->ui_container));
		anjuta_bonobo_document_make_temp (ANJUTA_BONOBO_DOCUMENT (document), "text/plain");
		add_doc (docman, document);*/
	}
}

static void
set_current_document (AnjutaNotebookDocumentManager *docman, 
		      AnjutaDocument *doc)
{
	BonoboControlFrame *frame;
	BonoboUIComponent *uic;
	CORBA_Environment ev;

	if (docman->current_document == doc) {
		return;
	}

	CORBA_exception_init (&ev);

	if (docman->current_document) {
		if (anjuta_bonobo_document_supports_modified (ANJUTA_BONOBO_DOCUMENT (docman->current_document))) {
			/* Disconnect from modified & unmodified signals. */
			g_signal_handlers_disconnect_matched (G_OBJECT (docman->current_document),
							      G_SIGNAL_MATCH_FUNC,
							      0,
							      0,
							      NULL,
							      docman_doc_modified,
							      NULL);

			g_signal_handlers_disconnect_matched (G_OBJECT (docman->current_document),
							      G_SIGNAL_MATCH_FUNC,
							      0,
							      0,
							      NULL,
							      docman_doc_unmodified,
							      NULL);
		}

		frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (ANJUTA_BONOBO_DOCUMENT (docman->current_document)->bonobo_widget));

		/* Deactivate control. */
		bonobo_control_frame_control_deactivate (frame);
	}

	docman->current_document = doc;

	uic = docman->ui_component;

	if (doc && ANJUTA_BONOBO_DOCUMENT (doc)->bonobo_widget) {
		/* Activate control. */
		frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (ANJUTA_BONOBO_DOCUMENT (doc)->bonobo_widget));
		bonobo_control_frame_control_activate (frame);

		if (anjuta_bonobo_document_supports_modified (ANJUTA_BONOBO_DOCUMENT (doc))) {
			/* Connect to modified & unmodified signals. */
			g_signal_connect (G_OBJECT (doc),
					  "modified",
					  G_CALLBACK (docman_doc_modified),
					  docman);

			g_signal_connect (G_OBJECT (doc),
					  "unmodified",
					  G_CALLBACK (docman_doc_unmodified),
					  docman);
		}

		if (uic) {
			char *value = (anjuta_bonobo_document_is_changed (ANJUTA_BONOBO_DOCUMENT (doc)) ||
				       !anjuta_bonobo_document_supports_modified (ANJUTA_BONOBO_DOCUMENT (doc))) ? "1" : "0";
			bonobo_ui_component_set_prop (uic, "/commands/FileSave",
						      "sensitive", value, NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileSaveAs",
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileSaveAll",
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileRevert",
						      "sensitive", value, NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileClose",
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileCloseAll",
						      "sensitive", "1", NULL);
		}
	} else if (uic) {
		bonobo_ui_component_set_prop (uic, "/commands/FileSave",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileSaveAs",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileSaveAll",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileRevert",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileClose",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileCloseAll",
					      "sensitive", "0", NULL);
	}

	g_signal_emit_by_name (docman, "current_document_changed", doc);

	CORBA_exception_free (&ev);
}

static NewItemMenuData *
make_menu_data (AnjutaNotebookDocumentManager *dm,
		const char *parent_uri,
		GnomeVFSFileInfo *file)
{
	NewItemMenuData *data;
	data = g_new0 (NewItemMenuData, 1);

	data->uri = g_strdup_printf ("%s/%s", parent_uri, file->name);
	data->label = g_strdup (
		gnome_vfs_mime_get_description 
		(file->mime_type));
	data->tip = g_strdup (_("Create a new file"));
	data->mime_type = g_strdup (file->mime_type);
	
	return data;
}

static void
new_item_menu_data_free (NewItemMenuData *data)
{
	g_free (data->uri);
	g_free (data->label);
	g_free (data->tip);
	
	g_free (data);
}

static void
load_template_dir (AnjutaNotebookDocumentManager *dm, 
		   const char *uri)
{
	GnomeVFSDirectoryHandle *dir;
	GnomeVFSResult result;

	result = gnome_vfs_directory_open (&dir, uri, 
					   GNOME_VFS_FILE_INFO_GET_MIME_TYPE);
	if (result == GNOME_VFS_OK) {
		GnomeVFSFileInfo *file_info = gnome_vfs_file_info_new ();

		
		while (gnome_vfs_directory_read_next (dir, file_info)
		       == GNOME_VFS_OK) {

			if (file_info->type == GNOME_VFS_FILE_TYPE_REGULAR 
			    && file_info->name[0] != '.'
			    && file_info->name[strlen (file_info->name) - 1] != '~') {
				
				NewItemMenuData *old_data;
				NewItemMenuData *data = 
					make_menu_data (dm, uri, file_info);

				old_data = 
					g_hash_table_lookup (dm->priv->templates,
							     file_info->name);

				if (old_data) {
					new_item_menu_data_free (old_data);
				}
				
				g_hash_table_insert (dm->priv->templates,
						     file_info->name,
						     data);
			}
		}

		gnome_vfs_file_info_unref (file_info);
		gnome_vfs_directory_close (dir);
	}
}

static void
new_file_cb (BonoboUIComponent *component, 
	     gpointer user_data, 
	     const char *cname)
{
	AnjutaNotebookDocumentManager *dm = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (user_data);
	NewItemMenuData *data;
	
	data = g_hash_table_lookup (dm->priv->verbs, cname);
	if (data) {
		AnjutaDocument *doc;
		doc = ANJUTA_DOCUMENT (anjuta_bonobo_document_new (dm->ui_container));
		
		if (anjuta_bonobo_document_load_template (ANJUTA_BONOBO_DOCUMENT (doc), data->uri)) {
			add_doc (dm, doc);
		} else {
			g_object_unref (doc);
		}
	}
}

static void
add_to_menu (char *name, NewItemMenuData *data, AnjutaNotebookDocumentManager *dm)
{
	static unsigned long verbnum = 0;
	char *xml;
	GdkPixbuf *pixbuf;
	char *pixdata = NULL;
	char *verb = g_strdup_printf ("New%ld", verbnum++);

	pixbuf = gdl_icons_get_mime_icon (dm->priv->icons, data->mime_type);
	if (pixbuf) {
		pixdata = bonobo_ui_util_pixbuf_to_xml (pixbuf);
		g_object_unref (pixbuf);
	}

	xml = g_strdup_printf ("<menuitem name=\"%s\" verb=\"\" label=\"%s\""
			       " tip=\"%s\" %s pixname=\"%s\"/>", 
			       verb, data->label, data->tip,
			       pixdata ? "pixtype=\"pixbuf\"" : "",
			       pixdata ? pixdata : "");
	g_free (pixdata);
	
	bonobo_ui_component_set (dm->ui_component,
				 "/menu/File/DocumentOps/New/Templates",
				 xml,
				 NULL);

	bonobo_ui_component_add_verb (dm->ui_component,
				      verb, (BonoboUIVerbFn)new_file_cb,
				      dm);

	g_hash_table_insert (dm->priv->verbs, verb, data);
	
	g_free (xml);
}

static void
make_new_menu (AnjutaNotebookDocumentManager *dm)
{
	g_hash_table_foreach (dm->priv->templates, (GHFunc)add_to_menu, dm);
}

static void
load_new_menu (AnjutaNotebookDocumentManager *dm)
{
	char *dir;
	
	/* FIXME: You should be able to unload and reload this menu, but
	 * at the moment you can't */
	g_return_if_fail (dm->priv->templates == NULL);
	
	dm->priv->templates = g_hash_table_new (g_str_hash, g_str_equal);
	dm->priv->verbs = g_hash_table_new (g_str_hash, g_str_equal);

	load_template_dir (dm, "file://" ANJUTA_DATADIR "/anjuta2/templates");

	dir = g_strdup_printf ("%s/.anjuta2/templates", g_get_home_dir ());
	load_template_dir (dm, dir);
	g_free (dir);

	make_new_menu (dm);
}

GtkWidget *
anjuta_notebook_document_manager_get_prefs_page (void)
{
	GladeXML *gui;
	GtkWidget *vbox, *tab, *backup, *autosave, *interval;

	gui = glade_xml_new (GLADEDIR "anjuta-document-manager.glade", 
			     "document-preferences", NULL);

	if (!gui) {
		g_warning ("Could not load anjuta-document-manager.glade, reinstall anjuta2");
		return NULL;
	}

	vbox = glade_xml_get_widget (gui, "document-preferences");
	tab = glade_xml_get_widget (gui, "tab-optionmenu");
	backup = glade_xml_get_widget (gui, "backup-checkbutton");
	autosave = glade_xml_get_widget (gui, "autosave-checkbutton");
	interval = glade_xml_get_widget (gui, "autosave-spinbutton");
	g_object_unref (gui);

	gconf_peditor_new_select_menu_with_enum (NULL, ANJUTA_DOCMAN_TAB_LOCATION,
						 tab, gtk_position_type_get_type (),
						 NULL);

	gtk_widget_show_all (vbox);

	return vbox;
}

static AnjutaDocument *
anjuta_notebook_document_manager_new_document (AnjutaDocumentManager *docman,
					       const char *mime_type,
					       GError *error)
{
	AnjutaDocument *doc;
	
	doc = ANJUTA_DOCUMENT (anjuta_bonobo_document_new (ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman)->ui_container));
	anjuta_bonobo_document_make_temp (ANJUTA_BONOBO_DOCUMENT (doc), 
					  mime_type);
	add_doc (ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman), doc );
	
	return doc;
}

static AnjutaDocument *
anjuta_notebook_document_manager_open (AnjutaDocumentManager *anjuta_docman,
				       const char *string_uri,
				       GError *error)
{
	AnjutaNotebookDocumentManager *docman = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (anjuta_docman);
	AnjutaDocument *document;
	GnomeVFSURI *uri;
	
	uri = gnome_vfs_uri_new (string_uri);
	if (!gnome_vfs_uri_exists (uri)) {
		char *msg;
		msg = g_strdup_printf (_("Could not find %s"), string_uri);
		anjuta_dialog_error (msg);
		g_free (msg);
		gnome_vfs_uri_unref (uri);

		return NULL;
	}
	gnome_vfs_uri_unref (uri);

	/* Only open the file if its not already opened. */
	document = anjuta_document_manager_get_document_for_uri (anjuta_docman,
								 string_uri,
								 TRUE,
								 NULL);
	if (document) {
		anjuta_document_manager_show_document (anjuta_docman, 
						       document);
		return document;
	}

	document = docman->current_document;

	if (document != NULL && (!anjuta_bonobo_document_is_untitled (ANJUTA_BONOBO_DOCUMENT (document)) ||
				 anjuta_bonobo_document_is_changed (ANJUTA_BONOBO_DOCUMENT (document)))) {
		document = ANJUTA_DOCUMENT (anjuta_bonobo_document_new (docman->ui_container));
		if (anjuta_bonobo_document_load_uri (ANJUTA_BONOBO_DOCUMENT (document), string_uri)) {
			add_doc (docman, document);
		} else {
			g_object_unref (G_OBJECT (document));
			document = NULL;
		}
	} else {
		anjuta_bonobo_document_load_uri
			(ANJUTA_BONOBO_DOCUMENT (document), string_uri);
		ANJUTA_BONOBO_DOCUMENT (document)->untitled = FALSE;
		docman->priv->untitled_count--;
		set_current_document(docman, document);
	}

	return document;
}

static void
anjuta_notebook_document_manager_close (AnjutaDocumentManager *docman,
					AnjutaDocument *document,
					GError *error)
{
	if (anjuta_bonobo_document_is_changed (ANJUTA_BONOBO_DOCUMENT (document))) {
		if (file_close_dialog (ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman), document) 
		    == GTK_RESPONSE_CANCEL) {
			return;
		}
	}

	anjuta_notebook_document_manager_remove_doc (ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman), document);

	if (!ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman)->documents) {
		anjuta_document_manager_new_document (docman,
						      "text/plain",
						      NULL);
	}
}

static void
anjuta_notebook_document_manager_close_all (AnjutaDocumentManager *docman,
					    GError *error)
{
	GList *l;
	AnjutaDocument *doc;

	for (l = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman)->documents; l; l = l->next) {
		doc = ANJUTA_DOCUMENT (l->data);

		if (anjuta_bonobo_document_is_changed (ANJUTA_BONOBO_DOCUMENT (doc))) {
			if (file_close_dialog (ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman), doc)
			    == GTK_RESPONSE_CANCEL) {
				return;
			}
		}
		anjuta_notebook_document_manager_close (docman, doc, error);
	}
}

static void
anjuta_notebook_document_manager_save_all (AnjutaDocumentManager *docman,
					   GError *error)
{
	GList *l;
	
	for (l = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman)->documents;
	     l != NULL; 
	     l = l->next) {
		AnjutaDocument *document = l->data;
		
		if (anjuta_bonobo_document_is_changed (ANJUTA_BONOBO_DOCUMENT (document))) {
			anjuta_document_manager_show_document (docman,
							       document);
			anjuta_document_save (document, NULL);
		}
	}
}

static int
anjuta_notebook_document_manager_num_documents (AnjutaDocumentManager *docman)
{
	g_return_val_if_fail (docman != NULL, -1);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), -1);

	return g_list_length (ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman)->documents);
}

static AnjutaDocument *
anjuta_notebook_document_manager_get_nth_document (AnjutaDocumentManager *docman,
						   int index)
{
	return ANJUTA_DOCUMENT (gtk_notebook_get_nth_page (GTK_NOTEBOOK (docman), index));
}

static AnjutaDocument *
anjuta_notebook_document_manager_get_document_for_uri   (AnjutaDocumentManager *docman,
							 const char *uri,
							 gboolean existing_only,
							 GError *error)
{
	GList *l;
	
 	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (uri != NULL, NULL);

	for (l = ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman)->documents; 
	     l != NULL;
	     l = l->next) {
		AnjutaDocument *document;
		const char *doc_uri;

		document = ANJUTA_DOCUMENT (l->data);
		doc_uri = anjuta_document_get_uri (document);

		if (doc_uri && !strcmp(doc_uri, uri)) {
			return document;
		}
	}
	
	if (existing_only) {
		return NULL;
	} else {
		return anjuta_document_manager_open (docman, uri, error);
	}
}

static void
anjuta_notebook_document_manager_show_document (AnjutaDocumentManager *docman, 
						AnjutaDocument *document)
{
	int index;
	
 	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));
	
	index = gtk_notebook_page_num (GTK_NOTEBOOK (docman), 
				       GTK_WIDGET (document));
	if (index > -1) {
		gtk_notebook_set_current_page (GTK_NOTEBOOK (docman), index);
	}
}

static AnjutaDocument *
anjuta_notebook_document_manager_get_current_document (AnjutaDocumentManager *docman)
{
	return ANJUTA_NOTEBOOK_DOCUMENT_MANAGER (docman)->current_document;
}

static void
anjuta_document_manager_iface_init (AnjutaDocumentManagerIface *iface)
{
	iface->new_document = anjuta_notebook_document_manager_new_document;
	iface->open = anjuta_notebook_document_manager_open;
	iface->close = anjuta_notebook_document_manager_close;
	iface->close_all = anjuta_notebook_document_manager_close_all;
	iface->save_all = anjuta_notebook_document_manager_save_all;
	iface->num_documents = anjuta_notebook_document_manager_num_documents;
	iface->get_nth_document = anjuta_notebook_document_manager_get_nth_document;
	iface->get_document_for_uri = anjuta_notebook_document_manager_get_document_for_uri;
	iface->show_document = anjuta_notebook_document_manager_show_document;
	iface->get_current_document = anjuta_notebook_document_manager_get_current_document;
}

ANJUTA_TYPE_BEGIN (AnjutaNotebookDocumentManager, 
		   anjuta_notebook_document_manager,
		   GTK_TYPE_NOTEBOOK)
ANJUTA_INTERFACE (anjuta_document_manager, ANJUTA_TYPE_DOCUMENT_MANAGER)
ANJUTA_TYPE_END
