/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * window.h
 * 
 * Copyright (C) 2001, 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ANJUTA_WINDOW_H__
#define __ANJUTA_WINDOW_H__

#include <bonobo.h>
#include <libanjuta/libanjuta.h>
#include <libxml/tree.h>
#include <gdl/gdl-dock-layout.h>
#include "preferences-dialog.h"

G_BEGIN_DECLS

#define ANJUTA_TYPE_WINDOW        (anjuta_window_get_type ())
#define ANJUTA_WINDOW(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), ANJUTA_TYPE_WINDOW, AnjutaWindow))
#define ANJUTA_WINDOW_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), ANJUTA_TYPE_WINDOW, AnjutaWindowClass))
#define ANJUTA_IS_WINDOW(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), ANJUTA_TYPE_WINDOW))
#define ANJUTA_IS_WINDOW_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), ANJUTA_TYPE_WINDOW))

typedef struct _AnjutaWindow		AnjutaWindow;
typedef struct _AnjutaWindowClass	AnjutaWindowClass;

struct _AnjutaWindow {
	BonoboWindow app;

	BonoboUIComponent *uic;
	BonoboUIContainer *ui_container;

	GtkWidget *dock;
	GdlDockLayout *layout_manager;
	gchar *layout;

	GHashTable *values;
	GHashTable *widgets;
	GHashTable *preference_pages;

	AnjutaSession *session;

	AnjutaPreferencesDialog *prefs_dialog;
};

struct _AnjutaWindowClass {
	BonoboWindowClass parent_class;

	/* Signals. */
	void (* close_window)                 (AnjutaWindow *window);
	void (* new_window)                   (AnjutaWindow *window);
	void (* quit)                         (AnjutaWindow *window);
};

GType      anjuta_window_get_type	      (void);
GtkWidget *anjuta_window_new		      (void);

void       anjuta_window_save_layout	      (AnjutaWindow *window,
					       const gchar  *name);
void       anjuta_window_load_layout	      (AnjutaWindow *window,
					       const gchar  *name);

G_END_DECLS

#endif /* __ANJUTA_WINDOW_H__ */
