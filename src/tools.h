/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * tools.h
 * 
 * Copyright (C) 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __TOOLS_H__
#define __TOOLS_H__

#include "e-splash.h"
#include "window.h"

G_BEGIN_DECLS

void       anjuta_tools_init            (void);
void       anjuta_tools_finalize        (void);

void       anjuta_tool_set_load         (AnjutaWindow *window, 
					 ESplash      *splash,
					 const char   *name);
gboolean   anjuta_tool_unload           (AnjutaWindow *window);

GtkWidget *anjuta_tools_get_preferences (void);

G_END_DECLS

#endif /* __TOOLS_H__ */
