/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * window.c
 * 
 * Copyright (C) 1998-2000 Steffen Kern
 * Copyright (C) 2001, 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <bonobo/bonobo-listener.h>
#include <bonobo/bonobo-ui-util.h>
#include <gconf/gconf-client.h>
#include <gdl/gdl-dock.h>
#include <libgnome/gnome-macros.h>
#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-about.h>
#include <glade/glade-xml.h>
#include "window.h"
#include "tools.h"

#define ANJUTA_WINDOW_STATE_PREFIX         "/apps/anjuta2/state"
#define ANJUTA_WINDOW_STATE_WIDTH_KEY      ANJUTA_WINDOW_STATE_PREFIX "/width" 
#define ANJUTA_WINDOW_STATE_HEIGHT_KEY     ANJUTA_WINDOW_STATE_PREFIX "/height" 
#define ANJUTA_WINDOW_STATE_MAXIMIZED_KEY  ANJUTA_WINDOW_STATE_PREFIX "/maximized"
#define ANJUTA_WINDOW_STATE_LAYOUT         ANJUTA_WINDOW_STATE_PREFIX "/layout"

typedef struct _AnjutaWindowState          AnjutaWindowState;
typedef struct _AnjutaLayoutMenuData       AnjutaLayoutMenuData;
typedef struct _AnjutaLayoutDialogData     AnjutaLayoutDialogData;

struct _AnjutaWindowState {
	gint          width;
	gint          height;
	gboolean      maximized;
};

struct _AnjutaLayoutMenuData {
	AnjutaWindow *window;
	char         *layout_name;
};

struct _AnjutaLayoutDialogData {
	AnjutaWindow *window;
	GtkWidget    *entry;
	GtkListStore *layouts_model;
};

/* Prototypes */
static gboolean anjuta_window_save_layout_to_file (AnjutaWindow *window);

static BonoboWindowClass *parent_class;

/* ------------ Window state functions */

static void
load_state (GtkWindow *window)
{
	GConfClient       *gconf_client;
	AnjutaWindowState *state;
	
	state = g_object_get_data (G_OBJECT (window), "window_state");
	if (!state) {
		state = g_new0 (AnjutaWindowState, 1);
		g_object_set_data (G_OBJECT (window), "window_state", state);
	}

	/* Restore window state. */
	gconf_client = gconf_client_get_default ();
	state->width = gconf_client_get_int (gconf_client,
					     ANJUTA_WINDOW_STATE_WIDTH_KEY,
					     NULL);
	state->height = gconf_client_get_int (gconf_client,
					      ANJUTA_WINDOW_STATE_HEIGHT_KEY,
					      NULL);
	state->maximized = gconf_client_get_bool (gconf_client,
						  ANJUTA_WINDOW_STATE_MAXIMIZED_KEY,
						  NULL);
	gtk_window_set_default_size (GTK_WINDOW (window), state->width, state->height);
	if (state->maximized)
		gtk_window_maximize (GTK_WINDOW (window));

	g_object_unref (gconf_client);
}

static void
save_state (GtkWindow *window)
{
	AnjutaWindowState *state;
	GConfClient       *gconf_client;
	
	state = g_object_get_data (G_OBJECT (window), "window_state");
	if (!state)
		return;
	
	/* Save the window state. */
	gconf_client = gconf_client_get_default ();
	gconf_client_set_int (gconf_client,
			      ANJUTA_WINDOW_STATE_HEIGHT_KEY,
			      state->height,
			      NULL);
	gconf_client_set_int (gconf_client,
			      ANJUTA_WINDOW_STATE_WIDTH_KEY,
			      state->width,
			      NULL);
	gconf_client_set_bool (gconf_client,
			       ANJUTA_WINDOW_STATE_MAXIMIZED_KEY,
			       state->maximized,
			       NULL);

	/* Save current layout name. */
	gconf_client_set_string (gconf_client,
				 ANJUTA_WINDOW_STATE_LAYOUT,
				 ANJUTA_WINDOW (window)->layout,
				 NULL);
	
	g_object_unref (gconf_client);
}

static gboolean
anjuta_window_state_cb (GtkWidget *widget,
			GdkEvent  *event,
			gpointer   user_data)
{
	AnjutaWindowState *state;

	g_return_val_if_fail (widget != NULL, FALSE);

	state = g_object_get_data (G_OBJECT (widget), "window_state");
	if (!state) {
		state = g_new0 (AnjutaWindowState, 1);
		g_object_set_data (G_OBJECT (widget), "window_state", state);
	}

	switch (event->type) {
	    case GDK_WINDOW_STATE:
		    state->maximized = event->window_state.new_window_state &
			    GDK_WINDOW_STATE_MAXIMIZED;
		    break;
	    case GDK_CONFIGURE:
		    if (!state->maximized) {
			    state->width = event->configure.width;
			    state->height = event->configure.height;
		    }
		    break;
	    default:
		    break;
	}

	return FALSE;
}

/* ---------- Callbacks */

static void
file_close_window_cb (BonoboUIComponent *component, AnjutaWindow *window)
{
	g_signal_emit_by_name (G_OBJECT (window), "close_window", NULL);
}

static void
file_exit_cb (BonoboUIComponent *component, AnjutaWindow *window)
{
	g_signal_emit_by_name (G_OBJECT (window), "quit", NULL);
}

static void
edit_preferences_cb (BonoboUIComponent *component, AnjutaWindow *window)
{
	gtk_window_set_transient_for (GTK_WINDOW (window->prefs_dialog),
				      GTK_WINDOW (window));
	gtk_widget_show (GTK_WIDGET (window->prefs_dialog));
}

static void
window_new_cb (BonoboUIComponent *component, AnjutaWindow *window)
{
	g_signal_emit_by_name (G_OBJECT (window), "new_window", NULL);
}

static void
selection_changed_cb (GtkTreeSelection *selection, gpointer data)
{
	GtkWidget *entry;
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *name;

	entry = GTK_WIDGET (data);

	/* Set the entry widget to the selected layout name. */
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		gtk_tree_model_get (model, &iter, 0, &name, -1);
		gtk_entry_set_text (GTK_ENTRY (entry), name);
		g_free (name);
	}
}

static void
save_button_cb (GtkWidget *widget, gpointer data)
{
	AnjutaLayoutDialogData *ldd = data;
	char *name;
	GList *layouts, *l;

	name = g_strdup (gtk_entry_get_text (GTK_ENTRY (ldd->entry)));
	layouts = gdl_dock_layout_get_layouts (ldd->window->layout_manager, FALSE);

	g_strstrip (name);
	if (strlen (name) > 0) {
		gboolean exists;

		exists = (g_list_find_custom (layouts, name, (GCompareFunc)strcmp) != NULL);
		if (!exists && strcmp (name, "__default__")) {
			GtkTreeIter iter;

			/* add the name to the model */
			gtk_list_store_append (ldd->layouts_model, &iter);
			gtk_list_store_set (ldd->layouts_model, &iter,
					    0, name, -1);
		}
		gdl_dock_layout_save_layout (ldd->window->layout_manager, name);
	} else {
		GtkWidget *error_dialog;
		error_dialog = gtk_message_dialog_new (GTK_WINDOW (ldd->window),
						       GTK_DIALOG_MODAL,
						       GTK_MESSAGE_ERROR,
						       GTK_BUTTONS_OK,
						       _("You must provide a name for the layout"));
		gtk_dialog_run (GTK_DIALOG (error_dialog));
		gtk_widget_destroy (error_dialog);
	}

	g_free (name);

	for (l = layouts; l != NULL; l = l->next) {
		g_free (l->data);
	}
	g_list_free (layouts);
}

static void
window_save_layout_cb (BonoboUIComponent *component, AnjutaWindow *window)
{
	GladeXML *gui;
	GtkWidget *vbox, *layout_tree, *name_entry, *button, *dialog;
	GtkListStore *store;
	GList *layouts, *l;
	GtkTreeIter iter;
	GtkCellRenderer *renderer;
	GtkTreeSelection *selection;
	AnjutaLayoutDialogData *ldd;
	GClosure *closure;

	gui = glade_xml_new (GLADEDIR "anjuta2.glade", "layout-vbox", NULL);

	if (!gui) {
		g_warning ("Could not load anjuta2.glade, reinstall anjuta2");
		return;
	}

	vbox = glade_xml_get_widget (gui, "layout-vbox");
	layout_tree = glade_xml_get_widget (gui, "layout-tree");
	name_entry = glade_xml_get_widget (gui, "name-entry");
	button = glade_xml_get_widget (gui, "save-button");
	g_object_unref (gui);

	store = gtk_list_store_new (1, G_TYPE_STRING);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (store),
					      0, GTK_SORT_ASCENDING);
	gtk_tree_view_set_model (GTK_TREE_VIEW (layout_tree),
				 GTK_TREE_MODEL (store));

	layouts = gdl_dock_layout_get_layouts (window->layout_manager, FALSE);
	for (l = layouts; l != NULL; l = l->next) {
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, 0, l->data, -1);
	}
	g_list_free (layouts);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (layout_tree),
						     0, _("Name"), renderer,
						     "text", 0, NULL);

	dialog = gtk_dialog_new_with_buttons (_("Save Layout"),
					      GTK_WINDOW (window),
					      GTK_DIALOG_DESTROY_WITH_PARENT |
					      GTK_DIALOG_NO_SEPARATOR,
					      GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
					      NULL);

	gtk_container_set_border_width (GTK_CONTAINER (dialog), 5);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 2);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), vbox);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (layout_tree));
	g_signal_connect (G_OBJECT (selection), "changed",
			  G_CALLBACK (selection_changed_cb), name_entry);

	ldd = g_new0 (AnjutaLayoutDialogData, 1);
	ldd->window = window;
	ldd->entry = name_entry;
	ldd->layouts_model = store;

	closure = g_cclosure_new (G_CALLBACK (save_button_cb), ldd,
				  (GClosureNotify)g_free);
	g_signal_connect_closure (G_OBJECT (button), "clicked", closure, TRUE);

	g_signal_connect_swapped (G_OBJECT (dialog), "response",
				  G_CALLBACK (gtk_widget_destroy), dialog);

	gtk_widget_show_all (dialog);
}

static void
help_about_cb (BonoboUIComponent *component, AnjutaWindow *window)
{
	static const gchar *authors[] = {
		"JP Rosevear",
		"Dave Camp",
		"Dirk Vangestel",
		"Naba Kumar",
		"Jeroen Zwartepoorte",
		"Gustavo Giráldez",
		NULL};
	static GtkWidget *about = NULL;

	/* Convert names in the about box to utf8 */
	static gboolean converted = TRUE;
	if (!converted) {
		int i;
 		for (i = 0; i < G_N_ELEMENTS (authors) - 1; i++) {
			authors[i] = g_locale_to_utf8 (authors[i], -1,
						       NULL, NULL, NULL);
		}
		converted = TRUE;
	}

	if (!about) {
		about = gnome_about_new
			(_("Anjuta"), 
			 VERSION,
			 _("Copyright Various Authors (C) 1998-2003"),
			 _("A Gnome based Development Environment"), 
			 authors, NULL, NULL, NULL);
		gtk_window_set_transient_for (GTK_WINDOW (about),
					      GTK_WINDOW (window));
		g_object_add_weak_pointer (G_OBJECT (about), (gpointer *)&about);
		gtk_widget_show (about);
	} else {
		gdk_window_raise (GTK_WIDGET (about)->window);
	}
}

/* Menu verbs */
static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("FileCloseWindow", file_close_window_cb),
	BONOBO_UI_UNSAFE_VERB ("FileExit", file_exit_cb),
	BONOBO_UI_UNSAFE_VERB ("EditPreferences", edit_preferences_cb),
	BONOBO_UI_UNSAFE_VERB ("WindowNew", window_new_cb),
	BONOBO_UI_UNSAFE_VERB ("WindowSaveLayout", window_save_layout_cb),
	BONOBO_UI_UNSAFE_VERB ("HelpAbout", help_about_cb),
	BONOBO_UI_VERB_END
};

static void
layout_cb (BonoboUIComponent           *component,
	   const char                  *path,
	   Bonobo_UIComponent_EventType type,
	   const char                  *state,
	   gpointer                     data)
{
	AnjutaLayoutMenuData *md = data;

	if (!strcmp (state, "1"))
		anjuta_window_load_layout (md->window, md->layout_name);
}

static void
layout_menu_data_destroy_cb (gpointer data, GClosure *closure)
{
	AnjutaLayoutMenuData *md = data;

	g_free (md->layout_name);
	g_free (md);
}

static void
update_layout_menu (AnjutaWindow *window)
{
	GList *layouts, *l;
	BonoboUIComponent *uic;
	int i;
	char *name, *verb_name, *cmd, *tip, *item_path, *xml;
	const char *menu_path = "/menu/Window/Layouts/Layouts";
	GClosure *closure;
	AnjutaLayoutMenuData *md;

	layouts = gdl_dock_layout_get_layouts (window->layout_manager, FALSE);

	uic = window->uic;

	bonobo_ui_component_freeze (uic, NULL);

	for (l = layouts, i = 1; l != NULL; l = l->next, i++) {
		name = l->data;

		/* Create verb & command for menuitem. */
		verb_name = g_strdup_printf ("%s%d", "layout-", i);
		cmd = g_strdup_printf ("<cmd name = \"%s\"/> ", verb_name);
		bonobo_ui_component_set_translate (uic, "/commands/", cmd, NULL);

		md = g_new0 (AnjutaLayoutMenuData, 1);
		md->window = window;
		md->layout_name = name;

		closure = g_cclosure_new (G_CALLBACK (layout_cb), md,
					  layout_menu_data_destroy_cb);

		bonobo_ui_component_add_listener_full (uic, verb_name, closure);

		tip = g_strdup_printf (_("Load '%s' layout"), name);

		item_path = g_strconcat (menu_path, "/", verb_name, NULL);

		if (bonobo_ui_component_path_exists (uic, item_path, NULL)) {
			bonobo_ui_component_set_prop (uic, item_path, "label", name, NULL);
			bonobo_ui_component_set_prop (uic, item_path, "tip", tip, NULL);
		} else {
			xml = g_strdup_printf ("<menuitem name=\"%s\" verb=\"%s\""
					       " _label=\"%s\"  _tip=\"%s \" hidden=\"0\""
					       " type=\"radio\" group=\"layout\"/>", 
						verb_name, verb_name, name, tip);

			bonobo_ui_component_set_translate (uic, menu_path,
							   xml, NULL);
			g_free (xml);
		}

		g_free (item_path);
		g_free (tip);
		g_free (cmd);
		g_free (verb_name);
	}

	g_list_free (layouts);

	bonobo_ui_component_thaw (uic, NULL);
}

static void 
layout_dirty_notify (GObject    *object,
		     GParamSpec *pspec,
		     gpointer    user_data)
{
	if (!strcmp (pspec->name, "dirty")) {
		gboolean dirty;
		g_object_get (object, "dirty", &dirty, NULL);
		if (dirty) {
			update_layout_menu (ANJUTA_WINDOW (user_data));
		
			/* user_data is the AnjutaWindow */
			g_idle_add (
				(GSourceFunc) anjuta_window_save_layout_to_file,
				user_data);
			
		}
	}
}

static void
anjuta_window_drag_recv_cb (GtkWidget        *widget,
			    GdkDragContext   *context,
			    gint              x,
			    gint              y,
			    GtkSelectionData *seldata,
			    guint             info,
			    guint             time,
			    gpointer          data)
{
#if GNOME2_CONVERSION_COMPLETE
	GList* files;
	GList* fnp;
	gint count;
	gchar* fname;
	CORBA_Environment ev;
	GNOME_Development_DocumentManager docman;
	GNOME_Development_Document document;

	g_assert(ANJUTA_IS_WINDOW(widget));

	/* get filenames */
	files = gnome_uri_list_extract_filenames((gchar*)seldata->data);
	count = g_list_length(files);

	CORBA_exception_init (&ev);
	docman = GNOME_Development_Shell_getObject 
		(bonobo_object_corba_objref 
		 (gtk_object_get_data (GTK_OBJECT (widget), "AnjutaShell")),
		 "DocumentManager", &ev);
	
	/* open files */
	if(count > 0 && docman != CORBA_OBJECT_NIL)
	{
		gchar *curdir = g_get_current_dir ();
		
		fnp = g_list_first(files);
		while(fnp)
		{
			fname = (gchar*)fnp->data;
			
			/* getDocument needs a full path for the file */
			if (g_path_is_absolute (fname))
				fname = g_strdup (fname);
			else
				fname = g_strdup_printf ("%s/%s", curdir, fname);

			document = 
				GNOME_Development_DocumentManager_getDocument
				(docman, fname, &ev);
			bonobo_object_release_unref (document, &ev);
			g_free (fname);

			fnp = g_list_next(fnp);
		}
		g_free (curdir);
	}

	gnome_uri_list_free_strings(files);
#endif
}

static void
anjuta_window_instance_init (AnjutaWindow *window)
{
	BonoboUIContainer *ui_container;
	GtkTargetEntry dragtypes[] = {{"text/uri-list", 0, 0}};

	ui_container = bonobo_window_get_ui_container (BONOBO_WINDOW (window));
	
	gtk_window_set_resizable (GTK_WINDOW (window), TRUE);

	/* configure dock */
	window->dock = gdl_dock_new ();
#if 0
	g_object_set (G_OBJECT (window->dock), "default_title", _("Anjuta dock"), NULL);
#endif
	
	bonobo_window_set_contents (BONOBO_WINDOW (window),
				    window->dock);
	gtk_widget_show (window->dock);

	/* create placeholders for default widget positions (since an
           initial host is provided they are automatically bound) */
	gdl_dock_placeholder_new ("ph_top", GDL_DOCK_OBJECT (window->dock),
				  GDL_DOCK_TOP, FALSE);
	gdl_dock_placeholder_new ("ph_bottom", GDL_DOCK_OBJECT (window->dock),
				  GDL_DOCK_BOTTOM, FALSE);
	gdl_dock_placeholder_new ("ph_left", GDL_DOCK_OBJECT (window->dock),
				  GDL_DOCK_LEFT, FALSE);
	gdl_dock_placeholder_new ("ph_right", GDL_DOCK_OBJECT (window->dock),
				  GDL_DOCK_RIGHT, FALSE);
	
	/* window state tracking */
	g_signal_connect (window, "window-state-event",
			  G_CALLBACK (anjuta_window_state_cb), NULL);
	g_signal_connect (window, "configure-event",
			  G_CALLBACK (anjuta_window_state_cb), NULL);
	load_state (GTK_WINDOW (window));

	gtk_widget_realize (GTK_WIDGET(window));

	/* drag'n'drop */
	gtk_drag_dest_set (GTK_WIDGET(window),
			   GTK_DEST_DEFAULT_ALL, dragtypes,
			   sizeof(dragtypes) / sizeof(dragtypes[0]),
			   GDK_ACTION_COPY);
	g_signal_connect (G_OBJECT (window), "drag_data_received",
			  G_CALLBACK (anjuta_window_drag_recv_cb), window);

	window->uic = bonobo_ui_component_new_default ();

	bonobo_ui_component_set_container (window->uic, 
					   BONOBO_OBJREF (ui_container), 
					   NULL);

	bonobo_ui_component_add_verb_list_with_data (window->uic, 
						     verbs, window);

	bonobo_ui_util_set_ui (window->uic, DATADIR, 
			       "anjuta.xml", "anjuta", NULL);
	window->ui_container = ui_container;
	
	bonobo_ui_engine_config_set_path (
		bonobo_window_get_ui_engine (BONOBO_WINDOW (window)),
		"/apps/anjuta2/UIConf/kvps");
	
	window->values           = g_hash_table_new_full (g_str_hash,
							  g_str_equal,
							  g_free, NULL);
	window->widgets          = g_hash_table_new_full (g_str_hash,
							  g_str_equal,
							  g_free, NULL);
	window->preference_pages = g_hash_table_new_full (g_str_hash,
							  g_str_equal,
							  g_free, NULL);

	window->layout = NULL;

	/* Create preferences dialog and add to shell. */
	window->prefs_dialog = g_object_new (ANJUTA_TYPE_PREFERENCES_DIALOG, NULL);
	anjuta_shell_add (ANJUTA_SHELL (window),
			  "Shell::PreferencesDialog",
			  ANJUTA_TYPE_PREFERENCES_DIALOG,
			  window->prefs_dialog,
			  NULL);

	/* Add preference page for the plugins. */
	anjuta_shell_add_preferences (ANJUTA_SHELL (window),
				      anjuta_tools_get_preferences (),
				      "Tools::Preferences", _("General"),
				      _("Plugins"), NULL);

	/* Create default session and add to shell. */
	window->session = anjuta_session_new ("default");
	anjuta_shell_add (ANJUTA_SHELL (window),
			  "Shell::CurrentSession",
			  ANJUTA_TYPE_SESSION,
			  window->session,
			  NULL);

	gtk_widget_queue_draw (GTK_WIDGET(window));
	gtk_widget_queue_resize (GTK_WIDGET(window));
}

GtkWidget *
anjuta_window_new (void)
{
	AnjutaWindow *window;

	window = ANJUTA_WINDOW (g_object_new (ANJUTA_TYPE_WINDOW, 
					      "win_name", "Anjuta",
					      "title", "Anjuta",
					      NULL));

	return GTK_WIDGET (window);
}

static void
anjuta_window_add_value (AnjutaShell *shell,
			 const char *name,
			 const GValue *value,
			 GError **error)
{
	GValue *copy;
	AnjutaWindow *window = ANJUTA_WINDOW (shell);

	if (g_hash_table_lookup (window->values, name)) {
		g_warning ("Value '%s' has already been added to the shell; "
			   "remove existing value first", name);
		return;
	}
	
	copy = g_new0 (GValue, 1);
	g_value_init (copy, value->g_type);
	g_value_copy (value, copy);

	g_hash_table_insert (window->values, g_strdup (name), copy);
	g_signal_emit_by_name (shell, "value_added", name, copy);
}

static void
anjuta_window_get_value (AnjutaShell *shell,
			 const char *name,
			 GValue *value,
			 GError **error)
{
	GValue *val;
	AnjutaWindow *window = ANJUTA_WINDOW (shell);
	
	val = g_hash_table_lookup (window->values, name);
	
	if (val) {
		if (!value->g_type) {
			g_value_init (value, val->g_type);
		}
		g_value_copy (val, value);
	} else {
		if (error) {
			*error = g_error_new (ANJUTA_SHELL_ERROR,
					      ANJUTA_SHELL_ERROR_DOESNT_EXIST,
					      _("Value doesn't exist"));
		}
	}
}

static void 
anjuta_window_add_widget (AnjutaShell *shell, 
			  GtkWidget *w, 
			  const char *name,
			  const char *title, 
			  GError **error)
{
	AnjutaWindow *window = ANJUTA_WINDOW (shell);
	GtkWidget *item;

	g_return_if_fail (w != NULL);

	if (g_hash_table_lookup (window->widgets, name)) {
		g_warning ("Widget '%s' has already been added to the shell; "
			   "remove existing widget first", name);
		return;
	}

	anjuta_shell_add (shell, name, G_TYPE_FROM_INSTANCE (w), w, NULL);

	g_hash_table_insert (window->widgets, g_strdup (name), w);

	item = gdl_dock_item_new (name, title, GDL_DOCK_ITEM_BEH_NORMAL);
	gtk_container_add (GTK_CONTAINER (item), w);
	g_object_set_data (G_OBJECT (w), "dockitem", item);

	gdl_dock_add_item (GDL_DOCK (window->dock), 
			   GDL_DOCK_ITEM (item), GDL_DOCK_TOP);
	
	gtk_widget_show_all (item);	
}

static void 
anjuta_window_add_preferences (AnjutaShell *shell, 
			       GtkWidget *w, 
			       const char *name,
			       const char *category,
			       const char *title, 
			       GError **error)
{
	AnjutaWindow *window = ANJUTA_WINDOW (shell);

	g_return_if_fail (w != NULL);

	if (g_hash_table_lookup (window->preference_pages, name)) {
		g_warning ("Preference page '%s' has already been added to the shell; "
			   "remove existing page first", name);
		return;
	}

	anjuta_shell_add (shell, name, G_TYPE_FROM_INSTANCE (w), w, NULL);
	anjuta_preferences_dialog_add_page (window->prefs_dialog,
					    category, title, w);

	g_object_set_data (G_OBJECT (w), "category", g_strdup (category));
	g_object_set_data (G_OBJECT (w), "title", g_strdup (title));

	g_hash_table_insert (window->preference_pages, g_strdup (name), w);
}

static void
anjuta_window_remove_value (AnjutaShell *shell, 
			    const char *name, 
			    GError **error)
{
	AnjutaWindow *window = ANJUTA_WINDOW (shell);
	GValue *value;
	GtkWidget *w;
	char *key, *category, *title;

	if (g_hash_table_lookup_extended (window->widgets, name, 
					  (gpointer*)&key, (gpointer*)&w)) {
		GtkWidget *item;
		g_hash_table_remove (window->widgets, name);
		item = g_object_get_data (G_OBJECT (w), "dockitem");
		gdl_dock_item_hide_item (GDL_DOCK_ITEM (item));
		gdl_dock_object_unbind (GDL_DOCK_OBJECT (item));
	}

	if (g_hash_table_lookup_extended (window->preference_pages, name, 
					  (gpointer *)&key, (gpointer *)&w)) {
		g_hash_table_remove (window->preference_pages, name);
		category = g_object_get_data (G_OBJECT (w), "category");
		title = g_object_get_data (G_OBJECT (w), "title");
		anjuta_preferences_dialog_remove_page (window->prefs_dialog,
						       category, title);
		g_free (category);
		g_free (title);
	}

	if (g_hash_table_lookup_extended (window->values, name, 
					  (gpointer *)&key, (gpointer *)&value)) {
		g_hash_table_remove (window->values, name);
		g_signal_emit_by_name (window, "value_removed", name);
		g_value_unset (value);
		g_free (value);
	}
}
#if 0
static void
anjuta_window_bring_to_front (AnjutaWindow *window, GtkWidget *w)
{
	GtkWidget *item;
	item = g_object_get_data (G_OBJECT (w), "dockitem");
	
	gdl_dock_object_present (GDL_DOCK_OBJECT (item), NULL);
}
#endif

static void
ensure_layout_manager (AnjutaWindow *window)
{
	gchar *filename;

	if (!window->layout_manager) {
		/* layout manager */
		window->layout_manager = gdl_dock_layout_new (GDL_DOCK (window->dock));
		
		/* load xml layout definitions */
		filename = gnome_util_prepend_user_home (".anjuta2/layout.xml");
		if (!gdl_dock_layout_load_from_file (window->layout_manager, filename)) {
			g_free (filename);
			filename = g_build_filename (DATADIR "/anjuta2",
						     "layout.xml", NULL);
			gdl_dock_layout_load_from_file (window->layout_manager, filename);
		}
		g_free (filename);
		
		g_signal_connect (window->layout_manager, "notify::dirty",
				  (GCallback) layout_dirty_notify, window);
	}
}

static gboolean
anjuta_window_save_layout_to_file (AnjutaWindow *window)
{
	char *dir;
	char *filename;

	dir = gnome_util_prepend_user_home (".anjuta2");
	if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		mkdir (dir, 0755);
		if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
			anjuta_dialog_error ("Could not create .anjuta2 directory.");
			return FALSE;
		}
	}
	g_free (dir);

	ensure_layout_manager (window);
	
	filename = gnome_util_prepend_user_home (".anjuta2/layout.xml");
	if (!gdl_dock_layout_save_to_file (window->layout_manager, filename))
		anjuta_dialog_error ("Could not save layout.");

	return FALSE;
}

void
anjuta_window_save_layout (AnjutaWindow *window, const gchar *name)
{
	g_return_if_fail (ANJUTA_IS_WINDOW (window));
	g_return_if_fail (name != NULL);

	ensure_layout_manager (window);

	gdl_dock_layout_save_layout (window->layout_manager, name);

	anjuta_window_save_layout_to_file (window);

	update_layout_menu (window);
}

void
anjuta_window_load_layout (AnjutaWindow *window, const gchar *name)
{
	g_return_if_fail (ANJUTA_IS_WINDOW (window));
	g_return_if_fail (name != NULL);

	ensure_layout_manager (window);

	gdl_dock_layout_load_layout (window->layout_manager, name);

	update_layout_menu (window);

	if (window->layout)
		g_free (window->layout);
	window->layout = g_strdup (name);
}

static void
anjuta_window_dispose (GObject *object)
{
	AnjutaWindow *window = ANJUTA_WINDOW (object);
	AnjutaWindowState *state;

	state = g_object_get_data (G_OBJECT (object), "window_state");
	if (state) {
		save_state (GTK_WINDOW (object));
		g_object_set_data (G_OBJECT (object), "window_state", NULL);
		g_free (state);
	}

	if (window->session) {
		anjuta_session_save (window->session);
		anjuta_shell_remove_value (ANJUTA_SHELL (window),
					   "Shell::CurrentSession",
					   NULL);
		g_object_unref (window->session);
		window->session = NULL;
	}

	if (window->prefs_dialog) {
		anjuta_shell_remove_value (ANJUTA_SHELL (window),
					   "Tools::Preferences",
					   NULL);
		anjuta_shell_remove_value (ANJUTA_SHELL (window),
					   "Shell::PreferencesDialog",
					   NULL);
		gtk_widget_destroy (GTK_WIDGET (window->prefs_dialog));
		window->prefs_dialog = NULL;
	}

	if (window->layout_manager) {
		g_object_unref (window->layout_manager);
		window->layout_manager = NULL;
	}

	if (window->layout) {
		g_free (window->layout);
		window->layout = NULL;
	}

	if (window->values) {
		g_hash_table_destroy (window->values);
		g_hash_table_destroy (window->widgets);
		g_hash_table_destroy (window->preference_pages);
		window->values = NULL;
		window->widgets = NULL;
		window->preference_pages = NULL;
	}
	
	if (window->uic) {
		bonobo_ui_component_unset_container (window->uic, NULL);
		window->uic = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
anjuta_window_class_init (AnjutaWindowClass *klass)
{
	GObjectClass *object_class;
	
	parent_class = g_type_class_peek_parent (klass);
	
	object_class = (GObjectClass*) klass;

	object_class->dispose = anjuta_window_dispose;

	g_signal_new ("new_window",
		      G_TYPE_FROM_CLASS (klass),
		      G_SIGNAL_RUN_LAST,
		      G_STRUCT_OFFSET (AnjutaWindowClass, new_window),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__VOID,
		      G_TYPE_NONE, 0);
	g_signal_new ("close_window",
		      G_TYPE_FROM_CLASS (klass),
		      G_SIGNAL_RUN_LAST,
		      G_STRUCT_OFFSET (AnjutaWindowClass, close_window),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__VOID,
		      G_TYPE_NONE, 0);
	g_signal_new ("quit",
		      G_TYPE_FROM_CLASS (klass),
		      G_SIGNAL_RUN_LAST,
		      G_STRUCT_OFFSET (AnjutaWindowClass, quit),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__VOID,
		      G_TYPE_NONE, 0);
}

static void
anjuta_shell_iface_init (AnjutaShellIface *iface)
{
	iface->add_widget = anjuta_window_add_widget;
	iface->add_preferences = anjuta_window_add_preferences;
	iface->add_value = anjuta_window_add_value;
	iface->get_value = anjuta_window_get_value;
	iface->remove_value = anjuta_window_remove_value;
}

ANJUTA_TYPE_BEGIN(AnjutaWindow, anjuta_window, BONOBO_TYPE_WINDOW);
ANJUTA_INTERFACE(anjuta_shell, ANJUTA_TYPE_SHELL);
ANJUTA_TYPE_END;
