/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * anjuta.c
 * 
 * Copyright (C) 1998-2000 Steffen Kern
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo-activation/bonobo-activation.h>
#include <libgnome/gnome-program.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libgnomeui/gnome-window-icon.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include "tools.h"
#include "e-splash.h"
#include "anjuta-windows.h"

int 
main (int argc, char *argv[]) 
{
	GtkWidget *splash;
	GnomeProgram *program;
	poptContext ctx;
	const char **startup_files;
	AnjutaWindow *window;

	/* Internationalization. */
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (PACKAGE, "UTF-8");
	textdomain (PACKAGE);

	/* Initialize the various libraries. */
	program = gnome_program_init (PACKAGE, VERSION, 
				      LIBGNOMEUI_MODULE,
				      argc, argv, 
				      GNOME_PROGRAM_STANDARD_PROPERTIES,
				      NULL);
	gnome_vfs_init ();
	bonobo_activate ();

	/* Set window icon. */
	gnome_window_icon_set_default_from_file (DATADIR "/pixmaps/anjuta2.png");

	/* Display splashscreen. */
	splash = e_splash_new ();
	gtk_widget_show (splash);
	g_object_ref (G_OBJECT (splash));
	while (gtk_events_pending ())
		gtk_main_iteration ();

	/* Initialize the anjuta tool system. */
	anjuta_tools_init ();

	/* Create main window. */
	window = anjuta_new_window (E_SPLASH (splash));

	gtk_widget_unref (splash);
	gtk_widget_destroy (splash);

	/* Add program startup parameters to the shell. */ 
	g_object_get (G_OBJECT (program), "popt-context", &ctx, NULL);
	startup_files = poptGetArgs (ctx);
	anjuta_shell_add (ANJUTA_SHELL (window),
			  "Shell::ProgramArguments",
			  G_TYPE_POINTER,
			  startup_files, NULL);

	/* Signal the plugins to restore any previous sessions. */
	g_signal_emit_by_name (window, "session_load", window);

	/* Program main loop. */
	gtk_main ();

	/* Cleanup. */
	anjuta_tools_finalize ();

	return EXIT_SUCCESS;
}
