/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * anjuta-windows.h
 * 
 * Copyright (C) 1998-2000 JP Rosevear
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ANJUTA_WINDOWS_H__
#define __ANJUTA_WINDOWS_H__

#include "window.h"
#include "e-splash.h"

G_BEGIN_DECLS

AnjutaWindow *anjuta_new_window      (ESplash *splash);

GList        *anjuta_get_all_windows (void);

G_END_DECLS

#endif /* __ANJUTA_WINDOWS_H__ */
