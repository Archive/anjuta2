/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * anjuta-windows.c
 * 
 * Copyright (C) 1998-2000 Steffen Kern
 * Copyright (C) 2001, 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glade/glade-xml.h>
#include <gconf/gconf-client.h>
#include "anjuta-windows.h"
#include "tools.h"

#define ANJUTA_WINDOW_STATE_PREFIX         "/apps/anjuta2/state"
#define ANJUTA_WINDOW_STATE_LAYOUT         ANJUTA_WINDOW_STATE_PREFIX "/layout"

static GList *window_list = NULL;

static GtkWidget *
get_dock_preferences (AnjutaWindow *window)
{
	GladeXML *gui;
	GtkWidget *vbox, *hbox, *image, *dock_items, *layout_hbox, *layouts;

	gui = glade_xml_new (GLADEDIR "anjuta2.glade", "dock-preferences", NULL);

	if (!gui) {
		g_warning ("Could not load anjuta2.glade, reinstall anjuta2");
		return NULL;
	}

	vbox = glade_xml_get_widget (gui, "dock-preferences");
	hbox = glade_xml_get_widget (gui, "dockitems-hbox");
	image = glade_xml_get_widget (gui, "dock-image");
	layout_hbox = glade_xml_get_widget (gui, "layouts-hbox");
	g_object_unref (gui);

	gtk_image_set_from_file (GTK_IMAGE (image), ANJUTA_IMAGES "/dock.png");

	dock_items = gdl_dock_layout_get_items_ui (window->layout_manager);
	gtk_box_pack_end (GTK_BOX (hbox), dock_items, TRUE, TRUE, 0);

	layouts = gdl_dock_layout_get_layouts_ui (window->layout_manager);
	gtk_box_pack_end (GTK_BOX (layout_hbox), layouts, TRUE, TRUE, 0);

	gtk_widget_show_all (vbox);

	return vbox;
}

static void
anjuta_window_close (AnjutaWindow *window, gpointer data)
{
	/* Check if there are plugins which don't want to quit (unsaved data). */
	if (!anjuta_tool_unload (window))
		return;

	anjuta_shell_remove_value (ANJUTA_SHELL (window),
				   "Window::Preferences", NULL);

	/* Remove the window from the window list & destroy it. */
	window_list = g_list_remove (window_list, window);
	gtk_widget_destroy (GTK_WIDGET (window));

	/* Quit if no more windows left. */
	if (g_list_length (window_list) == 0) {
		g_list_free (window_list);
		gtk_main_quit ();
	}
}

static void
anjuta_window_quit (AnjutaWindow *window, gpointer data)
{
	GList *l;

	/* Check if there are plugins which don't want to quit (unsaved data). */
	if (!anjuta_tool_unload (window))
		return;

	anjuta_shell_remove_value (ANJUTA_SHELL (window),
				   "Window::Preferences", NULL);

	/* Remove the window from the window list & destroy it. */
	window_list = g_list_remove (window_list, window);
	gtk_widget_destroy (GTK_WIDGET (window));

	/* Do the same for the other windows. */
	for (l = window_list; l != NULL; l = l->next) {
		window = ANJUTA_WINDOW (l->data);
		
		if (!anjuta_tool_unload (window))
			return;

		gtk_widget_destroy (GTK_WIDGET (window));
	}

	g_list_free (window_list);
	gtk_main_quit ();
}

static void
anjuta_window_new_window (AnjutaWindow *window, gpointer data)
{
	AnjutaWindow *new_wnd;
	GtkWidget *dock_prefs;

	new_wnd = ANJUTA_WINDOW (anjuta_window_new ());
	window_list = g_list_append (window_list, new_wnd);

	anjuta_tool_set_load (new_wnd, NULL, "default");

	if (window->layout)
		anjuta_window_load_layout (new_wnd, window->layout);
	else
		anjuta_window_load_layout (new_wnd, "__default__");

	/* Add dock preferences. */
	dock_prefs = get_dock_preferences (new_wnd);
	anjuta_shell_add_preferences (ANJUTA_SHELL (new_wnd), dock_prefs,
				      "Window::Preferences", _("General"),
				      _("Dock"), NULL);

	g_signal_connect (G_OBJECT (new_wnd), "new_window",
			  G_CALLBACK (anjuta_window_new_window), NULL);
	g_signal_connect (G_OBJECT (new_wnd), "close_window",
			  G_CALLBACK (anjuta_window_close), NULL);
	g_signal_connect (G_OBJECT (new_wnd), "delete_event",
			  G_CALLBACK (anjuta_window_close), NULL);
	g_signal_connect (G_OBJECT (new_wnd), "quit",
			  G_CALLBACK (anjuta_window_quit), NULL);

	gtk_widget_show (GTK_WIDGET (new_wnd));
}

AnjutaWindow *
anjuta_new_window (ESplash *splash)
{
	AnjutaWindow *window;
	GtkWidget *dock_prefs;
	GConfClient *client;
	gchar *layout;

	window = ANJUTA_WINDOW (anjuta_window_new ());
	window_list = g_list_append (window_list, window);

	anjuta_tool_set_load (window, splash, "default");

	/* Restore last layout. This needs to be done here, after the
	 * AnjutaWindow has been fully initialized. Doing this from the
	 * instance_init method in AnjutaWindow won't work (don't recall why
	 * exactly). */
	client = gconf_client_get_default ();
	layout = gconf_client_get_string (client,
					  ANJUTA_WINDOW_STATE_LAYOUT,
					  NULL);
	g_object_unref (G_OBJECT (client));
	if (!layout)
		layout = g_strdup ("__default__");
	anjuta_window_load_layout (ANJUTA_WINDOW (window), layout);
	g_free (layout);

	/* Add dock preferences. */
	dock_prefs = get_dock_preferences (window);
	anjuta_shell_add_preferences (ANJUTA_SHELL (window), dock_prefs,
				      "Window::Preferences", _("General"),
				      _("Dock"), NULL);

	g_signal_connect (G_OBJECT (window), "new_window",
			  G_CALLBACK (anjuta_window_new_window), NULL);
	g_signal_connect (G_OBJECT (window), "close_window",
			  G_CALLBACK (anjuta_window_close), NULL);
	g_signal_connect (G_OBJECT (window), "delete_event",
			  G_CALLBACK (anjuta_window_close), NULL);
	g_signal_connect (G_OBJECT (window), "quit",
			  G_CALLBACK (anjuta_window_quit), NULL);

	gtk_widget_show (GTK_WIDGET (window));
	
	return window;
}

GList *
anjuta_get_all_windows (void)
{
	return g_list_copy (window_list);
}
