#ifndef ANJUTA_DOCUMENT_MANAGER_H
#define ANJUTA_DOCUMENT_MANAGER_H

#include <glib-object.h>

#include "anjuta-document.h"

G_BEGIN_DECLS

#define ANJUTA_TYPE_DOCUMENT_MANAGER            (anjuta_document_manager_get_type ())
#define ANJUTA_DOCUMENT_MANAGER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ANJUTA_TYPE_DOCUMENT_MANAGER, AnjutaDocumentManager))
#define ANJUTA_IS_DOCUMENT_MANAGER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ANJUTA_TYPE_DOCUMENT_MANAGER))
#define ANJUTA_DOCUMENT_MANAGER_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), ANJUTA_TYPE_DOCUMENT_MANAGER, AnjutaDocumentManagerIface))

typedef struct _AnjutaDocumentManager      AnjutaDocumentManager;
typedef struct _AnjutaDocumentManagerIface AnjutaDocumentManagerIface;

struct _AnjutaDocumentManagerIface {
	GTypeInterface g_iface;
	
	/* Signals */
	void (*current_document_changed) (AnjutaDocumentManager *docman, 
					  AnjutaDocument *document);

	/* Virtual Table */
	AnjutaDocument *(*new_document)         (AnjutaDocumentManager *docman,
						 const char            *mime_type,
						 GError                *error);
	AnjutaDocument *(*open)                 (AnjutaDocumentManager *docman,
						 const char            *uri,
						 GError                *error);
	void            (*close)                (AnjutaDocumentManager *docman,
						 AnjutaDocument        *document,
						 GError                *error);
	void            (*close_all)            (AnjutaDocumentManager *docman,
						 GError                *error);
	void            (*save_all)             (AnjutaDocumentManager *docman,
						 GError                *error);
	int             (*num_documents)        (AnjutaDocumentManager *docman);
	AnjutaDocument *(*get_nth_document)     (AnjutaDocumentManager *docman,
						 int                    n);
	AnjutaDocument *(*get_document_for_uri) (AnjutaDocumentManager *docman,
						 const char            *uri,
						 gboolean               existing_only,
						 GError                *error);
	void            (*show_document)        (AnjutaDocumentManager *docman,
						 AnjutaDocument        *document);
	AnjutaDocument *(*get_current_document) (AnjutaDocumentManager *docman);
};

GType           anjuta_document_manager_get_type             (void);
AnjutaDocument *anjuta_document_manager_new_document         (AnjutaDocumentManager *docman,
							      const char            *mime_type,
							      GError                *error);
AnjutaDocument *anjuta_document_manager_open                 (AnjutaDocumentManager *docman,
							      const char            *uri,
							      GError                *error);
void            anjuta_document_manager_close                (AnjutaDocumentManager *docman,
							      AnjutaDocument        *document,
							      GError                *error);
void            anjuta_document_manager_close_all            (AnjutaDocumentManager *docman,
							      GError                *error);
void            anjuta_document_manager_save_all             (AnjutaDocumentManager *docman,
							      GError                *error);
int             anjuta_document_manager_num_documents        (AnjutaDocumentManager *docman);
AnjutaDocument *anjuta_document_manager_get_nth_document     (AnjutaDocumentManager *docman,
							      int                    n);
AnjutaDocument *anjuta_document_manager_get_document_for_uri (AnjutaDocumentManager *docman,
							      const char            *uri,
							      gboolean               existing_only,
							      GError                *error);
void            anjuta_document_manager_show_document        (AnjutaDocumentManager *docman,
							      AnjutaDocument        *document);
AnjutaDocument *anjuta_document_manager_get_current_document (AnjutaDocumentManager *docman);


G_END_DECLS

#endif
