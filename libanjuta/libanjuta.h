#ifndef LIBANJUTA_H
#define LIBANJUTA_H

#include "anjuta-utils.h"
#include "anjuta-tool.h"
#include "anjuta-doc.h"
#include "anjuta-session.h"

#define ANJUTA_WINDOW_LOC_TOP       GNOME_Development_Shell_LOC_TOP
#define ANJUTA_WINDOW_LOC_BOTTOM    GNOME_Development_Shell_LOC_BOTTOM
#define ANJUTA_WINDOW_LOC_LEFT      GNOME_Development_Shell_LOC_LEFT
#define ANJUTA_WINDOW_LOC_RIGHT     GNOME_Development_Shell_LOC_RIGHT
#define ANJUTA_WINDOW_LOC_CENTER    GNOME_Development_Shell_LOC_CENTER
#define ANJUTA_WINDOW_LOC_FLOATING  GNOME_Development_Shell_LOC_FLOATING
#define ANJUTA_WINDOW_LOC_LAST      GNOME_Development_Shell_LOC_LAST

#endif
