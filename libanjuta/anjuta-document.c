#include <config.h>
#include "anjuta-document.h"

#include "anjuta-marshal.h"

void 
anjuta_document_save (AnjutaDocument *document,
		      GError *error)
{
	g_return_if_fail (document != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));
	

	ANJUTA_DOCUMENT_GET_IFACE (document)->save (document, error);
}

void 
anjuta_document_save_as (AnjutaDocument *document,
			 const char *uri,
			 GError *error)
{
	g_return_if_fail (document != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));
	g_return_if_fail (uri != NULL);

	ANJUTA_DOCUMENT_GET_IFACE (document)->save (document, error);
}

const char *
anjuta_document_get_uri (AnjutaDocument *document)
{
	g_return_val_if_fail (document != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT (document), NULL);

	return ANJUTA_DOCUMENT_GET_IFACE (document)->get_uri (document);
}

const char *
anjuta_document_get_mime_type (AnjutaDocument *document)
{
	g_return_val_if_fail (document != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT (document), NULL);

	return ANJUTA_DOCUMENT_GET_IFACE (document)->get_mime_type (document);
}

Bonobo_Control
anjuta_document_get_control (AnjutaDocument *document)
{
	g_return_val_if_fail (document != NULL, CORBA_OBJECT_NIL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT (document), CORBA_OBJECT_NIL);

	return ANJUTA_DOCUMENT_GET_IFACE (document)->get_control (document);
}

static void
anjuta_document_base_init (gpointer gclass)
{
	static gboolean initialized = FALSE;
	
	if (!initialized) {
		g_signal_new ("modified",
			      ANJUTA_TYPE_DOCUMENT,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (AnjutaDocumentIface, 
					       modified),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

		g_signal_new ("unmodified",
			      ANJUTA_TYPE_DOCUMENT,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (AnjutaDocumentIface, 
					       unmodified),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
		
		g_signal_new ("uri_changed",
			      ANJUTA_TYPE_DOCUMENT,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (AnjutaDocumentIface, 
					       uri_changed),
			      NULL, NULL,
			      anjuta_marshal_VOID__STRING,
			      G_TYPE_NONE, 1,
			      G_TYPE_STRING);
	}
}

GType
anjuta_document_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (AnjutaDocumentIface),
			anjuta_document_base_init,
			NULL, 
			NULL,
			NULL,
			NULL,
			0,
			0,
			NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, 
					       "AnjutaDocument", 
					       &info,
					       0);
		
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	
	return type;			
}
