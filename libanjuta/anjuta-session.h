/* anjuta-session.h
 *
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef ANJUTA_SESSION_H
#define ANJUTA_SESSION_H

#include <glib-object.h>
#include <libxml/tree.h>

G_BEGIN_DECLS

#define ANJUTA_TYPE_SESSION		(anjuta_session_get_type ())
#define ANJUTA_SESSION(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ANJUTA_TYPE_SESSION, AnjutaSession))
#define ANJUTA_SESSION_CLASS(obj)	(G_TYPE_CHECK_CLASS_CAST ((klass), ANJUTA_TYPE_SESSION, AnjutaSessionClass))
#define ANJUTA_IS_SESSION(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ANJUTA_TYPE_SESSION))
#define ANJUTA_IS_SESSION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), ANJUTA_TYPE_SESSION))
#define ANJUTA_SESSION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ANJUTA_TYPE_SESSION, AnjutaSessionClass))

typedef struct _AnjutaSession		AnjutaSession;
typedef struct _AnjutaSessionClass	AnjutaSessionClass;
typedef struct _AnjutaSessionPrivate	AnjutaSessionPrivate;

struct _AnjutaSession {
	GObject parent;

	AnjutaSessionPrivate *priv;
};

struct _AnjutaSessionClass {
	GObjectClass parent_class;
};

/* Creation. */
GType          anjuta_session_get_type     (void);
AnjutaSession *anjuta_session_new          (const char *session_name);

void           anjuta_session_save         (AnjutaSession *session);

/* One or more values. */
void           anjuta_session_get          (AnjutaSession *session,
					    const char    *fist_name,
					    char         **fist_value,
					    ...);
void           anjuta_session_get_valist   (AnjutaSession *session,
					    const char    *first_name,
					    char         **first_value,
					    va_list        var_args);
void           anjuta_session_set          (AnjutaSession *session,
					    const char    *first_name,
					    const char    *first_value,
					    ...);
void           anjuta_session_set_valist   (AnjutaSession *session,
					    const char    *first_name,
					    const char    *first_value,
					    va_list        var_args);
void           anjuta_session_remove       (AnjutaSession *session,
					    const char    *name);

/* Groups. */
xmlNodePtr     anjuta_session_new_group    (AnjutaSession *session,
					    const char    *group);
xmlNodePtr     anjuta_session_get_group    (AnjutaSession *session,
					    const char    *group);
void           anjuta_session_remove_group (AnjutaSession *session,
					    const char    *name);

G_END_DECLS

#endif /* ANJUTA_SESSION_H */
