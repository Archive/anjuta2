/* anjuta-session.c
 *
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <string.h>
#include <sys/stat.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <libgnome/gnome-util.h>
#include "anjuta-session.h"
#include "anjuta-utils.h"

enum {
	PROP_0,
	PROP_SESSION_NAME
};

struct _AnjutaSessionPrivate {
	char *name;
	GHashTable *properties;
	GHashTable *groups;
};

static void dispose (GObject *object);
static void get_property (GObject      *object,
			  guint         prop_id,
			  GValue       *value,
			  GParamSpec   *pspec);
static void set_property (GObject      *object,
			  guint         prop_id,
			  const GValue *value,
			  GParamSpec   *pspec);

static void load_session (AnjutaSession *session);

/* Boilerplate. */
GNOME_CLASS_BOILERPLATE (AnjutaSession, anjuta_session, GObject, G_TYPE_OBJECT);

/* Private functions. */
static void
anjuta_session_class_init (AnjutaSessionClass *klass) 
{
	GObjectClass *g_object_class;

	g_object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	g_object_class->dispose = dispose;
	g_object_class->get_property = get_property;
	g_object_class->set_property = set_property;

	g_object_class_install_property 
		(g_object_class, PROP_SESSION_NAME,
		 g_param_spec_string ("name", 
				      _("Session name"),
				      _("The session name"),
				      "",
				      G_PARAM_READWRITE));
}

static void
anjuta_session_instance_init (AnjutaSession *session)
{
	AnjutaSessionPrivate *priv;

	priv = g_new0 (AnjutaSessionPrivate, 1);
	session->priv = priv;

	priv->name = NULL;
	priv->properties = g_hash_table_new_full (g_str_hash, g_str_equal,
						  g_free, g_free);
	priv->groups = g_hash_table_new_full (g_str_hash, g_str_equal,
					      g_free, NULL);
}

static void
dispose (GObject *object)
{
	AnjutaSession *session = ANJUTA_SESSION (object);
	AnjutaSessionPrivate *priv = session->priv;

	if (priv) {
		if (priv->name)
			g_free (priv->name);
		g_hash_table_destroy (priv->properties);
		g_hash_table_destroy (priv->groups);
		g_free (priv);
		session->priv = NULL;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	AnjutaSession *session = ANJUTA_SESSION (object);

	switch (prop_id) {
	case PROP_SESSION_NAME:
		g_value_set_string (value, session->priv->name);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	AnjutaSession *session = ANJUTA_SESSION (object);

	switch (prop_id) {
	case PROP_SESSION_NAME:
		if (session->priv->name) {
			g_free (session->priv->name);
			session->priv->name = NULL;
		}
		session->priv->name = g_strdup (g_value_get_string (value));
		load_session (session);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}   
}

static void
load_properties (AnjutaSession *session,
		 xmlNodePtr     node)
{
	AnjutaSessionPrivate *priv = session->priv;
	char *name, *value;

	if (!strcmp ("property", node->name)) {
		name = xmlGetProp (node, "name");
		value = xmlGetProp (node, "value");
		g_hash_table_insert (priv->properties, g_strdup (name),
				     g_strdup (value));
	} else if (!strcmp ("group", node->name)) {
		name = xmlGetProp (node, "name");
		g_hash_table_insert (priv->groups, g_strdup (name), node);
	}
}

static void
load_session (AnjutaSession *session)
{
	AnjutaSessionPrivate *priv = session->priv;
	char *dir, *filename;
	xmlDocPtr doc;
	xmlNodePtr root, node;

	/* Check if ~/.anjuta2/sessions/ exists and create it if not. */
	dir = gnome_util_prepend_user_home (".anjuta2/sessions/");
	if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		mkdir (dir, 0755);
		if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
			anjuta_dialog_error (_("Could not create sessions directory."));
			return;
		}
	}

	filename = g_strconcat (dir, "/", priv->name, ".xml", NULL);
	g_free (dir);

	/* Check if session file exists. Fallback to default session if not.
	 * If default.xml doesn't exist either, just create an empty xml doc.*/
	if (!g_file_test (filename, G_FILE_TEST_EXISTS)) {
		if (!strcmp (priv->name, "default")) {
			g_free (filename);
			return;
		} else {
			char *msg = g_strdup_printf (_("Session \"%s\" does not exist, using default session instead."),
						     priv->name);
			anjuta_dialog_error (msg);
			g_free (msg);
			g_free (priv->name);
			priv->name = g_strdup ("default");
			load_session (session);
		}
	}

	/* Load file. */
	doc = xmlParseFile (filename);
	g_free (filename);
	if (!doc) {
		g_error ("Error loading session file xml\n");
		return;
	}

	/* Parse properties. */
	root = doc->children;
	for (node = root->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE)
			load_properties (session, node);
	}
}

static void
foreach_property (gpointer key,
		  gpointer value,
		  gpointer user_data)
{
	xmlNodePtr node = user_data;
	xmlNodePtr child;

	child = xmlNewChild (node, NULL, "property", NULL);
	xmlSetProp (child, "name", (char *)key);
	xmlSetProp (child, "value", (char *)value);
}

static void
foreach_group (gpointer key,
	       gpointer value,
	       gpointer user_data)
{
	xmlNodePtr node = user_data;
	xmlNodePtr group = value;

	xmlAddChild (node, group);
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

AnjutaSession *
anjuta_session_new (const char *session_name)
{
	AnjutaSession *session;

	session = ANJUTA_SESSION (g_object_new (ANJUTA_TYPE_SESSION,
						"name", session_name,
						NULL));

	return session;
}

void
anjuta_session_save (AnjutaSession *session)
{
	AnjutaSessionPrivate *priv = session->priv;
	char *dir, *filename;
	xmlDocPtr doc;

	/* Create new xml doc. */
	doc = xmlNewDoc ("1.0");
	doc->children = xmlNewDocNode (doc, NULL, "anjuta-session", NULL);
	xmlSetProp (doc->children, "name", priv->name);

	/* Save properties & groups.. */
	g_hash_table_foreach (priv->properties, foreach_property, doc->children);
	g_hash_table_foreach (priv->groups, foreach_group, doc->children);

	/* Output xml. */
	dir = gnome_util_prepend_user_home (".anjuta2/sessions/");
	filename = g_strconcat (dir, "/", priv->name, ".xml", NULL);
	g_free (dir);
	xmlSaveFormatFile (filename, doc, TRUE);
	g_free (filename);
}

void
anjuta_session_get (AnjutaSession *session,
		    const char    *first_name,
		    char         **first_value,
		    ...)
{
	va_list var_args;

	g_return_if_fail (session != NULL);
	g_return_if_fail (ANJUTA_IS_SESSION (session));
	g_return_if_fail (first_name != NULL);
	g_return_if_fail (first_value != NULL);

	va_start (var_args, first_value);
	anjuta_session_get_valist (session, first_name, first_value, var_args);
	va_end (var_args);
}

void
anjuta_session_get_valist (AnjutaSession *session,
			   const char    *first_name,
			   char         **first_value,
			   va_list        var_args)
{
	const char *name;
	char **value;

	g_return_if_fail (session != NULL);
	g_return_if_fail (ANJUTA_IS_SESSION (session));
	g_return_if_fail (first_name != NULL);
	g_return_if_fail (first_value != NULL);

	name = first_name;
	value = first_value;

	while (name) {
		*value = g_hash_table_lookup (session->priv->properties, name);

		name = va_arg (var_args, char *);
		if (name)
			value = va_arg (var_args, char **);
	}
}

void
anjuta_session_set (AnjutaSession *session,
		    const char    *first_name,
		    const char    *first_value,
		    ...)
{
	va_list var_args;

	g_return_if_fail (session != NULL);
	g_return_if_fail (ANJUTA_IS_SESSION (session));
	g_return_if_fail (first_name != NULL);
	g_return_if_fail (first_value != NULL);

	va_start (var_args, first_value);
	anjuta_session_set_valist (session, first_name, first_value, var_args);
	va_end (var_args);
}

void
anjuta_session_set_valist (AnjutaSession *session,
			   const char    *first_name,
			   const char    *first_value,
			   va_list        var_args)
{
	AnjutaSessionPrivate *priv;
	const char *name;
	const char *value;

	g_return_if_fail (session != NULL);
	g_return_if_fail (ANJUTA_IS_SESSION (session));
	g_return_if_fail (first_name != NULL);
	g_return_if_fail (first_value != NULL);

	priv = session->priv;
	name = first_name;
	value = first_value;

	while (name) {
		if (g_hash_table_lookup (priv->properties, name)) {
			g_hash_table_replace (priv->properties,
					      g_strdup (name),
					      g_strdup (value));
		} else {
			g_hash_table_insert (priv->properties,
					     g_strdup (name),
					     g_strdup (value));
		}

		name = va_arg (var_args, char *);
		if (name)
			value = va_arg (var_args, char *);
	}
}

void
anjuta_session_remove (AnjutaSession *session,
		       const char    *name)
{
	g_return_if_fail (session != NULL);
	g_return_if_fail (ANJUTA_IS_SESSION (session));
	g_return_if_fail (name != NULL);

	g_hash_table_remove (session->priv->properties, name);
}

xmlNodePtr
anjuta_session_new_group (AnjutaSession *session,
			  const char    *name)
{
	xmlNodePtr group;

	g_return_val_if_fail (session != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_SESSION (session), NULL);
	g_return_val_if_fail (name != NULL, NULL);

	if (g_hash_table_lookup (session->priv->groups, name)) {
		g_warning ("Group already exists");
		return NULL;
	}

	group = xmlNewNode (NULL, "group");
	xmlSetProp (group, "name", name);
	g_hash_table_insert (session->priv->groups, g_strdup (name), group);

	return group;
}

xmlNodePtr
anjuta_session_get_group (AnjutaSession *session,
			  const char    *name)
{
	g_return_val_if_fail (session != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_SESSION (session), NULL);
	g_return_val_if_fail (name != NULL, NULL);

	return (xmlNodePtr)g_hash_table_lookup (session->priv->groups, name);
}

void
anjuta_session_remove_group (AnjutaSession *session,
			     const char    *name)
{
	xmlNodePtr group;

	g_return_if_fail (session != NULL);
	g_return_if_fail (ANJUTA_IS_SESSION (session));
	g_return_if_fail (name != NULL);

	group = g_hash_table_lookup (session->priv->groups, name);
	if (group) {
		xmlUnlinkNode (group);
		xmlFreeNode (group);
		g_hash_table_remove (session->priv->groups, name);
	}
}
