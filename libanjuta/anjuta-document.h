#ifndef ANJUTA_DOCUMENT_H
#define ANJUTA_DOCUMENT_H

#include <glib-object.h>
#include <gtk/gtkwidget.h>
#include <bonobo/bonobo-control.h>

G_BEGIN_DECLS

#define ANJUTA_TYPE_DOCUMENT            (anjuta_document_get_type ())
#define ANJUTA_DOCUMENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ANJUTA_TYPE_DOCUMENT, AnjutaDocument))
#define ANJUTA_IS_DOCUMENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ANJUTA_TYPE_DOCUMENT))
#define ANJUTA_DOCUMENT_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), ANJUTA_TYPE_DOCUMENT, AnjutaDocumentIface))

typedef struct _AnjutaDocument      AnjutaDocument;
typedef struct _AnjutaDocumentIface AnjutaDocumentIface;

struct _AnjutaDocumentIface {
	GTypeInterface g_iface;
	
	/* Signals */
	void (*modified)    (AnjutaDocument *document);
	void (*unmodified)  (AnjutaDocument *document);

	void (*uri_changed) (AnjutaDocument *document,
			     const char     *uri);

	/* Virtual Table */
	void            (*save)          (AnjutaDocument *document, 
				          GError         *error);
	void            (*save_as)       (AnjutaDocument *document, 
				          const char *filename,
				          GError *error);
	const char     *(*get_uri)       (AnjutaDocument *document);
	const char     *(*get_mime_type) (AnjutaDocument *document);
	Bonobo_Control  (*get_control)   (AnjutaDocument *document);
};

GType          anjuta_document_get_type      (void);

void           anjuta_document_save          (AnjutaDocument *document,
					      GError         *error);
void           anjuta_document_save_as       (AnjutaDocument *document,
					      const char     *uri,
					      GError         *error);

const char *   anjuta_document_get_uri       (AnjutaDocument *document);
const char *   anjuta_document_get_mime_type (AnjutaDocument *document);
Bonobo_Control anjuta_document_get_control   (AnjutaDocument *document);

G_END_DECLS

#endif
