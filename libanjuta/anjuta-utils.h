/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ANJUTA_UTILS_H__
#define __ANJUTA_UTILS_H__

#include <gtk/gtkdialog.h>

G_BEGIN_DECLS
GList          *anjuta_get_lang_list               (void);

GtkResponseType anjuta_dialog_question             (const char *msg);
void            anjuta_dialog_error                (const char *msg);
void            anjuta_dialog_info                 (const char *msg);

gboolean        anjuta_is_empty_string             (const char *s);
gulong          anjuta_get_file_size               (const char *filename);
gboolean        anjuta_file_exists                 (const char *filename);
gboolean        anjuta_file_check_if_exists        (const char *filename,
						    gboolean    askcreate);

/* Temporarily copied here */

#define ANJUTA_TYPE_BEGIN(class_name, prefix, parent_type) \
GType                                                     \
prefix##_get_type (void)                                  \
{                                                         \
  static GType type = 0;                                  \
  if (!type)                                              \
    {                                                     \
 static const GTypeInfo type_info =                       \
        {                                                 \
          sizeof (class_name##Class),                     \
          (GBaseInitFunc) NULL,                           \
          (GBaseFinalizeFunc) NULL,                       \
          (GClassInitFunc) prefix##_class_init,           \
          (GClassFinalizeFunc) NULL,                      \
          NULL,                                           \
          sizeof (class_name),                            \
          0, /* n_preallocs */                            \
          (GInstanceInitFunc) prefix##_instance_init,     \
        };                                                \
\
        type = g_type_register_static (parent_type,\
                                          #class_name,\
                                          &type_info, 0);


#define ANJUTA_TYPE_END \
     } \
  return type; \
}

#define ANJUTA_INTERFACE(prefix,interface_type)                 \
{                                                               \
GInterfaceInfo iface_info = { (GInterfaceInitFunc)prefix##_iface_init, NULL, NULL };\
g_type_add_interface_static (type, interface_type,              \
                             &iface_info);                      \
}

G_END_DECLS

#endif
