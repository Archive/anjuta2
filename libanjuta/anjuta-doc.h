/* Anjuta
 * Copyright (C) 2001 Dirk Vangestel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef ANJUTA_DOC_H
#define ANJUTA_DOC_H

#include "anjuta-tool.h"
#include "anjuta-document.h"
#include <gdl/GDL.h>
#include <glib.h>

G_BEGIN_DECLS

CORBA_Object anjuta_get_editor_interface (AnjutaDocument *document,
					  const char *interface);
CORBA_Object anjuta_get_current_editor_interface (AnjutaTool *tool, 
						  const char *interface);
GNOME_Development_EditorBuffer anjuta_get_editor_buffer (AnjutaTool *tool);

gboolean anjuta_show_file             (AnjutaTool *tool,
				       const char *uri);
char*    anjuta_get_current_uri       (AnjutaTool *tool);
char *   anjuta_get_current_word      (AnjutaTool *tool);
long     anjuta_get_document_length   (AnjutaTool *tool);
char*    anjuta_get_document_chars    (AnjutaTool *tool,
				       long        start_pos,
				       long        end_pos);
long     anjuta_get_line_num          (AnjutaTool *tool);
void     anjuta_set_line_num          (AnjutaTool *tool,
				       long        line);
long     anjuta_get_cursor_pos        (AnjutaTool *tool);
void     anjuta_set_cursor_pos        (AnjutaTool *tool,
				       long        pos);
void     anjuta_insert_text_at_pos    (AnjutaTool *tool,
				       long        pos,
				       const char *text);
void     anjuta_insert_text_at_cursor (AnjutaTool *tool,
				       const char *text);
void     anjuta_delete_text           (AnjutaTool *tool,
				       long        startpos,
				       long        endpos);

G_END_DECLS

#endif
