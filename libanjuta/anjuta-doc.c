/* Anjuta
 * Copyright (C) 2001 Dirk Vangestel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

/*
 * Document related functions for anjuta plugins
 */

/* TODO: unref, add checks, check exceptions */

#include <config.h>
#include "anjuta-doc.h"

#include <ctype.h>
#include <libanjuta/anjuta-document-manager.h>
#include <libanjuta/anjuta-tool.h>
#include <gdl/GDL.h>

#include <libbonobo.h>
#include <string.h>

CORBA_Object
anjuta_get_editor_interface (AnjutaDocument *document, const char *interface)
{
	CORBA_Environment ev;
	CORBA_Object ret;
	Bonobo_Control ctrl;
	
	CORBA_exception_init (&ev);
	ctrl = anjuta_document_get_control (document);
	ret = Bonobo_Unknown_queryInterface (ctrl, interface, &ev);
	bonobo_object_release_unref (ctrl, &ev);

	CORBA_exception_free (&ev);
	return ret;
}

CORBA_Object
anjuta_get_current_editor_interface (AnjutaTool *tool, const char *interface)
{
	AnjutaDocument *document;
	
	anjuta_shell_get (tool->shell, 
			  "CurrentDocument",
			  ANJUTA_TYPE_DOCUMENT,
			  &document,
			  NULL);
	
	if (document) {
		return anjuta_get_editor_interface (document, interface);
	}	  

	return CORBA_OBJECT_NIL;
}

GNOME_Development_EditorBuffer
anjuta_get_editor_buffer (AnjutaTool *tool)
{
	return anjuta_get_current_editor_interface (tool, "IDL:GNOME/Development/EditorBuffer:1.0");
}

gboolean
anjuta_show_file (AnjutaTool *tool, const char *uri)
{
	AnjutaDocumentManager *docman;
	
	anjuta_shell_get (tool->shell, 
			  "DocumentManager",
			  ANJUTA_TYPE_DOCUMENT_MANAGER,
			  &docman,
			  NULL);

	if (docman) {
		AnjutaDocument *doc;

		doc = anjuta_document_manager_get_document_for_uri (docman,
								    uri,
								    FALSE,
								    NULL);
		if (doc) {
			anjuta_document_manager_show_document (docman, doc);
			return TRUE;
		}
	}

	return FALSE;
}

static long
get_document_property_long (AnjutaTool *tool, const char *name)
{
	long prop = -1;
	Bonobo_Control			ctrl;
	Bonobo_PropertyBag		bag;
	CORBA_Environment		ev;

	CORBA_exception_init(&ev);

	ctrl = anjuta_get_current_editor_interface (tool, 
						    "IDL:Bonobo/Control:1.0");

	if (CORBA_Object_is_nil (ctrl, &ev)) {
		CORBA_exception_free (&ev);
		return -1;
	}
		
	bag = Bonobo_Control_getProperties(ctrl, &ev);
	prop = bonobo_pbclient_get_long(bag, name, &ev);
	if (BONOBO_EX (&ev)) {
		prop = -1;
	}

	bonobo_object_release_unref (ctrl, &ev);
	bonobo_object_release_unref (bag, &ev);

	CORBA_exception_free(&ev);

	return prop;	
}

static void
set_document_property_long (AnjutaTool *tool, const char *name, long val)
{
	Bonobo_Control			ctrl;
	Bonobo_PropertyBag		bag;
	CORBA_Environment		ev;

	CORBA_exception_init(&ev);

	ctrl = anjuta_get_current_editor_interface (tool, 
						    "IDL:Bonobo/Control:1.0");

	if (CORBA_Object_is_nil (ctrl, &ev)) {
		CORBA_exception_free (&ev);
		return;
	}
		
	bag = Bonobo_Control_getProperties(ctrl, &ev);

	bonobo_pbclient_set_long (bag, name, val, NULL);

	bonobo_object_release_unref (ctrl, &ev);
	bonobo_object_release_unref (bag, &ev);

	CORBA_exception_free (&ev);
}

long
anjuta_get_cursor_pos (AnjutaTool *tool)
{
	return get_document_property_long (tool, "position");
}

void
anjuta_set_cursor_pos (AnjutaTool *tool, long pos)
{
	set_document_property_long (tool, "position", pos);
}


static int 
anjuta_find_word_begin (AnjutaTool *tool, 
			GNOME_Development_EditorBuffer buffer,
			int pos)
{
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer_iobuf *iobuf;
	
	CORBA_exception_init (&ev);
	GNOME_Development_EditorBuffer_getChars (buffer, pos, 1, &iobuf, &ev);
	while (!BONOBO_EX (&ev) && 
	       iobuf->_length == 1 && 
	       (isalpha (iobuf->_buffer[0]) || iobuf->_buffer[0] == '_') &&
	       pos >= 0) {
		
		pos--;
		CORBA_free (iobuf);
		GNOME_Development_EditorBuffer_getChars (buffer, 
							 pos, 1, &iobuf, &ev);
	}

	pos++;
	
	if (!BONOBO_EX (&ev)) {
		CORBA_free (iobuf);
	}
	CORBA_exception_free (&ev);
	return pos;
}

static int
anjuta_find_word_end (AnjutaTool *tool, 
		      GNOME_Development_EditorBuffer buffer,
		      int pos)
{
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer_iobuf *iobuf;
	long size;

	CORBA_exception_init (&ev);
	size = GNOME_Development_EditorBuffer_getLength (buffer, &ev);
	GNOME_Development_EditorBuffer_getChars (buffer, pos, 1, &iobuf, &ev);
	while (!BONOBO_EX (&ev) && 
	       iobuf->_length == 1 && 
	       (isalpha (iobuf->_buffer[0]) || iobuf->_buffer[0] == '_') &&
	       pos < size) {
		
		pos++;
		CORBA_free (iobuf);
		GNOME_Development_EditorBuffer_getChars (buffer, 
							 pos, 1, &iobuf, &ev);
	}
	
	if (!BONOBO_EX (&ev)) {
		CORBA_free (iobuf);
	}
	CORBA_exception_free (&ev);
	return pos;
}

char *
anjuta_get_current_word (AnjutaTool *tool)
{
	char *str = NULL;
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer buffer;

	CORBA_exception_init (&ev);
	buffer = anjuta_get_editor_buffer (tool);
	/* FIXME: This could be a lot lot more efficient than it is. */
	if (!CORBA_Object_is_nil (buffer, &ev)) {
		int begin, end, pos;
 		GNOME_Development_EditorBuffer_iobuf *iobuf;

		pos = anjuta_get_cursor_pos (tool);

		begin = anjuta_find_word_begin (tool, buffer, pos);
		end = anjuta_find_word_end (tool, buffer, pos);

		if (end - begin > 1) {
			GNOME_Development_EditorBuffer_getChars (buffer, 
								 begin,
								 end - begin, 
								 &iobuf,
								 &ev);
			if (!BONOBO_EX (&ev)) {
				if (iobuf->_length > 0) {
					str = g_strndup (iobuf->_buffer, 
							 iobuf->_length);
				}
				CORBA_free (iobuf);
			}
		}
	}
	
	CORBA_exception_free (&ev);
	return str;
}

long
anjuta_get_line_num (AnjutaTool *tool)
{
	return get_document_property_long (tool, "line_num");
}

void
anjuta_set_line_num (AnjutaTool *tool, long pos)
{
	return set_document_property_long (tool, "line_num", pos);
}

void
anjuta_insert_text_at_pos (AnjutaTool *tool, long pos, const char *text)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	buffer = anjuta_get_editor_buffer (tool);
	if (!buffer) {
		CORBA_exception_free(&ev);
		return;
	}

	/* Insert text */
	GNOME_Development_EditorBuffer_insert (buffer, pos, text, &ev);

	anjuta_set_cursor_pos (tool, pos + strlen (text));

	bonobo_object_release_unref (buffer, &ev);

	CORBA_exception_free (&ev);
}

void
anjuta_insert_text_at_cursor (AnjutaTool *tool, const char *text)
{
	long pos;

	pos = anjuta_get_cursor_pos (tool);

	anjuta_insert_text_at_pos (tool, pos, text);
}

void
anjuta_delete_text (AnjutaTool *tool, long startpos, long endpos)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;
	long temp;

	CORBA_exception_init (&ev);

	buffer = anjuta_get_editor_buffer (tool);
	if (!buffer) {
		CORBA_exception_free (&ev);
		return;
	}

	if (startpos > endpos) {
		temp = startpos;
		startpos = endpos;
		endpos = temp;
	}

	GNOME_Development_EditorBuffer_delete (buffer, startpos, endpos - startpos, &ev);

	bonobo_object_release_unref (buffer, &ev);
	CORBA_exception_free(&ev);
}

long
anjuta_get_document_length (AnjutaTool *tool)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;
	long length;

	CORBA_exception_init (&ev);

	buffer = anjuta_get_editor_buffer (tool);
	if (!buffer)
		return 0;

	length = GNOME_Development_EditorBuffer_getLength (buffer, &ev);

	bonobo_object_release_unref (buffer, &ev);

	CORBA_exception_free (&ev);

	return length;
}

char *
anjuta_get_document_chars (AnjutaTool *tool, long start_pos, long end_pos)
{
	GNOME_Development_EditorBuffer	buffer;
	CORBA_Environment		ev;
	long				length;
	long				temp;
	GNOME_Development_EditorBuffer_iobuf*	buf;
	char*				result;

	if (start_pos > end_pos) {
		temp = start_pos;
		start_pos = end_pos;
		end_pos = temp;
	}

	CORBA_exception_init(&ev);

	buffer = anjuta_get_editor_buffer (tool);
	if (!buffer)
		return NULL;

	length = GNOME_Development_EditorBuffer_getLength (buffer, &ev);

	if (start_pos > length)
		return NULL;

	if (end_pos > length)
		end_pos = length;

	GNOME_Development_EditorBuffer_getChars (buffer, start_pos,
						 end_pos - start_pos, 
						 &buf, &ev);

	result = g_strndup (buf->_buffer, buf->_length);

	CORBA_free (buf);

	bonobo_object_release_unref (buffer, &ev);
	
	CORBA_exception_free (&ev);

	return result;
}

char *
anjuta_get_current_uri (AnjutaTool *tool)
{
	AnjutaDocument *document;

	anjuta_shell_get (tool->shell, 
			  "CurrentDocument", 
			  ANJUTA_TYPE_DOCUMENT,
			  &document,
			  NULL);
	
	if (document) {
		return g_strdup (anjuta_document_get_uri (document));
	}

	return NULL;
}

