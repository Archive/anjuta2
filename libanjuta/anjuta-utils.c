/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Copied here from Anjuta/src/gI_util.c */
/* FIXME: Because of the fact that this is for components, the dialog
 * parenting stuff is removed.  This needs to be resolved */

#include <ctype.h>
#include <string.h>
#include <gtk/gtk.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomevfs/gnome-vfs.h>
#include <bonobo/bonobo-file-selector-util.h>
#include <bonobo/bonobo-event-source.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-widget.h>
#include "anjuta-utils.h"

#define RETURN_DATA "return_data"


GList *
anjuta_get_lang_list (void)
{
        GSList *retval;
        const char *lang;
        char * equal_char;

        retval = NULL;

        lang = g_getenv ("LANGUAGE");

        if (!lang) {
                lang = g_getenv ("LANG");
        }


        if (lang) {
                equal_char = strchr (lang, '=');
                if (equal_char != NULL) {
                        lang = equal_char + 1;
                }

                retval = g_slist_prepend (retval, (char *)lang);
        }
        
        return retval;
}

gboolean
anjuta_is_empty_string (const char *s)
{
	if (!s)
		return TRUE;
	if (strlen (s) == 0)
		return TRUE;

	while (*s) {
		if (!isspace (*s))
			return FALSE;
		s++;
	}

	return TRUE;
}

GtkResponseType
anjuta_dialog_question (const char *msg)
{
	GtkWidget *dialog;
	GtkResponseType ret;

	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_QUESTION,
					 GTK_BUTTONS_YES_NO,
					 msg);

	ret = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);

	return ret;
}

void
anjuta_dialog_error (const char *msg)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_ERROR,
					 GTK_BUTTONS_OK,
					 msg);

	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

void 
anjuta_dialog_info (const char *msg)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_INFO,
					 GTK_BUTTONS_OK,
					 msg);

	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

gboolean
anjuta_file_exists (const char *filename)
{
	GnomeVFSURI *uri;
	gboolean ret;

	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (strlen (filename) != 0, FALSE);

	uri = gnome_vfs_uri_new (filename);
	ret = gnome_vfs_uri_exists (uri);
	gnome_vfs_uri_unref (uri);

	return ret;
}

gulong 
anjuta_get_file_size (const char *filename)
{
	glong ret = -1;
	GnomeVFSFileInfo *info;
	GnomeVFSResult r;

	info = gnome_vfs_file_info_new ();
	r = gnome_vfs_get_file_info (filename, info, 
				     GNOME_VFS_FILE_INFO_DEFAULT);
	if (r == GNOME_VFS_OK) 
		ret = (glong) info->size;
	else
		g_warning ("%s: %s", filename, gnome_vfs_result_to_string (r));

	gnome_vfs_file_info_unref (info);

	return ret;
}

gboolean
anjuta_file_check_if_exists (const char *filename,
			     gboolean    askcreate)
{
	GnomeVFSURI *uri;
	gboolean ret = FALSE;

	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (strlen (filename) != 0, FALSE);

	uri = gnome_vfs_uri_new (filename);

	if (gnome_vfs_uri_exists (uri))
		ret = TRUE;
	else if (askcreate) {
		gchar *txt;
		txt = g_strdup_printf (_("The file\n'%s'\ndoes not "
					 "exist!\nDo you want to "
					 "create the file?"), filename);

		if (anjuta_dialog_question (txt) == GTK_RESPONSE_YES) {
			GnomeVFSResult r;
			GnomeVFSHandle *handle = NULL;

			/* FIXME: is the default permission ok? */
			r = gnome_vfs_create_uri (&handle, uri, 
						  GNOME_VFS_OPEN_WRITE,
						  TRUE, 0660);
			
			if (r == GNOME_VFS_OK) {
				gnome_vfs_close (handle);
				ret = TRUE;
			} else {
				g_free (txt);
				txt = g_strdup_printf (
					_("Error creating file '%s': %s'"), 
					filename, 
					gnome_vfs_result_to_string (r));
				anjuta_dialog_error (txt);
			}
		}
		g_free (txt);
	}

	gnome_vfs_uri_unref (uri);

	return ret;
}
