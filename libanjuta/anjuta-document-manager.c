#include <config.h>
#include "anjuta-document-manager.h"

#include "anjuta-marshal.h"

AnjutaDocument * 
anjuta_document_manager_new_document (AnjutaDocumentManager *docman,
				      const char *mime_type,
				      GError *error)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (mime_type != NULL, NULL);

	return ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->new_document
		(docman, mime_type, error);
}

AnjutaDocument * 
anjuta_document_manager_open (AnjutaDocumentManager *docman,
			      const char *uri,
			      GError *error)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (uri != NULL, NULL);

	return ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->open (docman, 
								 uri, 
								 error);
}

void
anjuta_document_manager_close (AnjutaDocumentManager *docman,
			       AnjutaDocument *document,
			       GError *error)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));

	ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->close (docman, 
							   document, 
							   error);
}

void
anjuta_document_manager_close_all (AnjutaDocumentManager *docman,
				   GError *error)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));

	ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->close_all (docman, error);
}

void
anjuta_document_manager_save_all (AnjutaDocumentManager *docman,
				  GError *error)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));

	ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->save_all (docman, 
							      error);
}

int
anjuta_document_manager_num_documents (AnjutaDocumentManager *docman)
{
	g_return_val_if_fail (docman != NULL, -1);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), -1);

	return ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->num_documents 
		(docman);
}

AnjutaDocument * 
anjuta_document_manager_get_nth_document (AnjutaDocumentManager *docman,
					  int i)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);

	return ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->get_nth_document
		(docman, i);
}

AnjutaDocument * 
anjuta_document_manager_get_document_for_uri (AnjutaDocumentManager *docman,
					      const char *uri,
					      gboolean existing_only,
					      GError *error)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (uri != NULL, NULL);

	return ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->get_document_for_uri
		(docman, uri, existing_only, error);
}

void
anjuta_document_manager_show_document (AnjutaDocumentManager *docman,
				       AnjutaDocument *document)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));

	ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->show_document (docman, 
								   document);
}

AnjutaDocument * 
anjuta_document_manager_get_current_document (AnjutaDocumentManager *docman)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);

	return ANJUTA_DOCUMENT_MANAGER_GET_IFACE (docman)->get_current_document
		(docman);
	
}

static void
anjuta_document_manager_base_init (gpointer gclass)
{
	static gboolean initialized = FALSE;
	
	if (!initialized) {
		g_signal_new ("current_document_changed",
			      G_TYPE_FROM_CLASS (gclass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (AnjutaDocumentManagerIface, 
					       current_document_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1,
			      ANJUTA_TYPE_DOCUMENT);
	}
}

GType
anjuta_document_manager_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (AnjutaDocumentManagerIface),
			anjuta_document_manager_base_init,
			NULL, 
			NULL,
			NULL,
			NULL,
			0,
			0,
			NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, 
					       "AnjutaDocumentManager", 
					       &info,
					       0);
		
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	
	return type;			
}
