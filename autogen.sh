#!/bin/sh
# Run this to generate all the initial makefiles, etc.

echo "******************************************************************";
echo "You probably want to checkout 'anjuta' cvs module and not this one";
echo "******************************************************************";
echo "";
exit 1;

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="anjuta2"

(test -f $srcdir/configure.in \
  && test -f $srcdir/ChangeLog \
  && test -d $srcdir/src) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

which gnome-autogen.sh || {
    echo "You need to install gnome-common from the GNOME CVS"
    exit 1
}
USE_GNOME2_MACROS=1 . gnome-autogen.sh
         

