<!-- LinuxDoc file was created by LyX 0.12 (C) 1995-1998 by <alfi> Sun Nov  1 18:10:30 1998
 -->
<!-- Export filter v0.6 by Pascal Andre/Bernhard Iselborn-->

<!doctype linuxdoc system>

<article>

<title>gIDE Documentation
<toc>
<sect>Summary
<p>

gIDE is a gtk+ based Integrated Development Environment for the C programming
 language&lsqb;2&rsqb;. It is currently in heavy development, 
but it already
 features a powerful source editor&lsqb;1&rsqb;, Project Management and much
 more.
<sect>Requirements
<p>
<itemize>
<item>UNIX w/ X11 (gIDE has been successfully compiled on Linux/x86, Linux/Alpha,
 Linux/m68k, FreeBSD/x86 and Irix/SGI) 
<item>GTK+ widget kit (version 1.1.2 or newer), with the gtktext patch (this
 patch is required for the GtkEditor widget to work)
<item>C-Compiler (GNU C), Debugger (GNU GDB)
<item>Bash, XTerm
</itemize>
<sect>gIDE in Detail
<p>
<sect1>Editor
<p>
<sect2>New File (Alt-N) 
<p>

Creates a new 'Unitled' file in the editor.

<sect2>Open File (ALT-O)
<p>

Shows the File-Selector and opens the selected file in the editor.

<sect2>Reload File (ALT-R)
<p>

Reloads the current file.

<sect2>Save File (ALT-S)
<p>

Saves the current file. If no filename is set yet, the File-Selector dialog
 shows up.

<sect2>Print File 

<p>
<sect2>Close File (ALT-Q)
<p>

Closes the current file. If there are unsaved changes, a dialog pops up
 and asks if you really want to close the file.

<sect2>Close all Files
<p>

Closes all files in the editor, if there are unsaved changes, a dialog
 pops up for every changed file and asks if you really want to close it.

<sect2>Exit gIDE (ALT-X)
<p>

Writes the file history, closes all files and exits gIDE.

<sect2>Highlighting&lsqb;1&rsqb;
<p>

The content of the widget is parsed in constant intervalls using regular
 expressions, it depends on the pattern (regex patterns) and globassoc (associates
 filenames with patterns) files, which are described later in the Preferences
 section. The highlighting is very slow at the moment, this will change in future
 (TMJ is working on it).

<sect2>Indenting&lsqb;1&rsqb;
<p>

There's a Indenter integrated in the GtkEditor widget. If there's no selection
 made and the you press TAB the current line is indented one tab. If there's
 a selection and you press TAB the whole selection is indented one tab. To return
 to the normal "TAB-Key-Behavour" disable the Indenting in the Preferences.
 

<sect2>Shortcuts
<p>

Motion Shortcuts:
<itemize>
<item>Ctrl-A Beginning of line
<item>Ctrl-E End of line
<item>Ctrl-N Next Line
<item>Ctrl-P Previous Line
<item>Ctrl-B Backward one character
<item>Ctrl-F Forward one character
<item>Alt-B Backward one word
<item>Alt-F Forward one word
</itemize>

Editing Shortcuts
<itemize>
<item>Ctrl-H Delete Backward Character (Backspace)
<item>Ctrl-D Delete Forward Character (Delete)
<item>Ctrl-W Delete Backward Word
<item>Alt-D Delete Forward Word
<item>Ctrl-K Delete to end of line
<item>Ctrl-U Delete line 
</itemize>

Selection Shortcuts
<itemize>
<item>Ctrl-X Cut to clipboard
<item>Ctrl-C Copy to clipboard
<item>Ctrl-V Paste from clipboard
<item>Alt-A Select All
<item>Alt-L Select Line

</itemize>
<sect1>Project Management
<p>
<sect2>Create a Project
<p>

Select "New Project" from the Project Menu and enter the Project Name
 and if available Project Version, Main Target and ChangeLog-File. When the
 user clicks the OK-Button, all informations get saved.

<sect2>Open a Project
<p>

Select "Open Project" from the Project-Menu. A box with all projects
 in the gIDE Projects Directory pops up. Select the project from the list and
 click the "Open"-Button, or just double-click the list-item. In the options
 box below the list, you can choose if you want to open all files of the project
 and/or you want to close files that are currently opened.

<sect2>Edit a Project
<p>

First you have to open the project, then select "Edit Project" from the
 Project-Menu, do your changes and click the OK-Button.

<sect2>Close a Project
<p>

To close a project, select "Close Project" from the Project Menu. All
 opened project files will be closed. This is only possible, if you've opened
 a project before.

<sect2>Delete a Project
<p>

To delete a project (and perhaps all it's files), select "Delete Project"
 from the Project Menu. This is only possible, if you've opened a project before.

<sect2>Targets
<p>

To add a target, go to the "Targets"-Notebook-Page, enter the name of
 the new target into the entry and click the "Add Target"-Button. To remove
 a target, select the target to remove from the List and click the "Remove
 Target"-Button. When you remove a target, all associated files will be removed
 from the project, too. 

<sect2>Files
<p>

Go to the "Targets"-Notebook-Page and select the target you want to add
 the file to. Then go to the "Sources"-Notebook-Page and click the "Add Files"-Button.
 Select the File using the File-Selector and click the OK-Button. Select the
 File to remove from the List and click the "Remove File"-Button.

<sect2>Libraries
<p>

Go to the "Libraries"-Notebook-Page, enter the name of the library into
 the entry and click the "Add"-Button. You have to add all libraries that
 are neccessary to compile the project. Select the Library to remove from the
 List and click the "Remove"-Button.

<sect2>Parameters
<p>

On the "Parameters"-Notebook-Page you have to specifiy the Compile/Link-Parameters
 for the project.

<sect2>ToDo
<p>

To add a item to the list, enter the text into the entry on the "ToDo"-Notebook-Page
 and click the "Add"-Button. The item is added with status 'To Do'. It's possible
 to inc- or decrease the status of a item with the "Inc. Status" and "Dec.
 Status"-Buttons. You can add up to 250 ToDo-Items to a project. The status
 of a Todo-Item can be:
<enum>
<item>To Do
<item>Work in Progress
<item>Done
</enum>

The item can be automatically removed from the list, if it's done. And
 it's possible to automatically add a line to the project's changelog (both
 setable in Preferences). To remove a item, select it in the list and click
 the "Remove"-Button.

<sect2>Build Project
<p>

This tries to build the currently opened project. It's like the Makefile
 generation (described below).

<sect2>Create Makefile
<p>

This creates a simple (primitive) Makefile for a project. It does compile
 all C source files and links them together to the main target specified in
 the project. The name of the Makefile will be 'Makefile.&lt;project&gt;'. This
 is just to make sure that no other Makefile will be overwritten. If 'Makefile.&lt;project&gt;'
 does already exist, a dialog pops up and asks if you want to overwrite the
 file or abort.

<sect2>Create configure
<p>

Not Implemented!

<sect2>Create Distribution
<p>

Not Implemented!

<sect1>Run
<p>
<sect2>Run
<p>

If a project is opened and a main target is specified, gIDE tries to run
 it. If no project is opened, gIDE tries to run the executable of the current
 document, if there's no executable it tries to compile one.

<sect2>Pararameters
<p>

The parameters specified here are used to run the executables.

<sect1>Compile
<p>
<sect2>Compile to Executable
<p>

This tries to compile the current document (it needs to have '.c' extension)
 to an executable using the C-compiler and the parameters specified in the Preferences.
 The compile process is shown in the Compile Messages window.

<sect2>Compile to Object
<p>

This tries to compile the current document (it needs to have '.c' extension)
 to an object using the C-compiler and the parameters specified in the Preferences.
 The compile process is shown in the Compile Messages window.

<sect2>Make
<p>

Make (as specified in the Preferences) is called and the output is shown
 in the Compile Messages window.

<sect2>Link
<p>

This tries to link the object of the current document (it needs to have
 '.c' extension) to an executable using the parameters from the Preferences.
 If there's no object found, 'Compile to Executable' is called.

<sect2>Syntax Check
<p>

The specified compiler is called with the '-fsyntax-only' option. For the
 GNU C compiler this works. There's no compilation done, only the syntax is
 checked and the output is displayed in the 'Syntax Check Window' where you
 can double-click a error/warning to jump to the associated source line.

<sect2>Compile Messages Window / Syntax Check Window
<p>

The Compile Messages Window (the Syntax Check Window is the same window
 with another title) shows the output of all compile/link processes (can be
 more than one window). You can double-click on a line containing a error/warning
 (with file/line number) and gIDE opens the file in the editor (if it's not
 already opened) and scrolls to the line.

<sect1>Debug
<p>
<sect2>Start Debug (F5)
<p>

Opens the Debug Output Window (which shows the GDB input/output), runs
 GDB, loads an executable (the project main target, if a project is opened or
 the executable of the current document ('.c' extension needed)) and sets several
 parameters.

<sect2>Stop Debug (Shift-F5)
<p>

Terminates the GDB process and closes all debug ging windows.

<sect2>Attach to Process
<p>

Asks for a Process ID and attaches GDB to the given process.

<sect2>Set|Clear Breakpoint (F9)
<p>

Sets a breakpoint at the current cursor position. If there's already a
 breakpoint at the current position, it will be cleared.

<sect2>Step Over (F10)
<p>

Steps to the next command. (GDB cmd: next).

<sect2>Step Into (F11)
<p>

Steps into the current command (GDB cmd: step).

<sect2>Break (F12)
<p>

Sends a SIGINT to the process running in GDB (if there's one), simulating
 a STRG-C. Break.

<sect2>Debug Output Window
<p>

In this window the GDB input/output is shown. It also includes a entry
 for GDB commands. The entry is connected to the command queue system, so it's
 not possible to send input to console programs this way, at the moment. The
 Debug Output Window contains a popup menu that is activated if you press the
 right mouse button:
<itemize>
<item>Breakpoints/Add (F9): A dialog shows up and asks you for a breakpoint.
 You can use any format that GDB allows for the 'break' command. For example:
 123 (a line number), file.c:123 (filename + line number) or main:10 (function
 + line number).
<item>Breakpoints/Clear: A dialog shows up and asks you for a breakpoint. You
 have to enter a breakpoint number here.
<item>Breakpoints/View (ALT-F9): The Breakpoints Window (described below) shows
 up. You can remove breakpoints and jump to a breakpoint in the source here.
<item>Program/Break (F12): see 3.5.7. 
<item>Program/Continue (F6): Continues a interrupted (break'd) program.
<item>Program/Kill: Kills a interrupted (break'd) program.
<item>Program/Run (F5): Runs the loaded program.
<item>Backtrace: Shows the Backtrace Window (described below) and sends the backtrace
 command to GDB. The output is shown in the Backtrace Window.
<item>Run to Cursor: Runs the program until the line at the current cursor position
 in the editor is reached.
<item>Step Into (F11): see 3.5.6.
<item>Step Out: Leaves the current function.
<item>Step Over (F10): see 3.5.5.

</itemize>
<sect2>Breakpoints Window
<p>

The window contains a list of all breakpoints. Select a row in the list
 and press the right mouse button:
<itemize>
<item>Clear Breakpoint: Remove the selected breakpoint.
<item>View Source: Open/Switch to the file and scroll to the line where the breakpoint
 is set.

</itemize>
<sect2>Backtrace Window
<p>

The window contains a list of all frames shown through the GDB backtrace
 command. Select a row in the list and press the right mouse button:
<itemize>
<item>Goto Frame: Go to the selected frame.
<item>View Source: Open/Switch to the file and scroll to the line where the frame
 starts.

</itemize>
<sect1>Tools
<p>
<sect2>ASCII Table
<p>

Shows a ASCII-Table with three fields: 'Char', 'Dec&num;' and 'Hex&num;'.
 If you double-click a line in the list, the character will be inserted at the
 current position in the text.

<sect2>Prototype Generator
<p>

This is a Prototype Generator for C source files. The input and output
 files must be specified. You can decide if you want to skip static declarations
 and if you want to append the prototypes to the target file.

<sect1>Preferences
<p>
<sect2>Compile / Compile-Sets
<p>

Beginning with gIDE 0.0.11, you can define Compile-Sets here. This makes
 gIDE more language-independant. But of course, gIDE is still mainly designed
 for the C programming language. The Compile-Sets are saved in the compile_sets
 file in the gIDE directory. A sample file is in the gIDE source distribution.
 A Compile-Sets consists of:
<itemize>
<item>a name
<item>File extensions (to recognize the type of file), also called file pattern.
 Example: .c:.C
<item>Compiler (Example: /usr/bin/gcc)
<item>Linker (Example: /usr/bin/ld)
<item>Compile-Parameters (Include-Paths and Options)
<item>Link-Parameters (Library-Paths and Options)
</itemize>

If you want, you can specify Compile- and Link-Commandlines. The following
 variables are available for the use in the command lines:
<itemize>
<item>&percnt;compiler&percnt; for the specified compiler
<item>&percnt;linker&percnt; for the specified linker
<item>&percnt;copts&percnt; for the specified compile options
<item>&percnt;lopts&percnt; for the specified link options
<item>&percnt;incpaths&percnt; for the specified Include-Paths (results in: -Ipath1
 -Ipath2 -Ipath3)
<item>&percnt;libpaths&percnt; for the specified Library-Paths (results in: -llib1
 -llib2 -llib3)
<item>&percnt;source&percnt; for the source filename
<item>&percnt;output&percnt; for the output (target) filename
<item>&percnt;object&percnt; for the object filename (target+'.o')

</itemize>
<sect>Authors
<p>

The main author of the gIDE project is Steffen Kern (&lt;alfi@rocks.pn.org&gt;).
 The GtkEditor widget is coded by Thomas Mailund Jensen (&lt;mailund@daimi.au.dk&gt;).
 Many others have contributed patches to gIDE, some of them are listen in the
 "About gIDE" dialog that can be reached from the Help Menu.

<sect>Website
<p>

The Official gIDE Homepage is currently at http://gide.pn.org/. The Webmaster
 is Dave Smith (&lt;dsmith@ncis.ml.org&gt;).


&lsqb;1&rsqb; Syntax Highlighting and Indenting only available through
 the GtkEditor widget

&lsqb;2&rsqb; Currently only C is supported, this may change in the future

</article>
